f_map_var <- function(df_data,
                      var_to_fill,
                      legend_label = NULL,
                      var_to_facet = NULL,
                      title_label = NULL,
                      subtitle_label = NULL,
                      title_var = NULL,
                      coast_line_map = NULL,
                      var_to_fill_range = NULL,
                      log_scale = FALSE,
                      round_scale = FALSE){

  stopifnot(var_to_fill %in% names(df_data))

  ###---------------------------------------------------------------------------
  ### with facet if not null
  if (!is.null(var_to_facet)) {

    if (var_to_facet == "month") {
      # df_month <- data.frame(month = as.factor(seq(1, 12)))

      df_data <- df_data %>%
        filter(!st_is_empty(.))

      df_month_square_isis <- expand_grid(month = as.factor(seq(1, 12)),
                                          square_isis = unique(df_data$square_isis))

      df_data_spatial <- df_data %>%
        select("square_isis")

      df_month_square_isis <- left_join(df_month_square_isis,
                                        df_data_spatial,
                                        by = "square_isis")

      df_data <- df_data %>%
        st_drop_geometry() %>%
        right_join(df_month_square_isis, df_data,
                   by = c("month", "square_isis")) %>%
        st_as_sf()
    }
  }

  ###---------------------------------------------------------------------------
  ### add coast line or not if null
  if (!is.null(coast_line_map)) {
    map_var <- coast_line_map
  } else {
    map_var <- ggplot()
  }

  if (is.null(var_to_fill_range)) {
    var_to_fill_range <- df_data %>%
      st_drop_geometry() %>%
      pull(all_of(var_to_fill)) %>%
      range(., na.rm = TRUE)
  }

  ###---------------------------------------------------------------------------
  ### set legend label if not provided
  if (is.null(legend_label)) {
    legend_label <- str_replace_all(var_to_fill, "_"," ")
  }

  ###---------------------------------------------------------------------------
  ### make map
  if (log_scale) {
    if (round_scale) {
      scale_limits <- round(c(0, var_to_fill_range[2] + 1),
                            digits = 0)
    } else {
      scale_limits <- c(0, var_to_fill_range[2])
    }

    map_var <-  map_var +
      geom_sf(data = df_data, aes(fill = .data[[var_to_fill]]),
              size = 0.001,
              color = "black") +
      scale_fill_viridis_c(name = legend_label,
                           na.value = "grey",
                           limits = scale_limits)


  }

  if (log_scale == FALSE) {
    if (round_scale) {
      scale_limits <- round(c(0,
                              var_to_fill_range[2] + 6),
                            digits = -1)
    } else {
      scale_limits <- c(0, var_to_fill_range[2])
    }

    map_var <-  map_var +
      geom_sf(data = df_data, aes(fill = .data[[var_to_fill]]),
              size = 0.001,
              color = "black") +
      scale_fill_viridis_c(name = legend_label,
                           na.value = "grey",
                           limits = scale_limits)
  }

  ###---------------------------------------------------------------------------
  ### with facet if not null
  if (!is.null(var_to_facet)) {
    map_var <- map_var +
      facet_wrap( ~ .data[[var_to_facet]], ncol = 4)
  }

  ###---------------------------------------------------------------------------
  ### theme
  map_var <- map_var +
    theme(legend.position = "bottom",
          legend.direction = "horizontal",
          legend.key.width = unit(1,"cm"),
          legend.key.height = unit(0.25,"cm"),
          legend.text = element_text(angle = 0.45),
          axis.text = element_text(size = 6))

  ###---------------------------------------------------------------------------
  ### title
  if (!is.null(title_label)) {

    map_var <- map_var +
      ggtitle(label = title_label, subtitle = subtitle_label)

  } else {
    if (is.null(title_label) & is.null(title_var)) {
      map_var <- map_var +
        ggtitle(label = paste(var_to_fill,
                      "for month",
                      unique(df_data$month), sep = " "),
                subtitle = subtitle_label)
    }


    if (is.null(title_label) & !is.null(var_to_facet) & is.null(title_var)) {
      map_var <- map_var +
        ggtitle(label = var_to_fill,
                subtitle = subtitle_label)
    }


    if (!is.null(title_var) & is.null(title_label)) {

      # if (var_to_facet == "month") {

      var_title_label <- df_data %>%
        st_drop_geometry() %>%
        pull(all_of(title_var)) %>%
        unique()

      map_var <- map_var +
        ggtitle(label = paste(var_title_label, sep = " "),
                subtitle = subtitle_label)
    }
  }
  return(map_var)
}

