################################################################################
### 4 : Set spatial zone
################################################################################

###---------------------------------------
### Define metier per zone and season
#cat(bgBlue("3_4: Metier zone \n"))
#source("R/metier_analysis/set_metier_zone_season.R") #ALready done

# Extract only 2017 year in eflalo for internship report :
df_eflalo_ext <- df_eflalo %>%
  filter(REF_YEAR== 2017) %>%
  droplevels() %>%
  mutate(metier = super_metier) %>%
  rename(SECT_COD_SACROIS = LE_RECT) %>%
  rename(LE_RECT = SECT_COD_SACROIS_NIV5)  #Ok

###-----------------------------------------------------------------------------
### Make map for preliminary analysis
cat(bgBlue("3_2_0: Plot metier zone for internship report \n"))
source("R/metier_analysis/make_map_internship_analysis.R")

###-----------------------------------------------------------------------------
### Define metier per zone and season
cat(bgBlue("3_3: Plot metier zone \n"))
source("R/metier_analysis/plot_metier_zone.R")

###-----------------------------------------------------------------------------
### Define metier per zone and season
cat(bgBlue("3_4: Metier season \n"))
source("R/metier_analysis/set_metier_season_as_year.R")

###-----------------------------------------------------------------------------
### choose metier level to use in strategies setting
df_eflalo <- df_eflalo %>%
  mutate(metier_isis = metier_zone_season)


source("R/metier_analysis/filter_vessel_metier_isis_OTH_only.R")

###---------------------------------------------------------------------------
### save eflalo with metier isis finish
saveRDS(df_eflalo, file = paste0(path_eflalo_data, "/df_eflalo_metier_spatial_",
                                 min(year_vec), "_",
                                 max(year_vec), ".rds"))
