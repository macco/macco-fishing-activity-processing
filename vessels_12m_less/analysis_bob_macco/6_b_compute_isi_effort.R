################################################################################
### Compute effort for isis fish
################################################################################

cat(bgBlue("6: Compute effort for ISIS-Fish \n"))
cat("6_1: Compute number of vessels per fleet and strategies \n")

ending_year <- 2018

source("R/effort_analysis/compute_inactivity.R")

source("R/effort_analysis/compute_nvessel_fleet_strategy.R")

source("R/effort_analysis/compute_isis_effort_input.R")

source("R/effort_analysis/compute_isis_effort_calibration.R")
