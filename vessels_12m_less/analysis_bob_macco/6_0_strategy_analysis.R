################################################################################
### set strategies given the fleets, effort metier and month
################################################################################

cat(bgBlue("6: strategys definition \n"))

###-----------------------------------------------------------------------------
if (file.exists(paste0(path_eflalo_data, "/df_eflalo_strategy_fleet_metier_zone_isis_",
                       min(year_vec), "_",
                       max(year_vec), ".rds")) &
    erase_tidy_data == FALSE) {

  df_eflalo <- read_rds(paste0(path_eflalo_data, "/df_eflalo_strategy_fleet_metier_zone_isis_",
                               min(year_vec), "_",
                               max(year_vec), ".rds"))

} else {

  ###-----------------------------------------------------------------------------
  if (file.exists(paste0(path_eflalo_data, "/df_eflalo_fleet_metier_zone_isis_",
                         min(year_vec), "_",
                         max(year_vec), ".rds"))) {

    df_eflalo <- read_rds(paste0(path_eflalo_data, "/df_eflalo_fleet_metier_zone_isis_",
                                 min(year_vec), "_",
                                 max(year_vec), ".rds"))

  } else {
    stop("Run 4_0_fleet_definition.R to create df_eflalo with metier isis")
  }

  ### Create a data frame from eflalo that will be filtered for computing strategy
  df_eflalo <- df_eflalo %>%
    arrange(REF_YEAR, MONTH) %>%
    mutate(REF_YEAR_MONTH = fct_inorder(paste0(REF_YEAR, "_", MONTH)),
           metier_isis_REF_YEAR_MONTH = fct_inorder(paste0(metier_isis,
                                                           "_", REF_YEAR_MONTH)))

  ### create a strategy eflalo for filering eflalo with active vessels for clustering analysis
  # df_eflalo_strategy <- df_eflalo

  ###-----------------------------------------------------------------------------
  ### filter eflalo for keeping active vessels only
  # source(here("R/strategy_analysis/filter_vessels_with_low_activity.R"))
  # source(here("R/strategy_analysis/filter_vessels_with_more_than_one.R"))
  source(here("R/strategy_analysis/plot_vessel_activity.R"))

  ###-----------------------------------------------------------------------------
  ### filter eflalo for keeping active vessels only
  # source(here("R/strategy_analysis/filter_strategy_with_one_vessel_only.R"))
  # source(here("R/strategy_analysis/plot_super_strategy.R"))

  ###-----------------------------------------------------------------------------
  ### perform clustering
  # source(here("R/strategy_analysis/strategy_clustering.R"))
  # source(here("R/strategy_analysis/merge_cluster_with_eflalo_strategy.R"))
  # source(here("R/strategy_analysis/plot_strategy_cluster.R"))


  ###-----------------------------------------------------------------------------
  ### Fleet definition by hand because clustering is not performing well
  source(here("R/strategy_analysis/strategy_definition.R"))
  source(here("R/strategy_analysis/plot_strategy_isis_vessel_activity.R"))

  ###-----------------------------------------------------------------------------
  source(here("R/strategy_analysis/process_strategy_data.R"))
  source(here("R/strategy_analysis/plot_strategy_isis.R"))
  # source(here("R/strategy_analysis/plot_strategy_isis_year_month_metier_isis_prop.R"))

  saveRDS(df_eflalo,
          paste0(path_eflalo_data, "/df_eflalo_strategy_fleet_metier_zone_isis_",
                 min(year_vec), "_",
                 max(year_vec), ".rds"))


  ###---------------------------------------------------------------------------
  df_eflalo %>%
    dplyr::select(strategy_isis) %>%
    distinct() %>%
    write_csv(x = .,
              file = paste0(path_isis_data,
                            "/export_strategy_isis.csv"))

}
