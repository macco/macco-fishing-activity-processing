################################################################################
### Estimation of Fishing activity parameters for ISIS-Fish
################################################################################

cat(bgBlue("7: Estimation of Fishing activity parameters for ISIS-Fish \n"))

###-----------------------------------------------------------------------------
### load data for analysis
source("R/estim_fishing_activity_parameters/1_load_data_for_estim.R")

###-----------------------------------------------------------------------------
### plot data before fitting
source("R/estim_fishing_activity_parameters/2_plot_data.R")

###-----------------------------------------------------------------------------
### fit model
source("R/estim_fishing_activity_parameters/3_fit_model.R")

###-----------------------------------------------------------------------------
### extract estimated coefficient
source("R/estim_fishing_activity_parameters/4_extract_fitted_coefficient.R")

###-----------------------------------------------------------------------------
### plot estimates
source("R/estim_fishing_activity_parameters/5_plot_coefficient.R")
