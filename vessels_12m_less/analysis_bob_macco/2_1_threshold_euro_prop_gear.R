###-----------------------------------------------------------------------------
### Use threshold_euro_prop_metier per year define by Mahevas

threshold_euro_prop_gear <- data.frame(
  year = 2006:2014,
  threshold_euro_prop_gear = c(0.958, 0.956, 0.953, 0.942, 0.935,
                               0.939, 0.896, 0.892,0.914))
mean(c(0.958, 0.956, 0.953, 0.942, 0.935,
       0.939, 0.896, 0.892,0.914))

### fill extra year
if (ending_year > max(threshold_euro_prop_gear$year)) {
  threshold_euro_prop_gear <- bind_rows(threshold_euro_prop_gear,
                                        data.frame(year = (max(threshold_euro_prop_gear$year) + 1):ending_year,
                                                   threshold_euro_prop_gear = rep(0.950, times = length((max(threshold_euro_prop_gear$year) + 1):ending_year))))
}
