################################################################################
### Start analysis from eflalo data to reduce the number of metier for
### implementation in ISIS-Fish
### at the end of this third step, df_eflalo data frame has a new column metier
### for ISIS-Fish called metier_isis also used in step 4 for strategy analysis
################################################################################

if( !(file.exists(here(path_metier_data,
                    "df_metier_isis_sacrois_dcf_6_2010_2020.rds")) &
   file.exists(paste0(path_eflalo_data,
                      "/df_eflalo_metier_isis2010_2020.rds")))) {
  stop("Reference eflalo files missing (2010-2020).
       Please generate df_eflalo_metier_isis2010_2020.rds and df_metier_isis_sacrois_dcf_6_2010_2020.rds \n")
}

###-----------------------------------------------------------------------------
### If analysis already performed and erase_coda == false
### load previous analysis, otherwise run analysis
if (file.exists(paste0(path_eflalo_data, "/df_eflalo_metier_isis",
                       min(year_vec), "_",
                       max(year_vec), ".rds")) &
    erase_tidy_data == FALSE) {

  df_eflalo <- read_rds(paste0(path_eflalo_data, "/df_eflalo_metier_isis",
                               min(year_vec), "_",
                               max(year_vec), ".rds"))


  if (file.exists(here(path_data_tidy_plots,
                       "scale_color_metier_isis.rds"))) {


    scale_color_metier_isis <-  readRDS(file = here(path_data_tidy_plots,
                                                    "scale_color_metier_isis.rds"))

  }else{

    scale_color_metier_isis <- f_palette_for_factor(levels(as.factor(df_eflalo$metier_isis)),
                                                    palette = "Set1")

    saveRDS(scale_color_metier_isis, file = here(path_data_tidy_plots,
                                                 "scale_color_metier_isis.rds"))

  }

} else {
  ###---------------------------------------------------------------------------
  ### perform clustering metier analysis
  cat(bgBlue("3: Metier analysis \n"))
  cat(bgBlue(paste0("3_1: Process ", metier_analysis, "\n")))

  if (file.exists(paste0(path_eflalo_data, "/df_eflalo_filtered_",
                         min(year_vec), "_",
                         max(year_vec), ".rds"))) {

    df_eflalo <- read_rds(paste0(path_eflalo_data, "/df_eflalo_filtered_",
                                 min(year_vec), "_",
                                 max(year_vec), ".rds"))

  } else {
    stop("Run 2_0_preliminary_analysis.R to create df_eflalo")
  }

  ###---------------------------------------------------------------------------
  if (metier_analysis ==  "metier_dcf6") {
    source("R/metier_analysis/set_metier_as_dcf6_metier.R")
  }

  ###---------------------------------------------------------------------------
  if (metier_analysis ==  "gear_isis_metier_dcf6") {
    source("R/metier_analysis/set_metier_as_dcf6_metier_and_gear_isis.R")
  }
  source("R/metier_analysis/remove_weird_fishing_seq.R")

  ###---------------------------------------------------------------------------
  ### Plot metier 1 after after metier selection
  # source("R/metier_analysis/plot_metier.R")


  ### Load reference data from 2010 to 2018
  df_eflalo_ref <-  read_rds(paste0(path_eflalo_data,
                                    "/df_eflalo_metier_isis2010_2020.rds"))

  ###---------------------------------------------------------------------------
  ### correspondance table between metier_isis metier_sacrois and dcf6
  df_metier_isis_sacrois_dcf_6 <- read_rds(here(path_metier_data,
                                                "df_metier_isis_sacrois_dcf_6_2010_2020.rds")) %>%
    dplyr::select(metier, metier_isis) %>%
    distinct()

  ### When new metier in following year (>2020) manually assign a metier_isis
  df_metier_isis_sacrois_dcf_6 <- df_metier_isis_sacrois_dcf_6 %>%
    add_row(metier = "G_GTR_CRU_50_59_0", metier_isis = "G_1") %>%
    add_row(metier = "F_FPO_CEP_0_0_0", metier_isis = "F_1")  %>%
    add_row(metier = "O_OTB_CRU_32_54_0", metier_isis = "O_5") %>%
    add_row(metier = "G_GNS_CRU_>=100_0", metier_isis = "G_3")  %>%
    add_row(metier = "Mixed_PTM_SPF_>=70_0", metier_isis = "M_low_activity_2")  %>%
    add_row(metier = "M_PTM_SPF_>=70_0", metier_isis = "M_low_activity_2")  %>%
    add_row(metier = "M_PTM_DEF_>=70_0", metier_isis = "M_low_activity_2")  %>%
    add_row(metier = "Mixed_OTM_DEF_>=70_0", metier_isis = "O_1") %>%
    add_row(metier = "M_OTM_DEF_>=70_0", metier_isis = "O_1") %>%
    add_row(metier = "M_metier_OTH", metier_isis = "M_metier_OTH") %>%
    add_row(metier = "M_low_activity", metier_isis = "M_low_activity_2")



  if(length(setdiff(unique(df_eflalo$metier), unique(df_metier_isis_sacrois_dcf_6$metier)))  > 0) {
    stop("Metier DCF-6 in new years not present in reference eflalo (2010-2020) \n")
  }

  metier_isis_sacrois_dcf_6_cols <- names(df_metier_isis_sacrois_dcf_6)
  metier_isis_sacrois_dcf_6_cols <- metier_isis_sacrois_dcf_6_cols[!grepl("^metier$", metier_isis_sacrois_dcf_6_cols)]
  metier_isis_sacrois_dcf_6_cols <- metier_isis_sacrois_dcf_6_cols[!grepl("NAVLC4_COD", metier_isis_sacrois_dcf_6_cols)]

  ### extract years that are not in reference data and need metier
  df_eflalo_to_update <- df_eflalo %>%
    filter(!(REF_YEAR %in% df_eflalo_ref$REF_YEAR)) %>%
    droplevels() %>%
    dplyr::select(-any_of(metier_isis_sacrois_dcf_6_cols)) %>%
    left_join(., df_metier_isis_sacrois_dcf_6,
              by = c("metier"))

  df_eflalo <- bind_rows(df_eflalo_ref,
                         df_eflalo_to_update)

  source("R/metier_analysis/filter_vessel_metier_isis_OTH_only.R")


  scale_color_metier_isis <- f_palette_for_factor(
    unique(as.character(df_eflalo$metier_isis))[order(stringi::stri_extract_last(unique(as.character(df_eflalo$metier_isis)), regex = "\\w"))],
    #levels(fct_shuffle(df_eflalo$metier_isis)),
    palette = "Set1")

  saveRDS(scale_color_metier_isis,
          file = here(path_data_tidy_plots,
                      "scale_color_metier_isis.rds"))

  ###---------------------------------------------------------------------------
  ### Plots super metier from clustering
  source("R/metier_analysis/plot_metier_isis.R")

  ### Drop empty levels before saving
  df_eflalo <- df_eflalo %>%
    droplevels()

  ###---------------------------------------------------------------------------
  ### save eflalo with metier
  saveRDS(df_eflalo, file = paste0(path_eflalo_data, "/df_eflalo_metier_isis",
                                   min(year_vec), "_",
                                   max(year_vec), ".rds"))
}
