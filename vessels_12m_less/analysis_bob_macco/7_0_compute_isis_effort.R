################################################################################
### 7 : Compute effort for isis fish
################################################################################

cat(bgBlue("6: Compute effort for ISIS-Fish \n"))
cat("6_1: Compute number of vessels per fleet and strategies \n")

source("R/effort_analysis/compute_isis_effort_calibration.R")
source("R/effort_analysis/compute_isis_effort_input.R")
