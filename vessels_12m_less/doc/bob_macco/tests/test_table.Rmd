---
title: "Untitled"
author: "Jean-Baptiste Lecomte"
date: "2023-01-26"
output: html_document
---
```{r setup-package, include = FALSE}
library(here)
library(patchwork)
fishery_name <- "bob_macco"
```

```{r, child = c(here("doc", "template", "setup.Rmd"))}
```


```{r}
df_metier_isis_vessel_size_summary <- readRDS(here(path_isis_data, "df_metier_isis_vessel_size_summary.rds"))
kable(df_metier_isis_vessel_size_summary)
```

