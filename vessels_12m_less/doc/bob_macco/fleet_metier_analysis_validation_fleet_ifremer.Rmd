---
title: "Macco - Fleet analysis of vessels under 12m from Ifremer fleets segmentation"
date: "`r Sys.Date()`"
output: html_document
---

```{r setup-package, include = FALSE}
library(here)
fishery_name <- "bob_macco"
```

```{r, child = c(here("doc", "template", "setup.Rmd"))}
```

```{r, child = c(here("doc", "template", "fleet_analysis", "load_fleet_isis.Rmd"))}
```

```{r, child = c(here("doc", "template", "fleet_analysis", "load_fleet_ifremer.Rmd"))}
```

# Isis fleets for Macco per vessels size based on ifremer fleet segmentation

## Number of vessels 
```{r fleet-ifremer-n-navs-macco-nav-size}
list_plot_fleet_ifremer_macco_nav_size$plot_fleet_ifremer_n_nav
```


## Number of fishing sequences 
```{r fleet-ifremer-n-seq-macco-nav-size}
list_plot_fleet_ifremer_macco_nav_size$plot_fleet_ifremer_n_seq
```

## Number of fishing sequences per metier isis {.tabset}

```{r expand-fleet-isis-nseq-metier, include = FALSE}

list_plot_fleet_ifremer_n_seq_metier_isis <- list_plot_fleet_ifremer_macco_nav_size$list_plot_fleet_ifremer_n_seq_metier_isis

# create a list of raw Rmarkdown code as a list of character vectors, which are then evaluated
fleet_names <- names(list_plot_fleet_ifremer_n_seq_metier_isis)
n_fleets <- length(fleet_names)
plots_fleets <- vector("list", n_fleets) # the list of raw Rmarkdown code


# 1st level tabset ------------------------------------------------------------
for (i in 1:n_fleets) {
  fleet <- names(fleet_names)[i]
  
  plots_fleet_activity <- vector("list", 1 + i) # the lower level list of raw Rmarkdown code
  plots_fleet_activity[[1]] <- knit_expand(text = c( # first element = heading
    "### {{fleet_names[i]}} {.tabset}\n",
    "```{r nseq-metier-{{i}}}",
    "list_plot_fleet_ifremer_n_seq_metier_isis[[{{i}}]]",
    "```\n",
    "\n"
  ))
  plots_fleets[[i]] <- unlist(plots_fleet_activity)
}
```

`r knit(text = unlist(plots_fleets))`

## Average annual landings

```{r fleet-ifremer-landings-macco-nav-size}
list_plot_fleet_ifremer_macco_nav_size$plot_landing_fleet_ifremer 
```

```{r fleet-ifremer-landings-prop-macco-nav-size}
list_plot_fleet_ifremer_macco_nav_size$plot_landing_prop_fleet_ifremer
```

## Average annual landings with Macco species only
```{r fleet-ifremer-landings-noOTH-macco-nav-size}
list_plot_fleet_ifremer_macco_nav_size$plot_landing_fleet_ifremer_noOTH
```

```{r fleet-ifremer-landings-noOTH-prop-macco-nav-size}
list_plot_fleet_ifremer_macco_nav_size$plot_landing_prop_fleet_ifremer_noOTH
```

## Proportion of species, metier, gear per fleets

### Proportion of species landings per metier isis {.tabset}

```{r, child = c(here("doc", "template", "metier_analysis", "tabset_metier_fleet_isis_species.Rmd")), eval=TRUE}
```


### Proportion of Macco species only landings per metier isis {.tabset}

```{r, child = c(here("doc", "template", "metier_analysis", "tabset_metier_fleet_isis_species_noOTH.Rmd")), eval=TRUE}
```

### Proportion of gear per year {.tabset}

```{r, child = c(here("doc", "template", "metier_analysis", "tabset_metier_fleet_isis_gear.Rmd")), eval=TRUE}
```


### Proportion of metier isis per year  {.tabset}

```{r, child = c(here("doc", "template", "metier_analysis", "tabset_metier_fleet_isis_metier_isis.Rmd")), eval=TRUE}
```

