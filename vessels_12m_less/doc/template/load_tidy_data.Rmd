```{r load-tidy-data}
df_eflalo <- read_rds(paste0(path_eflalo_data, "/df_eflalo_final_isis",
                             min(year_vec), "_",
                             max(year_vec), ".rds"))

df_sacrois <- read_rds(paste0(path_sacrois_data_tidy, "/df_sacrois_",
                              min(year_vec), "_",
                              max(year_vec), ".rds"))

```

