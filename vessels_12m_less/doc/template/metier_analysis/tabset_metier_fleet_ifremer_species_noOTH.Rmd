```{r expand-fleet-isis-noOTH, include = FALSE}

list_plots_prop_landings_metier_isis_fleet_isis_noOTH <- read_rds(here(path_data_tidy_plots, "list_plots_prop_landings_metier_isis_fleet_isis_noOTH.rds"))

# create a list of raw Rmarkdown code as a list of character vectors, which are then evaluated
fleet_names <- names(list_plots_prop_landings_metier_isis_fleet_isis_noOTH)
n_fleets <- length(fleet_names)
plots_fleets <- vector("list", n_fleets) # the list of raw Rmarkdown code


# 1st level tabset ------------------------------------------------------------
for (i in 1:n_fleets) {
  fleet <- names(fleet_names)[i]
  
  plots_fleet_activity <- vector("list", 1 + i) # the lower level list of raw Rmarkdown code
  plots_fleet_activity[[1]] <- knit_expand(text = c( # first element = heading
    "#### {{fleet_names[i]}} {.tabset}\n",
    "```{r plots-prop-landings-metier-isis-fleet-isis_noOTH{{i}}}",
    "list_plots_prop_landings_metier_isis_fleet_isis_noOTH[[{{i}}]]",
    "```\n",
    "\n"
  ))
  plots_fleets[[i]] <- unlist(plots_fleet_activity)
}
```

`r knit(text = unlist(plots_fleets))`
