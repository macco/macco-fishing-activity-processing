```{r load-strategy-isis}
list_plot_strategy_isis <- readRDS(here(path_data_tidy_plots,
                                        "list_plot_strategy_isis.rds"))
```

```{r strategy-nvessel, fig.cap="Annual average number of vessels per strategies isis."}
list_plot_strategy_isis$plot_strategy_isis_nvessels
```

```{r strategy-nvessel-years, fig.cap="Average number of vessels per strategies isis and years."}
list_plot_strategy_isis$plot_strategy_isis_fishing_seq_years
```


```{r strategy-nseq, fig.cap="Annual average number of fishing sequences per strategies isis."}
list_plot_strategy_isis$plot_strategy_isis_fishing_seq
```

```{r strategy-average-landings, fig.cap="Annual average mean landings per strategy isis with key species only."}
list_plot_strategy_isis$plot_strategy_isis_landings_no_OTH
```
