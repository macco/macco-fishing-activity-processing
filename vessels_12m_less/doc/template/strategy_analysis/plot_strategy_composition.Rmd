## Strategy composition per year-month {.tabset} 

```{r expand-strategy-isis-composition-month, include = FALSE}

# create a list of raw Rmarkdown code as a list of character vectors, which are then evaluated
strategy_comp_names <- names(list_plot_eflalo_strategy_prop_metier$list_plot_eflalo_strategy_prop)
n_strategy_comps <- length(strategy_comp_names)
plots_strategy_comps <- vector("list", n_strategy_comps) # the list of raw Rmarkdown code


# 1st level tabset ------------------------------------------------------------
for (i in 1:n_strategy_comps) {
  strategy_comp <- names(strategy_comp_names)[i]
  
  plots_strategy_comp_activity <- vector("list", 1 + i) # the lower level list of raw Rmarkdown code
  plots_strategy_comp_activity[[1]] <- knit_expand(text = c( # first element = heading
    "### strategy isis composition: {{strategy_comp_names[i]}} {.tabset}\n",
    "```{r, str-year-month-{{strategy_comp_names[i]}}}\n",
    "list_plot_eflalo_strategy_prop_metier$list_plot_eflalo_strategy_prop[[{{i}}]] +
    ggtitle('')",
    "```\n",
    "\n"
  ))
  plots_strategy_comps[[i]] <- unlist(plots_strategy_comp_activity)
}
```

`r knit(text = unlist(plots_strategy_comps))`

## Strategy composition per month {.tabset} 

```{r expand-strategy-isis-composition-year-month, include = FALSE}

# create a list of raw Rmarkdown code as a list of character vectors, which are then evaluated
strategy_comp_names <- names(list_plot_eflalo_strategy_prop_metier$list_plot_eflalo_strategy_prop_mean)
n_strategy_comps <- length(strategy_comp_names)
plots_strategy_comps_mean <- vector("list", n_strategy_comps) # the list of raw Rmarkdown code


# 1st level tabset ------------------------------------------------------------
for (i in 1:n_strategy_comps) {
  strategy_comp <- names(strategy_comp_names)[i]
  
  plots_strategy_comp_activity <- vector("list", 1 + i) # the lower level list of raw Rmarkdown code
  plots_strategy_comp_activity[[1]] <- knit_expand(text = c( # first element = heading
    "### strategy isis composition: {{strategy_comp_names[i]}} {.tabset}\n",
    "```{r, str-month-{{strategy_comp_names[i]}}}\n",
    "list_plot_eflalo_strategy_prop_metier$list_plot_eflalo_strategy_prop_mean[[{{i}}]] +
    ggtitle('')",
    "```\n",
    "\n"
  ))
  plots_strategy_comps_mean[[i]] <- unlist(plots_strategy_comp_activity)
}
```

`r knit(text = unlist(plots_strategy_comps_mean))`
