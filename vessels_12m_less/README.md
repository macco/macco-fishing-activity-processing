# Processing fisheries data to parametrize ISIS-fish 

## Objectives

- Reproducible methods to compute fisheries input for ISIS-fish models.
- Build reproducible and generic documentation describing the methods.

## Documentation

- `doc/general_doc/project_structure_nomenclature.Rmd` presents the structure of the project and the nomenclature used for R objects.
- `doc/general_doc/fishing_activity_workflow.Rmd` show the workflow used to analyse the fishery data
- A vignette summarizing the analysis performed for each fishery is generated dynamically and saved in the `doc` folder.

## Run the analysis

1. Open the `Main.R` file.
2. Choose the fishery to work with by setting `fishery_name`. For example `fishery_name <- "bob_macco"` for the fishery of the bay of Biscay in the macco project.
3. Check that the options/hypotheses in the `R/fishery_examples/bob_macco/0_control_file.R` are set correctly.
4. Source the `Main.R` file with R.
5. Data export for isis are stored in `data/tidy/fishery_name/isis`

## Data requirement

- sacrois flux as Rdata files (one for each year i.e. `SACROISJour_Flux_2020_v3.3.10.RData`) are available at <https://sih.ifremer.fr/>
- Reference tables for vessels and species (<https://sih.ifremer.fr/>)
- ICES rectangles and areas for producing maps 

Raw data are available at  `\\nantes\simulateur\ActiviteDePeche\Analyses_generiques\fishing-activity-processing\data\raw`

## R packages requirement

All pakcages used for the analysis are avaiblable on CRAN, excepts for Ifremer COSTs packages. They can be installed manually from zip files available at  `\\nantes\simulateur\ActiviteDePeche\Analyses_generiques`. The analysis can be performed without COSTs packages installed.

## R packages management with renv

The `renv` package is a tool to manage R packages version. `renv` helps manage library paths to help isolate R project dependencies. Existing tools used for managing R packages (e.g. install.packages(), remove.packages()) should work as they did before.

If user encounters problem in installing or loading CRAN R packages, use the command `renv::restore()` (see help <https://rstudio.github.io/renv/articles/collaborating.html>). If the error persists, user can delete manually `renv` folder and `renv.lock`, but will loose the availability to work with the packages version used in this project.

## Fork the project

In order to use the proposed method and scripts, the user should fork the project. The forked project is a copy of the original repository by now owned by the user, the original repository is called the upstream. Forking a repository allows for file editing and versioning without interfering with the original repository. It also keeps the link with the original repository.

In order to fork a repository, the user can follow the steps proposed here : <https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html>

To set the original repository as the upstream repository, user can open a terminal in the forked repository and run:

1. `git remote add upstream https://gitlab.ifremer.fr/isis-fish-tools/data-processing.git` to set the remote
2. `git fetch upstream` retrieve the changes made in the original repository
3. `git merge upstream/master` merge the changes from the original repository to the forked repository

more help about fork and upstream can be found here : <https://www.atlassian.com/git/tutorials/git-forks-and-upstreams>

## How to smartly use the code 

### Test different approaches to set metiers (or strategies, fleets ...)

In order to compare and track changes, the user should create a fishery for each combination of state variables that he want to compare. For example, the user want to compare a clustering analysis for metier and the metier defined in sacrois date by the `METIER_COD_SACROIS` variable. It is recommended to create to fishery example sharing the same state variables excepts de metier state variable and compare the outputs of the two methods after.

### Where to save my custom codes

If generic R codes developed here to do not fulfill your needs, the best place to save your custom codes is in the fishery directory (`path_fishery` : i.e. `R/fishery_examples/bob_macco`). If your codes are generic and could interest other users do not hesitate to ask for a merge request (see help <https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork>).  

## Notes

- Fishing effort (df_eflalo$LE_EFF) is extracted as default from sacrois data as TP_NAVIRES_SACROIS: Temps de pêche consolidé (‘de référence’) de la séquence en heures décimales, issu du croisement entre temps de pêche géolocalisé et temps de pêche SACAPT, en privilégiant la source GEOLOC lorsque celle-ci est renseignée.
- Ifremer fleet segmentation is set to default. DCF fleet segmentation is not implemented yet.
- Duplicated rows in sacrois data are removed based on:
- ORIGINE_ESP_COD_FAO: SIPA_CAPTURES or SIPA_DEBARQUEMENTS are kept, OFIMER are discarded
