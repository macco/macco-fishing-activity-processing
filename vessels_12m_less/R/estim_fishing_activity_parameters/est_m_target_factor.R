###################
#2 calcul de targeting factors par saison et metier, saison, metier, plot des tendances saisonni?res pour chaque m?tier

load(paste(inputPath,"sacroisSSdoublonRed",as.character(year),".Rdata",sep=""))
colnames(sacroisSSdoublonRed)

# metier scale only

TarfHkeOld=data.frame(MetAgZone=c("spanishTrawlers","spanishTrawlers","Metier_ChalutMixte_NordPC","Metier_ChaluMixte_InterC","Metier_ChalutBenth_NordAPC","Metier_ChalutBenth_NordC","Metier_ChalutBenth_APCS","Metier_Lang_NordPC","Metier_Lang_InterC","Metier_FiletSole_NordC","Metier_FiletSole_NordIntPC","Metier_FiletSole_InterSudPC","Metier_FiletSole_InterC","Metier_ChalutSole_InterC","Metier_PalangreMerlu_InterSudAC","Metier_PalangreMerlu_InterSudC","Metier_PalangreMixte_NordC","Metier_PalangreMixte_InterC","Metier_ChalutSole_InterSudC","Metier_ChalutSole_NordCet","Metier_FiletMerlu_NordC","Metier_FiletMerlu_NordPC","Metier_FiletMixte_NordPC","Metier_FiletMixte_NordC","Metier_FiletMixte_NordInterPC","Metier_FiletMixte_InterSudC","Metier_FiletMIxte_InterC","Metier_FiletMerlu_InterSudAPC","Metier_FiletMerlu_InterSudC","Metier_ChalutMixte_NordC","Metier_FiletMerlu_InterSudPC","Metier_ChalutMixte_APCS","Metier_Lang_InterPC"),factor = c(0.76,0.13,1.52,1.11,0.36,2.07,0.72,1.62,3.4,0.01,0.02,0.02,0.08,1.52,12.14,7.13,0.26,0.41,1.46,0.54,0.38,1.49,1,1,1,0.13,0.13,1.64,0.18,0.66,0.28,1.05,2.84))

TarfHkeOld$factor<-TarfHkeOld$factor/sum(TarfHkeOld$factor)
TarfHkeOld$MetAgZone<-as.character(TarfHkeOld$MetAgZone)
TarfHkeOld$LE_KG=TarfHkeOld$LE_EFF=TarfHkeOld$LdivE=NA
TarfHkeOld$season="old"

TarfDf<- sacroisSSdoublonRed[sacroisSSdoublonRed$ESP_COD_FAO=="HKE",]
TarfHke<-aggregate(cbind(LE_KG,LE_EFF)~MetAgZone,TarfDf,FUN=sum)
TarfHke$LdivE<-TarfHke$LE_KG/TarfHke$LE_EFF
aaa<-data.frame(MetAgZone="spanishTrawlers", LE_KG=NA, LE_EFF=NA, LdivE=mean(CPUESpanishTrawlers))
aaa$MetAgZone=as.character(aaa$MetAgZone)
TarfHke<-rbind(TarfHke,aaa)
TarfHke$factor=TarfHke$LdivE/sum(TarfHke$LdivE)
TarfHke$season="all"
TarfHke$MetAgZone<-as.character(TarfHke$MetAgZone)
# this metier  positionning si DIFFERENT of the previous one. the 2 spanish trawlers metier share the same targeting factors.

# season + m?tiers scale
TarfDfSeas <- sacroisSSdoublonRed[sacroisSSdoublonRed$ESP_COD_FAO=="HKE",]
TarfDfSeas$month<-sapply(as.character(TarfDfSeas$LE_DAT), function(x) as.numeric(strsplit(x, split="-")[[1]][2]))
TarfDfSeas$season<-floor((TarfDfSeas$month-1)/3)+1

TarfHkeSeas<-aggregate(cbind(LE_KG,LE_EFF)~MetAgZone+season,TarfDfSeas,FUN=sum)
TarfHkeSeas$LdivE<-TarfHkeSeas$LE_KG/TarfHkeSeas$LE_EFF
TarfHkeSeas$factor<-NA
aaa<-data.frame(MetAgZone="spanishTrawlers", season=1:4, LE_KG=NA, LE_EFF=NA, LdivE=CPUESpanishTrawlers, factor=NA)
aaa$MetAgZone<-as.character(aaa$MetAgZone)
TarfHkeSeas<-rbind(TarfHkeSeas,aaa)

TarfHkeSeas$factor[TarfHkeSeas$season==1]<-TarfHkeSeas$LdivE[TarfHkeSeas$season==1]/sum(TarfHkeSeas$LdivE[TarfHkeSeas$season==1])
TarfHkeSeas$factor[TarfHkeSeas$season==2]<-TarfHkeSeas$LdivE[TarfHkeSeas$season==2]/sum(TarfHkeSeas$LdivE[TarfHkeSeas$season==2])
TarfHkeSeas$factor[TarfHkeSeas$season==3]<-TarfHkeSeas$LdivE[TarfHkeSeas$season==3]/sum(TarfHkeSeas$LdivE[TarfHkeSeas$season==3])
TarfHkeSeas$factor[TarfHkeSeas$season==4]<-TarfHkeSeas$LdivE[TarfHkeSeas$season==4]/sum(TarfHkeSeas$LdivE[TarfHkeSeas$season==4])


# season scale only

TarfHkeSeasOnly<-aggregate(cbind(LE_KG,LE_EFF)~season,TarfDfSeas,FUN=sum)
# Add spanish catch and efforts
TarfHkeSeasOnly$LE_KG=1000*c(2888,2311,1408,2046)+TarfHkeSeasOnly$LE_KG
TarfHkeSeasOnly$LE_EFF=spanishEffortTrawlers+TarfHkeSeasOnly$LE_EFF
TarfHkeSeasOnly$LdivE<-TarfHkeSeasOnly$LE_KG/TarfHkeSeasOnly$LE_EFF

TarfHkeSeasOnly$factor<-TarfHkeSeasOnly$LdivE/sum(TarfHkeSeasOnly$LdivE)


#3 plot

TarfHkeSeas4plot<-rbind(TarfHkeSeas,TarfHkeOld,TarfHke)

FactorsMetier<-ggplot(TarfHkeSeas4plot, aes(x=MetAgZone,y=factor,colour=season))+
  geom_point(size=2)+
  xlab("Metier")+
  ylab("Factor")+
  expand_limits(y = 0)+
  theme_minimal(base_size=18)+
  theme(axis.text.x = element_text(angle = 45, hjust=1),axis.title.y=element_text(angle=0),panel.grid.major = element_line(size = 2), legend.key.width=unit(2,"cm"))
FactorsMetier

FactorsMetier2<-ggplot(subset(TarfHkeSeas4plot, season%in%c("all","old")), aes(x=MetAgZone,y=factor,colour=season))+
  geom_point(size=2)+
  xlab("Metier")+
  ylab("Factor")+
  expand_limits(y = 0)+
  theme_minimal(base_size=18)+
  theme(axis.text.x = element_text(angle = 45, hjust=1),axis.title.y=element_text(angle=0),panel.grid.major = element_line(size = 2), legend.key.width=unit(2,"cm"))
FactorsMetier2

TarfHkeSeas4plotSeas<-TarfHkeSeas
TarfHkeSeas4plotSeas<-dcast(TarfHkeSeas4plotSeas,MetAgZone~season,value.var="factor")
TarfHkeSeas4plotSeas[is.na(TarfHkeSeas4plotSeas)]=0
TarfHkeSeas4plotSeas$sum<-TarfHkeSeas4plotSeas$`1`+TarfHkeSeas4plotSeas$`2`+TarfHkeSeas4plotSeas$`3`+TarfHkeSeas4plotSeas$`4`
TarfHkeSeas4plotSeas[TarfHkeSeas4plotSeas==0]=NA
TarfHkeSeas4plotSeas$`1`=TarfHkeSeas4plotSeas$`1`/TarfHkeSeas4plotSeas$sum
TarfHkeSeas4plotSeas$`2`=TarfHkeSeas4plotSeas$`2`/TarfHkeSeas4plotSeas$sum
TarfHkeSeas4plotSeas$`3`=TarfHkeSeas4plotSeas$`3`/TarfHkeSeas4plotSeas$sum
TarfHkeSeas4plotSeas$`4`=TarfHkeSeas4plotSeas$`4`/TarfHkeSeas4plotSeas$sum
TarfHkeSeas4plotSeas<-melt(subset(TarfHkeSeas4plotSeas,select=-c(sum)), id.vars=c("MetAgZone"))
names(TarfHkeSeas4plotSeas)<-c("MetAgZone","season","factor")

FactorsSeasons<-ggplot(TarfHkeSeas4plotSeas, aes(x=as.numeric(season),y=factor,colour=MetAgZone))+
  geom_line(size=0.5)+
  xlab("Season")+
  ylab("Factor")+
  expand_limits(y = 0)+
  theme_minimal(base_size=18)+
  theme(axis.text.x = element_text(angle = 45, hjust=1),axis.title.y=element_text(angle=0),panel.grid.major = element_line(size = 2), legend.key.width=unit(2,"cm"))
FactorsSeasons

TarfHkeSeas4plotSeasAgain<-TarfHkeSeas
TarfHkeSeas4plotSeasAgain<-dcast(TarfHkeSeas4plotSeasAgain,season~MetAgZone,value.var="factor")
TarfHkeSeas4plotSeasAgain[is.na(TarfHkeSeas4plotSeasAgain)]=0
TarfHkeSeas4plotSeasAgain<-rbind(TarfHkeSeas4plotSeasAgain,c(0,colSums(TarfHkeSeas4plotSeasAgain[,2:dim(TarfHkeSeas4plotSeasAgain)[2]])))
TarfHkeSeas4plotSeasAgain[1,2:dim(TarfHkeSeas4plotSeasAgain)[2]]<-TarfHkeSeas4plotSeasAgain[1,2:dim(TarfHkeSeas4plotSeasAgain)[2]]/TarfHkeSeas4plotSeasAgain[5,2:dim(TarfHkeSeas4plotSeasAgain)[2]]
TarfHkeSeas4plotSeasAgain[2,2:dim(TarfHkeSeas4plotSeasAgain)[2]]<-TarfHkeSeas4plotSeasAgain[2,2:dim(TarfHkeSeas4plotSeasAgain)[2]]/TarfHkeSeas4plotSeasAgain[5,2:dim(TarfHkeSeas4plotSeasAgain)[2]]
TarfHkeSeas4plotSeasAgain[3,2:dim(TarfHkeSeas4plotSeasAgain)[2]]<-TarfHkeSeas4plotSeasAgain[3,2:dim(TarfHkeSeas4plotSeasAgain)[2]]/TarfHkeSeas4plotSeasAgain[5,2:dim(TarfHkeSeas4plotSeasAgain)[2]]
TarfHkeSeas4plotSeasAgain[4,2:dim(TarfHkeSeas4plotSeasAgain)[2]]<-TarfHkeSeas4plotSeasAgain[4,2:dim(TarfHkeSeas4plotSeasAgain)[2]]/TarfHkeSeas4plotSeasAgain[5,2:dim(TarfHkeSeas4plotSeasAgain)[2]]
TarfHkeSeas4plotSeasAgain[5,2:dim(TarfHkeSeas4plotSeasAgain)[2]]<-TarfHkeSeas4plotSeasAgain[5,2:dim(TarfHkeSeas4plotSeasAgain)[2]]/TarfHkeSeas4plotSeasAgain[5,2:dim(TarfHkeSeas4plotSeasAgain)[2]]
TarfHkeSeas4plotSeasAgain<-melt(TarfHkeSeas4plotSeasAgain[1:4,], id.vars=c("season"))
names(TarfHkeSeas4plotSeasAgain)<-c("season","MetAgZone","factor")

FactorsSeasonsAgain<-ggplot(TarfHkeSeas4plotSeasAgain, aes(x=MetAgZone,y=factor))+
  geom_point()+
  facet_wrap(~season, ncol=1)+
  xlab("M?tier")+
  ylab("Factor")+
  expand_limits(y = 0)+
  theme_minimal(base_size=18)+
  theme(axis.text.x = element_text(angle = 45, hjust=1),axis.title.y=element_text(angle=0),panel.grid.major = element_line(size = 2), legend.key.width=unit(2,"cm"))
FactorsSeasonsAgain


# les nouveaux target factors m?tiers

subset(TarfHkeSeas4plot, season=="all")

# les nouveaux target factors saison

4*TarfHkeSeasOnly$factor

# Les target factors corrig?s pour la sole

test <- sacroisSSdoublonRed[sacroisSSdoublonRed$ESP_COD_FAO=="SOL",]

# = Facteur de standardisation pour chaque m?tier p?chant de la sole. On les a pour les m?tiers qui nous posaient probl?me.
vmoysole<-aggregate(cbind(LE_KG,LE_EFF)~MetAgZone,test,FUN=sum)
vmoysole$LdivE<-vmoysole$LE_KG/vmoysole$LE_EFF
vmoysole$factor<-vmoysole$LdivE/sum(vmoysole$LdivE)

MetierFiletSoleInterCValue= 0.296018745

vmoysole$vsolestd = vmoysole$factor * 0.296018745 / subset(vmoysole, MetAgZone == "Metier_FiletSole_InterC")$factor

subset(vmoysole, MetAgZone%in%c("Metier_FiletSole_NordIntPC","Metier_PalangreMixte_NordC"))

save(FactorsMetier,FactorsMetier2,FactorsSeasons,TarfHkeSeas4plot,TarfHkeSeasOnly, FactorsSeasonsAgain,vmoysole,file=paste(outPathnewBase,"TargetfactorsAccessibilties.Rdata",sep=""))#FPlotArea
