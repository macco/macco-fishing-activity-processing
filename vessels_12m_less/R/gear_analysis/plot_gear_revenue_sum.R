################################################################################
### plot revenue as a proportion of the cumulative sum
################################################################################

threshold_euro_prop_gear <- data.frame(
  year = factor(year_vec),
  threshold_euro_prop_gear = threshold_euro_prop_gear_fixed)

###-----------------------------------------------------------------------------
### create a data frame with gear and revenue only
df_gear <- df_eflalo %>%
  dplyr::select(REF_YEAR, VE_FLT, euro, kg) %>%
  group_by(VE_FLT, REF_YEAR) %>%
  summarise(euro = sum(euro),
            kg = sum(kg))  %>%
  group_by(REF_YEAR) %>%
  arrange(REF_YEAR, desc(euro)) %>%
  mutate(cumSumEuro = cumsum(euro),
         sumEuro = sum(euro),
         propEuro_cumSum = round(cumSumEuro/sumEuro, digits = 3)) %>%
  arrange(REF_YEAR, propEuro_cumSum) %>%
  mutate(VE_FLT = fct_reorder2(VE_FLT, propEuro_cumSum, REF_YEAR))


###-----------------------------------------------------------------------------
### threshold above which gear are discarded given their revenue
### df_threshold_euro_prop_gear set in 0_control_file.R
if (class(threshold_euro_prop_gear) == "data.frame") {

  df_gear <- left_join(df_gear, threshold_euro_prop_gear,
                       by = c("REF_YEAR" = "year")) %>%
    mutate(gear_selection = ifelse(threshold_euro_prop_gear > propEuro_cumSum,
                                   "keep", "discard"))
}

if (class(threshold_euro_prop_gear) == "numeric") {
  df_gear <- df_gear %>%
    mutate(gear_selection = ifelse(threshold_euro_prop_gear > propEuro_cumSum,
                                   "keep", "discard"))
}

###-----------------------------------------------------------------------------
### make plot for proportion of revenue in euro per gear and year
list_plot_gear_select <- purrr::map(.x = year_vec,
                                    .f = ~{
                                      filter(df_gear, REF_YEAR == .x) %>%
                                        ungroup() %>%
                                        mutate(VE_FLT = fct_reorder(VE_FLT, propEuro_cumSum)) %>%
                                        ggplot(.,  aes(x = VE_FLT,  y = propEuro_cumSum,
                                                       fill = gear_selection)) +
                                        geom_col() +
                                        ylab("Proportion in euros") +
                                        xlab("Gears") +
                                        ylim(0, 1) +
                                        geom_hline(aes(yintercept = threshold_euro_prop_gear)) +
                                        facet_grid( ~ REF_YEAR) +
                                        theme(axis.text.x = element_text(angle = 90, vjust = 0.5,
                                                                         hjust = 1, size= 7),
                                              legend.background = element_rect(fill = "lightblue",
                                                                               size = 0.4,
                                                                               linetype = "solid",
                                                                               colour = "darkblue"),
                                              legend.title = element_blank(),
                                              legend.position=c(0.9, 0.7))
                                    })
### print plot
if (show_plot) {
  sapply(list_plot_gear_select, print)
}

###-----------------------------------------------------------------------------
### save plots if save_plot == TRUE
purrr::map2(.x = year_vec,
            .y = list_plot_gear_select,
            .f = ~{
              f_save_plot(.y,
                          file_name = paste0("gear_select_", .x),
                          path = path_figure,
                          save = save_plot)})

### save list of plots as rds file
saveRDS(list_plot_gear_select,
        file = paste0(path_fishery_data_tidy_plots,
                      "/list_plot_gear_select.rds"))

###-----------------------------------------------------------------------------
### filter data frame given the threshold
df_gear_selected <- df_gear %>%
  filter(threshold_euro_prop_gear > propEuro_cumSum) %>%
  mutate(propEuro = euro/sumEuro) %>%
  dplyr::select(VE_FLT, REF_YEAR) %>%
  droplevels()

###-----------------------------------------------------------------------------

## List gear selected
df_gear_selected_year <- df_gear_selected %>%
  summarise(
    nb= n()
  )

mean_df_gear_selected_year <- mean(df_gear_selected_year$nb)

unique(df_sacrois$VE_FLT)
unique(df_gear_selected$VE_FLT)

