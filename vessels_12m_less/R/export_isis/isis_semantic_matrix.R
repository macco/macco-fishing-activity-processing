###############################################################################
# dealing with ISIS semantic matrix

# file specifications:
# 1st line = shape of the matrix
# n following lines (n as number of dimensions, also number of categorical variables):
# 	all levels of each variable, separated by comma, preceded by variable name with colon
# the rest = data frame as csv format with semicolon separator
# 	categorical variables values must be nominally numbered, not stores as character strings
# 	attention in java, index starts from 0
###############################################################################

import::here(.from = magrittr, "%>%", extract, extract2)
import::here(.from = dplyr, anti_join)
import::here(.from = purrr, imap_chr, imap_dfc)

#' @title write a data frame as semantic matrix for ISIS
#' @examples write_as_semantic_matrix(yolo, "tmp/yolo.txt")
#'
#' @param df data frame
#' @param filename file path
#' @param val_col characters vector of length 1; column holding numeric value to fill the matrix
#' @param var_cols characters vector; columns holding dimensions name of the matrix
#' @param complete boolean; should missing combinations of var_cols included (filled with 0)?
#'
#' @return none; write a data frame as semantic matrix for ISIS
#' @export
write_as_semantic_matrix <- function(df, filename, val_col = NULL, var_cols = NULL, complete = TRUE, encoding = "UTF-8") {

	# if val_col not given, than assign as any numeric column (float not integer !!)
	if (is.null(val_col)) val_col <- names(df)[sapply(df, is.numeric)]
	if (length(val_col) != 1L) stop("accept only 1 column of value")

	# if var_cols not given, than assign as all columns other than val_col
	if (is.null(var_cols)) var_cols <- names(df)[names(df) != val_col]
	if (any(duplicated(df[, var_cols]))) stop("combinations of categorical columns must be unique")

	tmp <- # this object will be the center of action
		if (isFALSE(complete) || is.vector(df[, var_cols])) { # cas avec 1 seule colonne
			df[, c(var_cols, val_col)]
		} else {
			# the result matrix must have all combinations of categorical variables
			# if the original data miss a few then we fill with 0
			missing_combinations <- lapply(df[, var_cols, drop = FALSE], FUN = unique) %>%
				expand.grid() %>%
				anti_join(df[, var_cols]) %>%
				suppressMessages()
			if (nrow(missing_combinations) > 0) {
				missing_combinations[[val_col]] <- 0
			} else {
				missing_combinations <- NULL
			}

			# data frame with all combinations
			rbind(df[, c(var_cols, val_col)], missing_combinations)
		}
	tmp_var_cols <- tmp[, var_cols, drop = FALSE] # safeguard in case of only 1 column

	if (nrow(tmp) != nrow(unique(tmp_var_cols)))
		stop("unknown error of combinations")

	# convert to factor
	# because it has interesting properties: nlevels & levels
	if (any(sapply(tmp[, var_cols], is.factor)))
		warnings("non-factor columns will be converted to non-factor, any order within those columns will be overridden")
	tmp[, var_cols] <- lapply(tmp_var_cols, as.factor)
	tmp <- droplevels(tmp)
	tmp_var_cols <- tmp[, var_cols, drop = FALSE] # safeguard in case of only 1 column

	# 1st line = shape of the matrix
	first_line <- sapply(tmp_var_cols, nlevels) %>%
		paste0(collapse = ",") %>%
		paste0("[", ., "]")

	# n following lines = all levels of each variable
	n_lines <- lapply(tmp_var_cols, FUN = levels) %>%
		imap_chr(function(val, id) paste0(id, ":", paste0(val, collapse = ","))) %>%
		setNames(NULL)

	tmp[, var_cols] <- lapply(tmp_var_cols, function(x) as.integer(x) - 1L)
	# -1 because java index starts from 0

	tc <- textConnection("my_print", open = "w", local = TRUE)
	write.table(tmp, file = tc, sep = ";", row.names = FALSE, col.names = FALSE)
	close(tc)

	res <- c(first_line, n_lines, my_print)
	if (.Platform$OS.type == "windows" && encoding != "UTF-8") res <- iconv(res, from = "UTF-8", to = encoding, sub = "byte")
	writeLines(res, filename, useBytes = TRUE)
}

#' @title read a semantic matrix as data frame
#'
#' @param filename file path
#' @param val_col characters vector of length 1; name for the column filling the matrix
#'
#' @return data frame
#' @export
parse_semantic_matrix <- function(filename, val_col = "value", encoding = "UTF-8") {
	raw_text <- readLines(filename, encoding = encoding) %>%
		extract(. != "") # empty lines

	# détecter quelles lignes avec données numériques
	nb_dims <- raw_text[1] %>% # 1st line
		substr(., 2, nchar(.) - 1) %>% # enlever les crochets
		strsplit(., split = ",", fixed = TRUE) %>%
		extract2(1) %>%
		length()
	idx_lines_with_data <- rep("\\d+;", times = nb_dims) %>%
		paste0(collapse = "") %>%
		paste0("^", .) %>% # exemple: "^\\d+;\\d+;" si nb_dims = 2
		grepl(., raw_text)

	data_text <- raw_text[idx_lines_with_data]
	header_lines <- raw_text[!idx_lines_with_data][-1] # 1re ligne = shape = sert à rien
	var_cols <- gsub(":.+$", "", header_lines) # don’t take the last column (val_col) yet!

	df <- read.table(
		text = paste0(data_text, collapse = "\n"), sep = ";",
		header = FALSE, check.names = FALSE, col.names = c(var_cols, val_col)
	)

	categorical_val <- lapply(header_lines, function(x) {
		y <- gsub("^[^:]+:", "", x)
		res <- strsplit(y, split = ",", fixed = TRUE)
		return(unlist(res))
	}) %>% setNames(var_cols)

	df[, var_cols] <- imap_dfc(categorical_val, function(val, id) val[1 + df[[id]]])
	# +1 because R index starts from 1
	return(df)
}
