###############################################################################
# divers fonctions utilitaires
###############################################################################

import::here(.from = magrittr, "%>%")
import::here(.from = dplyr, recode)
import::here(.from = stringi, stri_detect_fixed, stri_split_fixed, stri_replace_first_fixed)

#' @title séparateur champs exotique
#' @export
sep_char <- " @ "

#' @title convert to factor of months name abbreviated
#' @export
to_month_name <- function(month_number) factor(month.abb[as.integer(month_number)], level = month.abb)

#' @title shortcut to number of day per month
#' @export
get_days_in_month <- function(month_number) recode(month_number,
	"1" = 31, "2" = 28, "3" = 31, "4" = 30, "5" = 31, "6" = 30,
	"7" = 31, "8" = 31, "9" = 30, "10" = 31, "11" = 30, "12" = 31
)

#' @title match months to quarter
#' @export
df_quarter_month <- data.frame(MOIS = 1:12, quarter = rep(1:4, each = 3))

#' @title pretty print lm coef
#'
#' @param lm_model an object of class lm
#' @param transform_fun function; applied on estimates
#'
#' @return a data frame of variables, each level of categorical variables and their estimates
#' @export
get_lm_coef <- function(lm_model, transform_fun = NULL) {
	lm_coef <- coef(lm_model) # estimate of all param
	lm_coef_lab <- names(lm_coef) # raw label of estimates e.g. c("metierOTB_OCT:ESP_COD_FAONEP")
	lm_vars <- lm_model$xlevels # list of vars with levels for categorical variables e.g. list("metier" = c("metierOTB_OCT:ESP_COD_FAONEP"))
	lm_vars_lab <- attr(lm_model$terms, "term.labels") # vector of vars in original formula e.g. c("metier")
	lm_vars_lab_id <- lm_model$assign # index of lm_vars_lab in lm_coef
	lm_contrs <- lm_model$contrasts # contrast of categorical variables

	# broadcast lm_vars_lab to have same length as lm_coef
	res_lm_vars <- c("(Intercept)", lm_vars_lab[lm_vars_lab_id])
	res_lm_terms <- character(length(lm_vars_lab_id)) # empty vector to be filled later
	res_lm_terms[1] <- "(Intercept)"

	# look for interaction param to process differently
	idx_coef_interact <- stri_detect_fixed(lm_coef_lab, ":")

	#------------------------------------------------------
	# process non-interaction terms

	no_inter_terms <- lm_coef_lab[!idx_coef_interact][-1] # first is intercept e.g. c("metierOTB_OCT:ESP_COD_FAONEP")
	no_inter_vars <- res_lm_vars[!idx_coef_interact][-1] # first is intercept e.g. c("metier")
	res_no_inter_terms <- clean_lm_terms(no_inter_vars, no_inter_terms, lm_contrs, lm_vars)
	res_lm_terms[!idx_coef_interact][-1] <- res_no_inter_terms

	#------------------------------------------------------
	# process interaction terms

	interact_terms <- lm_coef_lab[idx_coef_interact]
	interact_vars <- res_lm_vars[idx_coef_interact]

	# matrix
	res_interact_terms <- stri_split_fixed(interact_terms, ":", simplify = TRUE)
	interact_vars_bis <- stri_split_fixed(interact_vars, ":", simplify = TRUE)
	stopifnot(all(dim(res_interact_terms) == dim(interact_vars_bis)))

	res_interact_terms <-
		mapply( # apply for each column of 2 matrices
			clean_lm_terms, interact_vars_bis, res_interact_terms,
			MoreArgs = list(my_contrs = lm_contrs, my_vars = lm_vars)
		) %>%
		matrix(
			nrow = nrow(res_interact_terms),
			ncol = ncol(res_interact_terms)
		) %>%
		apply(
			MARGIN = 1,
			FUN = function(x) paste0(x, collapse = ":")
		)

	res_lm_terms[idx_coef_interact] <- res_interact_terms

	#------------------------------------------------------
	names(lm_coef) <- NULL
	if (!is.null(transform_fun)) {
		transform_fun <- match.fun(transform_fun)
		lm_coef <- transform_fun(lm_coef)
	}
	return(data.frame(
		var = res_lm_vars,
		term = res_lm_terms,
		estimate = lm_coef
	))
}

#' @title remove mentions of vars in terms
#' @details
#'   given term `"metierOTB_OCT:ESP_COD_FAONEP"` and var `"metier:ESP_COD_FAO"` return `"OTB_OCT:NEP"`
#'   also process the case of sum-to-0 contrast when levels is replaced by numbers
#' @keywords internal
clean_lm_terms <- function(my_var, my_coef, my_contrs, my_vars) {
	stopifnot(length(my_var) == length(my_coef))
	my_val <- stri_replace_first_fixed(my_coef, my_var, "")
	res <- character(length(my_val))
	for (i in unique(my_var)) {
		idx <- my_var == i
		idx_i <- suppressWarnings(as.integer(my_val[idx]))
		is_label_replaced <- my_contrs[[i]] == "contr.sum" && all(!is.na(idx_i))
		res[idx] <-
			if (is_label_replaced) my_vars[[i]][1+idx_i] # first label skipped
			else my_val[idx]
	}
	return(res)
}
