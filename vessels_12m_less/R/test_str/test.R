###-----------------------------------------------------------------------------
dfs_prop_metier_zone_isis %>%
  mutate(fr.ifremer.isisfish.types.Month = as.numeric(as.character(MONTH)) - 1, # mois dois commencer de 0
         Metier = as.character(metier_zone_fleet_isis)) %>%
  split(., ~ strategy_isis) -> test

test_1 <- test[[1]]

unique(test_1$metier_zone_isis)


test_1_fleet <- df_eflalo %>%
  filter(fleet_isis %in% test_1$fleet_isis) %>%
  droplevels()

unique(test_1_fleet$metier_zone_isis) %in%
  unique(test_1$metier_zone_isis)


unique(test_1_fleet$metier_zone_isis)




test_j <- dfs_metier_zone_isis_per_fleet_month %>%
  filter(fleet_isis %in% test_1$fleet_isis)
unique(test_j$metier_zone_isis)
