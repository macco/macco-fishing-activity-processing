################################################################################
### Set SUPER_QUAM based on expert knowledge
### Assignment : 1 = Channel ; 2 = BoB North ; 3 = BoB South
################################################################################

cat(bgBlue("1_1: Load SUPER QUAM navire data \n"))

if (file.exists(paste0(path_fishery_data_tidy_vessels, "/df_SUPER_QAM.rds"))) {
  df_SUPER_QAM <- read_rds(paste0( path_fishery_data_tidy_vessels,
                                   "/df_SUPER_QAM.rds"))
} else {
  df_SUPER_QAM <- data.frame(
    SUPER_QAM_LIB_id = c(3, 3, 1, 2, 3, 2, 2, 2, 1, 1, 3, 2, 2, 3, 2, 3, 3, 3,
                         1, 1, 1, 1, 1, 1, 2, 2, 2, 1, 1, 2, 2, 4, 4, 4, 4),
    QAM_LIB = c("Bayonne", "Marennes", "Paimpol", "Guilvinec", "La Rochelle",
                "Vannes", "Audierne", "Auray", "Saint-Brieuc", "Cherbourg",
                "Ile d'Oléron", "Lorient", "Concarneau", "L'Ile-d'Yeu",
                "Saint-Nazaire", "Bordeaux", "Les Sables-d'Olonne", "Arcachon",
                "Morlaix", "Dieppe", "Saint-Malo", "Caen", "Boulogne-sur-Mer",
                "Fécamp", "Brest", "Camaret", "Douarnenez", "Dunkerque",
                "Le Havre", "Nantes", "Noirmoutier",
                "Sete", "Port-Vendres", "Martigues", "Marseille")) %>%
    mutate(SUPER_QAM_LIB = factor(SUPER_QAM_LIB_id, labels = c("channel",
                                                               "bob_north",
                                                               "bob_south",
                                                               "medit")
                                  ))

  saveRDS(df_SUPER_QAM, file = paste0(path_fishery_data_tidy_vessels, "/df_SUPER_QAM.rds"))
}


### extract the QUAM LIB associated to the bob
df_QAM_LIB_BoB <- df_SUPER_QAM

### merge with reference navire table
df_ref_nav <- left_join(df_ref_nav, df_SUPER_QAM,
                        by = "QAM_LIB") %>%
  mutate(SUPER_QAM_LONG = factor(paste0(SUPER_QAM_LIB, NAVLC4_COD)))

df_ref_nav <- df_ref_nav %>%
  mutate(SUPER_QAM_LONG = factor(paste0(SUPER_QAM_LIB, SEGMENT))) %>%
  droplevels() %>%
  drop_na(SUPER_QAM_LIB) #delete Mediteranean and DOM-TOM QAM-LIB
