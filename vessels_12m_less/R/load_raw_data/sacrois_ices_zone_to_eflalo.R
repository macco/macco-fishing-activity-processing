################################################################################
### Transfrom sacrois data into eflalo format
### script build from SACROIS_EFLALO_SS_doublonLE_ID.R with
### authors M. Bertignac, S. Mahevas, Y. Vermard developped in 2013
### Major update from Jean-Baptiste Lecomte
################################################################################

if (file.exists(paste0(path_eflalo_data, "/df_eflalo_", y, ".rds")) &
    load_raw_data == FALSE) {

  cat("Year:", y,
      "- eflalo rds file already generated and available at",
      path_eflalo_data, "\n")

  df_eflalo <- read_rds(paste0(path_eflalo_data,
                               "/df_eflalo_", y, ".rds"))

} else {
  cat("Year:", y, "- generating eflalo data \n")
  ###---------------------------------------------------------------------------
  ### remove duplicated rows
  df_sacrois_4_eflalo <- read_rds(paste0(path_sacrois_data_tidy,
                                         "/df_sacrois_",
                                         y, ".rds")) %>%
    mutate(ESP_COD_FAO_KG = paste0("LE_KG_", ESP_COD_FAO, sep = ""),
           ESP_COD_FAO_ER = paste0("LE_EURO_", ESP_COD_FAO, sep = "")) %>%
    ungroup() %>%
    dplyr::select(-ORIGINE_ESP_COD_FAO)

  ###---------------------------------------------------------------------------
  ### Spread KG and EURO to have one columns per species for eflalo format

  dfw_sacrois_kg <- pivot_wider(dplyr::select(df_sacrois_4_eflalo, LE_ID, ESP_COD_FAO_KG, LE_KG),
                                # names_prefix = "LE_KG_",
                                names_from = ESP_COD_FAO_KG,
                                values_from = LE_KG,
                                values_fill = 0)

  dfw_sacrois_euro <- pivot_wider(dplyr::select(df_sacrois_4_eflalo, LE_ID, ESP_COD_FAO_ER, LE_EURO),
                                  # names_prefix = "LE_EURO_",
                                  names_from = ESP_COD_FAO_ER,
                                  values_from = LE_EURO,
                                  values_fill = 0)

  ###---------------------------------------------------------------------------
  ### Join everything

  df_sacrois_4_eflalo_bis <- df_sacrois_4_eflalo %>%
    dplyr::select(-LE_ID_EFF, -LE_ID_ESP, -ESP_COD_FAO,
                  -ESP_COD_FAO_KG, -ESP_COD_FAO_ER,
                  -LE_KG, -LE_EURO) %>%
    distinct()

  dfw_sacrois <- left_join(df_sacrois_4_eflalo_bis, dfw_sacrois_kg, by = "LE_ID")
  dfw_sacrois <- left_join(dfw_sacrois, dfw_sacrois_euro, by = "LE_ID")

  ### sacrois to eflalo
  # df_eflalo <- vmstools::poolEflaloSpecies(eflalo = dfw_sacrois,
  #                                          threshold = 0.00001,
  #                                          code = "ZZZZ")
  df_eflalo <- dfw_sacrois

  ###---------------------------------------------------------------------------
  ### save df_eflalo in rds format
  cat("save eflalo data to ", path_eflalo_data, "\n")

  df_eflalo <- df_eflalo %>%
    mutate(REF_YEAR = factor(REF_YEAR))

  saveRDS(df_eflalo,
          file = paste0(path_eflalo_data, "/df_eflalo_", y, ".rds"))

  rm(list = c("dfw_sacrois",
              "dfw_sacrois_euro",
              "dfw_sacrois_kg",
              "df_sacrois_4_eflalo",
              "df_sacrois_4_eflalo_bis"))
  gc()
}


df_eflalo_all_year <- bind_rows(df_eflalo_all_year, df_eflalo) %>%
  mutate_at(vars(contains("KG")), ~ replace_na(.x, 0)) %>%
  mutate_at(vars(contains("EURO")), ~ replace_na(.x, 0)) %>%
  mutate(euro = rowSums(dplyr::select(., contains("EURO")), na.rm = TRUE),
         kg = rowSums(dplyr::select(., contains("KG")), na.rm = TRUE),
         REF_YEAR = factor(REF_YEAR))



