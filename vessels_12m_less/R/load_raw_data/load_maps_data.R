################################################################################
### Load maps data for computing effort per zones : SIH DATA
################################################################################
source("../shared_scripts/f_map_var.R")

###-----------------------------------------------------------------------------
### load ICES rectangles
source('../shared_scripts/sf_ices_rectangle.R')

###-----------------------------------------------------------------------------
### load coast line
source('../shared_scripts/sf_coast_line.R')

###-----------------------------------------------------------------------------
### load coast line
# source('../shared_scripts/sf_ices_rectangle_coast_line.R')

###-----------------------------------------------------------------------------
### load isis grid
source('../shared_scripts/load_spatial_grid_isis_fish.R')

###-----------------------------------------------------------------------------
### load shom limits
source('../shared_scripts/sf_shom_limits.R')

###-----------------------------------------------------------------------------
### empty map used as a base plot for all maps
source('../shared_scripts/empty_map.R')

###-----------------------------------------------------------------------------
### merge spatial data with eflalo

# plot_empty_map +
#   geom_sf(data = sf_ices_rec)
#
# ggplot() +
#   geom_sf(data = sf_ices_ss_rec, aes(fill = as.factor(ID))) +
#   theme(legend.position = "none")


###-----------------------------------------------------------------------------

### keep isis grid square that match rect stat
isis_grid_sf_centroid <- isis_grid_sf %>%
  st_centroid()

sf_ices_rec_isis_grid <- st_join(isis_grid_sf_centroid, sf_ices_rec)


###-----------------------------------------------------------------------------
### empty map used as a base plot for all maps
# source('R/fun/spatial/empty_map.R')

###-----------------------------------------------------------------------------
### merge spatial data with eflalo
#source('R/load_raw_data/merge_spatial_data_with_eflalo.R')

# plot_empty_map +
#   geom_sf(data = sf_ices_rec)
#
# ggplot() +
#   geom_sf(data = sf_ices_ss_rec, aes(fill = as.factor(ID))) +
#   theme(legend.position = "none")
