
###-----------------------------------------------------------------------------
### Compute summary for plotting

df_metier_isis_landing_all_years_fleet_isis <- pivot_longer(df_eflalo,
                                                              cols = starts_with("LE_KG"),
                                                              names_to = "Species") %>%
  filter(value > 0) %>%
  dplyr::select(Species, metier_isis, fleet_isis, nav_size, value) %>% #value= landed quantity
  group_by(metier_isis, Species, fleet_isis, nav_size) %>%
  mutate(Species = as_factor(str_sub(Species, 7, 9)),
         Species = fct_relevel(Species, "OTH", after = Inf)) %>%
  summarise(mean_landing = mean(value))


dfs_metier_isis_landing_fleet_isis <- df_metier_isis_landing_all_years_fleet_isis %>%
  group_by(metier_isis, fleet_isis, nav_size) %>%
  mutate(total_landing = sum(mean_landing),
         prop_landing_species = mean_landing / total_landing)


### Compute summary for plotting
dfs_metier_isis_landing_fleet_isis_noOTH <- df_metier_isis_landing_all_years_fleet_isis %>%
  filter(Species != "OTH") %>%
  group_by(metier_isis, fleet_isis, nav_size) %>%
  mutate(total_landing = sum(mean_landing)) %>%
  mutate(prop_landing_species = mean_landing / total_landing)


###-----------------------------------------------------------------------------
### plot landings profiles
list_plots_prop_landings_metier_isis_fleet_isis <- dfs_metier_isis_landing_fleet_isis %>%
  group_by(fleet_isis) %>%
  group_split() %>%
  setNames(., nm = map(.x = ., ~ glue("{first(.x$fleet_isis)}"))) %>%
  purrr::map(.x = ., ~ ggplot(data = .x,
                              aes(x = metier_isis,
                                  y = prop_landing_species,
                                  fill = Species,
                                  group = Species)) +
               theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust = 1)) +
               geom_col() +
               xlab("Metier isis") +
               ylab("Proportion of mean landings (kg)") +
               scale_fill_manual(values = scale_color_species_isis) +
               facet_grid(~nav_size, scales = "free_x") )

saveRDS(list_plots_prop_landings_metier_isis_fleet_isis,
        file = here(path_data_tidy_plots,
                    "/list_plots_prop_landings_metier_isis_fleet_isis.rds"))


###-----------------------------------------------------------------------------
### plot landings profiles
list_plots_prop_landings_metier_isis_fleet_isis_noOTH <- dfs_metier_isis_landing_fleet_isis_noOTH %>%
  group_by(fleet_isis) %>%
  group_split(.keep = TRUE) %>%
  setNames(., nm = map(.x = ., ~ glue("{first(.x$fleet_isis)}"))) %>%
  purrr::map( ~ ggplot(data = .x,
                              aes(x = metier_isis,
                                  y = prop_landing_species,
                                  fill = Species,
                                  group = Species)) +
               theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust = 1)) +
               geom_col() +
               xlab("Metier isis") +
               ylab("Proportion of mean landings (kg)") +
               scale_fill_manual(values = scale_color_species_isis) +
               facet_grid(~nav_size, scales = "free_x"))

saveRDS(list_plots_prop_landings_metier_isis_fleet_isis_noOTH,
        file = here(path_data_tidy_plots,
                    "/list_plots_prop_landings_metier_isis_fleet_isis_noOTH.rds"))
