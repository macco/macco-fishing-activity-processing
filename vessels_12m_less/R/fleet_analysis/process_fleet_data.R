################################################################################
### Summarize fleet_isis data for plotting
################################################################################
list_fleet_isis <- list()

###-----------------------------------------------------------------------------
### extract unique Vessel, year and fleet_isis
df_fleet_isis_vessel <- df_eflalo %>%
  dplyr::select(VE_REF, fleet_isis, REF_YEAR) %>%
  distinct()

list_fleet_isis$df_fleet_isis_vessel <- df_fleet_isis_vessel

###-----------------------------------------------------------------------------
### count number of vessels per fleet_isis and year
dfs_fleet_isis_nvessel <- df_fleet_isis_vessel %>%
  group_by(REF_YEAR, fleet_isis) %>%
  summarise(n_vessels = n())

list_fleet_isis$dfs_fleet_isis_nvessel <- dfs_fleet_isis_nvessel

### Check for missing data
missing_QAM <- levels(dfs_fleet_isis_nvessel$fleet_isis)[str_starts(levels(dfs_fleet_isis_nvessel$fleet_isis), "NA")]

if (length(unique(missing_QAM)) > 0) {
  cat(bgMagenta("fleet_isiss with missing QAM \n"))
  print(missing_QAM)
}


### Check for missing data
missing_vessels_lengths <- levels(dfs_fleet_isis_nvessel$fleet_isis)[str_ends(levels(dfs_fleet_isis_nvessel$fleet_isis), "NA")]

if (length(unique(missing_vessels_lengths)) > 0) {
  cat(bgMagenta("fleet_isiss with missing length \n"))
  print(missing_vessels_lengths)
}

###-----------------------------------------------------------------------------
### extract vessels with more than one fleet_isis = which is not possible in ISIS
dfs_fleet_isis_vessel_nyear <- df_fleet_isis_vessel %>%
  group_by(VE_REF, fleet_isis) %>%
  distinct() %>%
  summarise(n_year = n())

list_fleet_isis$dfs_fleet_isis_vessel_nyear <- dfs_fleet_isis_vessel_nyear


###-----------------------------------------------------------------------------
### Vessels with two fleet_isiss
dfs_fleet_isis_vessel_2fleet_isiss <- dfs_fleet_isis_vessel_nyear %>%
  group_by(VE_REF) %>%
  filter(n() > 1) %>%
  droplevels()


n_vessels_with_2_fleet_isiss <- length(unique(dfs_fleet_isis_vessel_2fleet_isiss$VE_REF))

if (n_vessels_with_2_fleet_isiss > 0) {
 cat(bgMagenta(paste(n_vessels_with_2_fleet_isiss, "vessels with two different fleet_isiss \n")))

df_fleet_isis_vessel_2feets <- df_fleet_isis_vessel %>%
  filter(VE_REF %in% dfs_fleet_isis_vessel_2fleet_isiss$VE_REF) %>%
  arrange(VE_REF)
}


### ----------------------------------------------------------------------------
### save plots as rds file
dir.create( glue("{path_data_tidy}/fleet"), showWarnings = FALSE)
saveRDS(list_fleet_isis,
        file = glue("{path_data_tidy}/fleet/list_fleet_isis.rds"))

