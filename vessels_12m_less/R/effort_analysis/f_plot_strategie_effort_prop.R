f_plot_strategies_effort_prop <- function(df_data, strategie){

    df_data <- df_data %>%
    group_by(MONTH) %>%
    filter(strategie == strategie) %>%
    mutate(metier = factor(metier))

  ggplot(data = df_data,
         aes(x = MONTH,
             y = prop_effort_metier,
             fill = metier)) +
    geom_col() +
    theme(legend.position = "none") +
    facet_grid(~strategie) +
    xlab("") +
    ylab("Proportion of effort") +
    theme(axis.text = element_text(size = 5), legend.title = element_blank())

}
