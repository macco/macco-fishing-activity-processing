---
title: 'Rapport pour le projet Macco - 5 : Exploration des données spatiales SIH dans
  le cadre de l''analyse de l''activité de pêche des navires de moins de 12 m'
author: "Auriane"
date: "20/05/2022"
output:
  pdf_document: default
  html_document:
    df_print: paged
---

```{r setup, include=FALSE}
#Sert de reglage pour tous les autres, option de tchunk, par ex. permet d'importer les données
knitr::opts_chunk$set(fig.width=12, fig.height=8, warning=FALSE, message=FALSE)
```

# 1. Présentation de la synthèse 

## 1.1. Objectif 

Ce document est réalisé dans le cadre de la tache 1 du projet « Macco ». Il représente un rapport intermédiaire dans le cadre d'un stage de m2 sur l’amélioration de la paramétrisation du modèle de dynamique de flottille ISIS-Fish pour la pêcherie démerso-benthique du golfe de Gascogne. 

L’objectif de ce rapport est de présenter les données spatiales disponibles dans le jeu de données Sacrois. Les données spatiales des navires de moins de 12 mètres se limitent donc au données déclaratives, pour chaque séquence de pêche la maille occupée est renseignée. La maille correspond à une rectangle statistique CIEM avec une taille de 30 miles par 30 miles. Pour le trait de côte du nord est atlantique français les sous-rectangle statistique CIEM sont accessibles, ils ont été créés par le SIH (Système d’Information Halieutique) pour permettre de collecter et de restituer la donnée halieutique. Ces sous rectangles sont compris dans les rectangles statistiques CIEM nord est Atlantique.

Ce document analyse dans un premier tempos à partir de quel flux de donnée les zones de séquence de pêche sont construites, puis la projection des rectangles et sous-rectangle statistique. L'objectif est de vérifier si l'utilisation des rectangles et sous-rectangles est cohérente dans le cadre de l'étude. 


## 1.2. Méthodologie

La synthèse repose sur une méthodologie standard de suivi des activités de navires au moyen d’un projet R développé par Jean-Baptiste Lecomte. 

```{r load-packages, include=FALSE}
utils::memory.limit(size = 500000)
fishery_name <- "bob_macco"
### boot r script: load package and function
source("R/fun/boot.R")
```


```{r load file data, include= FALSE}
### load control file specific to GdG_sol_hke_nep
source(here(path_fishery, "0_control_file.R"))
```


## 1.3. Sources de données 

Le présent document est une restitution des données disponibles au sein du SIH sur les navires de pêche français entre `r starting_year` et `r ending_year`. Les navires dont le port de rattachement se situe dans le golfe de Gascogne ont été extraits des documents : 

  * Référentiel navires : le référentiel Navires consolidé désigne les navires et les armateurs du programme “SIH”, il est alimenté par trois sources de données : le flux "Flotte de Pêche Communautaire" (Fpc), le flux issu des Affaires Maritimesest et le flux VMS. Les données extraites de ce fichier sont celles issuent du flux Fpc, qui référencent l'ensemble des données administratives des navires de pêche professionnelle de la flotte française et leurs caractéristiques techniques (source : SIH, 2015) ;
  
```{r load and show ref data, echo= FALSE, cache=TRUE}
df_ref_nav <- read_rds(paste0(path_fishery_data_tidy_vessels, "//df_ref_nav_",min(year_vec), "_", max(year_vec), ".rds")) #NEW
```

  * Données Sacrois : correspondent aux données de captures et d’efforts de pêche par navire estimées sur la base d’un algorithme de croisement des données de ventes, des données déclaratives et des données VMS (source : Ifremer, 2021 );  
  
Pour analyser la dynamique des navires de moins de 12 mètres, les données Sacrois sont fusionnées aux données du Référentiel navires afin de pouvoir filtrer par la suite les segments de taille. 

```{r load and show sacrois data, echo= FALSE}
df_sacrois <- read_rds(paste0(path_sacrois_data_tidy, "/df_sacrois_",
                                min(year_vec), "_",
                                max(year_vec), ".rds"))

df_eflalo <- read_rds(paste0(path_eflalo_data, "/df_eflalo_",
                               min(year_vec), "_",
                               max(year_vec), ".rds"))
```


  
# 2. Exploration des données : Qu'elles sont les données spatiales disponibles dans Sacrois? 

### 2.1. Comment les zones d'exploitation sont-elles construites ? 


Les données issues du fichier Sacrois proposent une variable `SECT_COD_ORIGINE` qui permet d'identifier la ou les sources qui ont permis au logiciel de construire le zonage d'estimer de la séquence de pêche.

Les sources disponibles pour estimer le zonage sont décrites dans le tableau ci-dessous. 

```{r show sacrois origine value, echo= FALSE, fig.path= "Liste des valeurs possibles pour la variable SECT_COD_ORIGINE dans Sacrois"}
vecteur1 <- c("VMS","SIPA","VMS-SIPA","SIH-SACROIS-HISTO","REF-CRIEE-DIVISION-CIEM","ACTIVITY","ACTIVITY_N1","ACTIVITY_N2") 
vecteur2 <- c("Le zonage proposé est déduit directement du flux VMS."," Le zonage est déduit directement du flux JBE-SACAPT/SACAPT.", "Le zonage proposé est déduit d’une confrontation des zonages observés dans les flux VMS et JBE-SACAPT/SACAPT", "Le zonage proposé est déduit de l’historique des marées « SACROIS » du navire (recherche dans les 6 mois précédents et les 6 mois à suivre du navire)." , "Le zonage proposé est déduit de la division CIEM  adjacente au lieu de la vente.", "Le zonage proposé est déduit des données « enquête calendrier d’activité mensuelle (IFREMER) » du navire pour l'année courante N.", "Le zonage proposé est déduit des données « enquête calendrier d’activité mensuelle (IFREMER) » du navire pour l'année N-1. Les zones du  calendrier d'activité du navire ne sont proposées que si le navire n'a pas changé d'armateur et n'a pas changé de quartier d'immatriculation.", "Le zonage proposé est déduit des données « enquête calendrier d’activité mensuelle (IFREMER) » du navire pour l'année N-2. Les zones du calendrier d'activité du navire ne sont proposées que si le navire n'a pas changé d'armateur et n'a pas changé de quartier d'immatriculation.")
tableau <- data.frame(x = vecteur1, y = vecteur2)

kable(tableau) 
```
Source : Flux SACROIS – v3.3.10 Février 2021 (Ifremer, 2021)



La figure représente l'évolution annuelle de la proportion de chaque flux de donnée pour la construction de l'entité  "séquence de pêche" dans les données Sacrois (Valeur de la variable `SECT_COD_ORIGINE`). 
Ces graphiques sont représentés pour les navires de moins de 7 mètres, 7 à 10 mètres et plus de 10 mètres. 

En moyenne, pour les trois segments confondus, les zones des séquences de pêche sont construites majoritairement au moyen du flux déclaratif (SIPA : 67,43±26.18 %) et du flux enquête calendrier d’activité (ACTIVITY : 31,32 ±26.15 %). Les zones construites au moyen du calendrier à de partir l’année N-1 ou N -2 et de l’équipement VMS représentent chacune en moyenne moins de 1 %.


Pour les navires de moins de 10 mètres, la part des flux déclaratifs dans la construction des zones est majoritaire jusqu’à 2009 pour être remplacée par le flux issu des données d’enquêtes du calendrier d’activité.


Les navires de moins de 7 mètres aucun flux VMS permet de construire des zones, pour les 7 à 10 mètres 0,32±0,44 %. La part du flux de donnée des VMS pour les navires de 10 à 12 mètres est de 2,26±1,80 %.


**ERREUR :**

Les trous au sein du graph sont des 'lacunes ', la variable `SECT_COD_ORIGINE` n'a pas de valeur pour l'ensemble des années / taille. Malgré plusieurs tuto utilisés je n'ai pas pu combler les trous. J'ai pourtant ajouté dans le tableau de donnée l'ensemble des valeurs pour chaque variable (année, origine et segment de taille) et attribué un 0  pour les valeurs vides ... Une idée ? 

```{r data exploration data origine, echo= FALSE}
##make a data frame of origine data
df_sacrois_ORI <- df_sacrois %>%
  dplyr::select(REF_YEAR, VE_REF, SECT_COD_ORIGINE, SEGMENT,SEQ_ID) %>% 
  distinct() %>%    #permet d'isoler par maree : est-ce que c'est bon ?
  group_by(REF_YEAR, SECT_COD_ORIGINE, SEGMENT) %>% 
  summarise(
    eff_FO = n()
  ) %>% 
  group_by(REF_YEAR, SEGMENT) %>% 
  mutate(sumFO = sum(eff_FO), # somme par annee
         propFO = round((eff_FO/sumFO), digits = 5)*100) 

##fill the table for empty value to avoid empty space in geom_area
ori <- unique(df_sacrois$SECT_COD_ORIGINE)
SEGMENT <- unique(df_sacrois$SEGMENT)
year <- unique(df_sacrois$REF_YEAR)
combi <- expand.grid(SECT_COD_ORIGINE= ori, SEGMENT= SEGMENT, REF_YEAR= year)

df_sacrois_ORI <- full_join(df_sacrois_ORI, combi, by=  c("SECT_COD_ORIGINE"= "SECT_COD_ORIGINE" , "SEGMENT" = "SEGMENT", "REF_YEAR"= "REF_YEAR")) %>% 
                            mutate(eff_FO= ifelse(is.na(eff_FO), 0, eff_FO)) %>% 
                            mutate(sumFO= ifelse(is.na(sumFO), 0, sumFO)) %>% 
                            mutate(propFO= ifelse(is.na(propFO), 0, propFO)) %>% 
                            mutate(SEGMENT = factor(SEGMENT, ordered = TRUE, 
                                   levels = c("[0-7[ m", "[7-10[ m", "[10-12[ m"))) %>% 
                            arrange(SECT_COD_ORIGINE, SEGMENT, REF_YEAR)

#Plot proportion od SECT_COD_ORIGINE data in sacrois data for each SEGMENT segment 
plot_ORI_sacrois <- ggplot(df_sacrois_ORI, aes(x= REF_YEAR, y= propFO, group = SECT_COD_ORIGINE, fill= SECT_COD_ORIGINE)) +
  geom_area(alpha=0.6 , size=0.5, colour= "grey") +
  scale_fill_viridis(discrete = T) +
  facet_grid(~SEGMENT) +
  ylab("% data source") +
  xlab("") +
  ylim(0,100) +
  theme(axis.text.x = element_text(angle = 90),
        legend.background = element_rect(fill = "white",
                                         size = 0.4,
                                         linetype = "solid",
                                         colour = "grey"))
print(plot_ORI_sacrois)

# Mean and sd of each origine data
mean_sacrois_ori <- tapply(df_sacrois_ORI$propFO, df_sacrois_ORI$SECT_COD_ORIGINE, mean)
sd_gis_sacrois_ori <- tapply(df_sacrois_ORI$propFO, df_sacrois_ORI$SECT_COD_ORIGINE, sd)

  ## By segment 

## Inferior at 7 meters
df_sacrois_ORI_Inf7m <- df_sacrois_ORI %>% 
  filter(SEGMENT %in% "[0-7[ m")
mean_sacrois_ori_Inf7m <- tapply(df_sacrois_ORI_Inf7m$propFO, df_sacrois_ORI_Inf7m$SECT_COD_ORIGINE, mean)
sd_gis_sacrois_ori_Inf7m <- tapply(df_sacrois_ORI_Inf7m$propFO, df_sacrois_ORI_Inf7m$SECT_COD_ORIGINE, sd)

## Between 7 and 10 meters
df_sacrois_ORI_7to10m <- df_sacrois_ORI %>% 
  filter(SEGMENT %in% "[7-10[ m")
mean_sacrois_ori_7to10m <- tapply(df_sacrois_ORI_7to10m$propFO, df_sacrois_ORI_7to10m$SECT_COD_ORIGINE, mean)
sd_gis_sacrois_ori_7to10m <- tapply(df_sacrois_ORI_7to10m$propFO, df_sacrois_ORI_7to10m$SECT_COD_ORIGINE, sd)

## Superior to 10 meters
df_sacrois_ORI_Sup10m <- df_sacrois_ORI %>% 
  filter(SEGMENT %in% "[10-12[ m")
mean_sacrois_ori_Sup10m <- tapply(df_sacrois_ORI_Sup10m$propFO, df_sacrois_ORI_Sup10m$SECT_COD_ORIGINE, mean)
sd_gis_sacrois_ori_Sup10m <- tapply(df_sacrois_ORI_Sup10m$propFO, df_sacrois_ORI_Sup10m$SECT_COD_ORIGINE, sd)
```

# 2. Qu'elle est la représentation spatiale des données Sacrois ? 

```{r load map data for both data, include=FALSE} 

###-----------------------------------------------------------------------------
### load spatial data for mapping : SIH data 
source("R/load_raw_data/load_maps_data.R")

###-----------------------------------------------------------------------------
### load spatial data for mapping : GIS-VALPENA data 
source("R/load_raw_data/load_maps_gis_data.R")
```


## 2.1. Qu'elle est l'information spatiale contenue dans les rectangles statistiques ? 

Dans cette partie, le nombre de navires déclarant une activité dans les données Sacrois est sommé pour chaque rectangle statistique. 

  * Cartographie du nombre de navires actifs par rectangle CIEM par an : 
  
```{r load map number vessels with REC, echo= FALSE}
## 1 : with all rectangle  --> sf_ices_all_rec

#compil a data frame with each row is : vessels number by rect
df_map_sacrois_nb_vessels_REC <- df_sacrois %>% 
  dplyr::select(REF_YEAR, LE_RECT, VE_REF) %>%
  distinct()  %>% 
  group_by(REF_YEAR, LE_RECT) %>%
  summarize(
    total_nb_vessels = n()
  ) %>% 
  data.frame()

###Join map data
sf_map_sacrois_nb_vessels_REC <- left_join(df_map_sacrois_nb_vessels_REC, sf_ices_rec,
                                   by = c("LE_RECT" ="ICESNAME"),
                                   all.y=T) %>%
  st_sf() %>%
  st_difference(., st_combine(sf_coast_line))

###Create region_rec shapes
sf_region_rec <- st_difference(sf_ices_all_rec, st_combine(sf_coast_line)) %>%
  filter(ICESNAME %in% df_eflalo$LE_RECT) 

### map nb vessels 
map_sacrois_nb_vessels_REC <- plot_empty_map +
  geom_sf(data= sf_map_sacrois_nb_vessels_REC, aes(fill = total_nb_vessels), size = 0.01) +
  geom_sf(data = sf_region_rec, fill = NA, size = 0.01) +
  facet_wrap(~REF_YEAR, ncol= 4) +
  scale_fill_viridis_c(name = "Vessels number by rect in a year") +
  theme(plot.title = element_text(size = 10, face = "bold"),
        # legend.title = element_text(size = 10),
        axis.text = element_text(size = 4),
        axis.title = element_text(size = 8),
        strip.text.x = element_text( size = 5,
                                     margin = margin(.01, 0, .01, 0, "cm")),
        legend.key.width = unit(1, 'cm'),
        legend.key.height = unit(0.25, 'cm'),
        legend.position = "bottom",
        legend.direction = "horizontal"
  ) +
  ggtitle(("Number of vessels by rect in a year in sacrois data"))

print(map_sacrois_nb_vessels_REC)
```

  * Boxplot de la variation annuelle du nombre de navires dans le golfe de Gascogne : 

```{r plot number vessels with REC, echo= FALSE}

nb_vessels_number_REC <- df_map_sacrois_nb_vessels_REC %>% 
  group_by(REF_YEAR) %>%
  summarize(
    total_nb_vessels_year = n()
  ) 

plot_sacrois_nb_vessels_REC <- df_map_sacrois_nb_vessels_REC %>%
  ggplot( aes(x=REF_YEAR, y= total_nb_vessels)) +
    geom_boxplot() +
    scale_fill_viridis(discrete = TRUE, alpha=0.6) +
    geom_jitter(color="black", size=0.4, alpha=0.9) +
    xlab("Year") +
    ylab("# vessels") +
    theme(plot.title = element_text(size = 10, face = "bold"),
        # legend.title = element_text(size = 10),
        axis.text = element_text(size = 4),
        axis.title = element_text(size = 8),
        strip.text.x = element_text( size = 5,
                                     margin = margin(.01, 0, .01, 0, "cm")),
        legend.key.width = unit(1, 'cm'),
        legend.key.height = unit(0.25, 'cm'),
        legend.position = "bottom",
        legend.direction = "horizontal") +
    ggtitle("Number of vessels in the rect per year in Sacrois data") 

print(plot_sacrois_nb_vessels_REC)
```

## 2.2. Qu'elle est l'information spatiale contenue dans les rectangles et sous-rectangles statistiques ? 

Dans cette partie, le nombre de navires déclarant une activité dans les données Sacrois est sommé pour chaque rectangle statistique et sous-rectangle statistique. 

  * Cartographie du nombre de navires par rectangle statistiques et sous-rectangle CIEM : 
  
```{r load map number vessels with REC and ss-REC, echo= FALSE}

####################################################################################
## 2 : with rectangle  : sf_all_ices_rec
df_map_sacrois_nb_vessels_all_REC <- df_sacrois %>% 
  dplyr::select(REF_YEAR, LE_RECT, VE_REF)  %>% 
  distinct  %>% 
  group_by(REF_YEAR,LE_RECT) %>% 
  summarize(
    total_nb_vessels = n()
  ) %>% 
  data.frame()

sf_map_all_rec_sacrois_nb_vessels <- left_join(df_map_sacrois_nb_vessels_all_REC, sf_ices_all_rec,
                                          by = c("LE_RECT" ="ICESNAME"),
                                          all.y=T) %>%
  st_sf() %>%
  st_difference(., st_combine(sf_coast_line))

### map nb vessels 
map_sacrois_nb_vessels_all_REC <- plot_empty_map +
  geom_sf(data= sf_map_all_rec_sacrois_nb_vessels, aes(fill = total_nb_vessels), size = 0.01) +
  geom_sf(data = sf_region_rec, fill = NA, size = 0.01) +
  scale_fill_viridis_c(name = "Vessels number by rect and ss-rect in a year") +
  facet_wrap(~REF_YEAR, ncol= 4) +
  theme(plot.title = element_text(size = 10, face = "bold"),
        # legend.title = element_text(size = 10),
        axis.text = element_text(size = 4),
        axis.title = element_text(size = 8),
        strip.text.x = element_text( size = 5,
                                     margin = margin(.01, 0, .01, 0, "cm")),
        legend.key.width = unit(1, 'cm'),
        legend.key.height = unit(0.25, 'cm'),
        legend.position = "bottom",
        legend.direction = "horizontal"
  ) +
  ggtitle("Number of vessels by rect in a year in sacrois data")

print(map_sacrois_nb_vessels_all_REC)
```

  * Boxplot de la variation annuelle du nombre de navires par rectangle et sous-rectangle dans le golfe de Gascogne : 

```{r load boxplot number vessels with REC and ss-REC, echo= FALSE}

nb_vessels_number_REC <- df_map_sacrois_nb_vessels_all_REC %>% 
  group_by(REF_YEAR) %>%
  summarize(
    total_nb_vessels_year = n()
  ) 

plot_sacrois_nb_vessels_all_REC <- df_map_sacrois_nb_vessels_all_REC %>%
  ggplot( aes(x=REF_YEAR, y= total_nb_vessels)) +
    geom_boxplot() +
    scale_fill_viridis(discrete = TRUE, alpha=0.6) +
    geom_jitter(color="black", size=0.4, alpha=0.9) +
    xlab("Year") +
    ylab("# vessels") +
    theme(plot.title = element_text(size = 10, face = "bold"),
        # legend.title = element_text(size = 10),
        axis.text = element_text(size = 4),
        axis.title = element_text(size = 8),
        strip.text.x = element_text( size = 5,
                                     margin = margin(.01, 0, .01, 0, "cm")),
        legend.key.width = unit(1, 'cm'),
        legend.key.height = unit(0.25, 'cm'),
        legend.position = "bottom",
        legend.direction = "horizontal") +
    ggtitle("Number of vessels in the rect and ss-rect per year in Sacrois data") 

print(plot_sacrois_nb_vessels_all_REC)
```
  

## 2.3. Projection du nombre de navires par rectangle et sous-rectangle statistique CIEM en sommant ceux des sous-rectangle

Chaque sous-rectangle statistique est une division d'un rectangle, l'ensemble des sous-rectangle appartenant à un même rectangle sont regroupés. Pour chaque rectangle, le nombre de navires déclarant une activité dans les données Sacrois est sommé.

  * Cartographie du nombre de navires actifs par rectangle CIEM en regroupant les sous-rectangles par an : 
  
```{r load map number vessels with REC with ss-REC grouped, echo= FALSE}
## 3 : With group of ss-rec in rect 


#compil a data frame with each row is : vessels number by rect
df_map_sacrois_nb_vessels_joinREC <- df_sacrois %>% 
  dplyr::select(REF_YEAR, LE_RECT, VE_REF) %>% 
  mutate(LE_RECT = substring(LE_RECT,first=1,last=4)) %>% 
  distinct()  %>% 
  group_by(REF_YEAR, LE_RECT) %>%
  summarize(
    total_nb_vessels = n()
  ) 

###Join map data
sf_map_sacrois_nb_vessels_joinREC <- left_join(df_map_sacrois_nb_vessels_joinREC, sf_ices_all_rec,
                                   by = c("LE_RECT" ="ICESNAME"),
                                   all.y=T) %>%
  st_sf() %>%
  st_difference(., st_combine(sf_coast_line))

### map nb vessels 
map_sacrois_nb_vessels_joinREC <- plot_empty_map +
  geom_sf(data= sf_map_sacrois_nb_vessels_joinREC, aes(fill = total_nb_vessels), size = 0.01) +
  geom_sf(data = sf_region_rec, fill = NA, size = 0.01) +
  facet_wrap(~REF_YEAR, ncol= 4) +
  scale_fill_viridis_c(name = "Vessels number by rect in a year") +
  theme(plot.title = element_text(size = 10, face = "bold"),
        # legend.title = element_text(size = 10),
        axis.text = element_text(size = 4),
        axis.title = element_text(size = 8),
        strip.text.x = element_text( size = 5,
                                     margin = margin(.01, 0, .01, 0, "cm")),
        legend.key.width = unit(1, 'cm'),
        legend.key.height = unit(0.25, 'cm'),
        legend.position = "bottom",
        legend.direction = "horizontal"
  ) +
  ggtitle(("Number of vessels in the rect (ss-rect grouped) per year in Sacrois data"))

print(map_sacrois_nb_vessels_joinREC)
```


  * Boxplot de la variation annuelle du nombre de navires par rectangle en regroupant les sous-rectangles dans le golfe de Gascogne : 

```{r load boxplot number vessels in REC with ss-REC grouped, echo= FALSE}

nb_vessels_number_REC <- df_map_sacrois_nb_vessels_joinREC %>% 
  group_by(REF_YEAR) %>%
  summarize(
    total_nb_vessels_year = n()
  ) 

plot_sacrois_nb_vessels_joinREC <- df_map_sacrois_nb_vessels_joinREC %>%
  ggplot( aes(x=REF_YEAR, y= total_nb_vessels)) +
    geom_boxplot() +
    scale_fill_viridis(discrete = TRUE, alpha=0.6) +
    geom_jitter(color="black", size=0.4, alpha=0.9) +
    xlab("Year") +
    ylab("# vessels") +
    theme(plot.title = element_text(size = 10, face = "bold"),
        # legend.title = element_text(size = 10),
        axis.text = element_text(size = 4),
        axis.title = element_text(size = 8),
        strip.text.x = element_text( size = 5,
                                     margin = margin(.01, 0, .01, 0, "cm")),
        legend.key.width = unit(1, 'cm'),
        legend.key.height = unit(0.25, 'cm'),
        legend.position = "bottom",
        legend.direction = "horizontal") +
    ggtitle("Number of vessels in the rect (ss-rect grouped) per year in Sacrois data") 

print(plot_sacrois_nb_vessels_joinREC)
```
  
  
## 2.5. Comparaison des trois cartes pour l'année 2017

Afin d'améliorer la visibilité des cartes, une seule année est représentée. Cette année a été sélectionnée car elle représente l'année disponible pour les données GIS-VALPENA. 

```{r map with all projection in 2017, echo=FALSE}

# 1 : REC
sf_map_sacrois_nb_vessels_REC <- sf_map_sacrois_nb_vessels_REC %>%
	filter(REF_YEAR== 2017)

map_sacrois_nb_vessels_REC <- plot_empty_map +
  geom_sf(data= sf_map_sacrois_nb_vessels_REC, aes(fill = total_nb_vessels), size = 0.01) +
  geom_sf(data = sf_region_rec, fill = NA, size = 0.01) +
  facet_wrap(~REF_YEAR, ncol= 4) +
  scale_fill_viridis_c(name = "Vessels number by rect in a year") +
  theme(plot.title = element_text(size = 10, face = "bold"),
        # legend.title = element_text(size = 10),
        axis.text = element_text(size = 4),
        axis.title = element_text(size = 8),
        strip.text.x = element_text( size = 5,
                                     margin = margin(.01, 0, .01, 0, "cm")),
        legend.key.width = unit(1, 'cm'),
        legend.key.height = unit(0.25, 'cm'),
        legend.position = "bottom",
        legend.direction = "horizontal"
  ) +
  ggtitle(("Number of vessels by rect in a year in sacrois data"))

print(map_sacrois_nb_vessels_REC)

# 2 : all_rec
sf_map_all_rec_sacrois_nb_vessels <- sf_map_all_rec_sacrois_nb_vessels %>%
	filter(REF_YEAR== 2017)


### map nb vessels 
map_sacrois_nb_vessels_all_REC <- plot_empty_map +
  geom_sf(data= sf_map_all_rec_sacrois_nb_vessels, aes(fill = total_nb_vessels), size = 0.01) +
  geom_sf(data = sf_region_rec, fill = NA, size = 0.01) +
  scale_fill_viridis_c(name = "Vessels number by rect and ss-rect in a year") +
  facet_wrap(~REF_YEAR, ncol= 4) +
  theme(plot.title = element_text(size = 10, face = "bold"),
        # legend.title = element_text(size = 10),
        axis.text = element_text(size = 4),
        axis.title = element_text(size = 8),
        strip.text.x = element_text( size = 5,
                                     margin = margin(.01, 0, .01, 0, "cm")),
        legend.key.width = unit(1, 'cm'),
        legend.key.height = unit(0.25, 'cm'),
        legend.position = "bottom",
        legend.direction = "horizontal"
  ) +
  ggtitle("Number of vessels by rect in a year in sacrois data")

print(map_sacrois_nb_vessels_all_REC)


# 3 : rec with ss-rec grouped
sf_map_sacrois_nb_vessels_joinREC <- sf_map_sacrois_nb_vessels_joinREC %>%
  filter(REF_YEAR == 2017)

map_sacrois_nb_vessels_joinREC <- plot_empty_map +
  geom_sf(data= sf_map_sacrois_nb_vessels_joinREC, aes(fill = total_nb_vessels), size = 0.01) +
  geom_sf(data = sf_region_rec, fill = NA, size = 0.01) +
  facet_wrap(~REF_YEAR, ncol= 4) +
  scale_fill_viridis_c(name = "Vessels number by rect in a year") +
  theme(plot.title = element_text(size = 10, face = "bold"),
        # legend.title = element_text(size = 10),
        axis.text = element_text(size = 4),
        axis.title = element_text(size = 8),
        strip.text.x = element_text( size = 5,
                                     margin = margin(.01, 0, .01, 0, "cm")),
        legend.key.width = unit(1, 'cm'),
        legend.key.height = unit(0.25, 'cm'),
        legend.position = "bottom",
        legend.direction = "horizontal"
  ) +
  ggtitle(("Number of vessels in the rect (ss-rect grouped) per year in Sacrois data"))

print(map_sacrois_nb_vessels_joinREC)

```

## 3. CONCLUSION

Les données des sous-rectangles ne semblent pas complètes, certains navires doivent renseigner les rectangles statistiques au lieu des sous-rectangles. Cependant, l'exclusion des sous-rectangles lors de la projection diminue la précision de l'activité de pêche sur la bande côtière. 

Si chaque groupe de navire appartenant à la même flottille et pratiquant le même métier a la même distribution spatiale au sein d'un sous-rectangle pour un même rectangle à un instant donné, alors au sein de chaque groupe de navire, les navires qui renseignent uniquement le rectangle statistique peuvent être distribués au sein des sous-rectangles statistiques. 

Chaque groupe de navires est divisé en sous-groupe selon la déclaration ou non des sous-rectangle lorsqu'ils sont disponibles. Pour chaque groupe dont les navires déclarent leur présence dans le  sous-rectangle, la proportion d'occupation de chaque sous-rectangle au sein d'un rectangle peut-être calculée. Les navires déclarant uniquement les rectangles peuvent être répartis selon la proportion d'occupation des sous-rectangles pour un rectangle donné.


