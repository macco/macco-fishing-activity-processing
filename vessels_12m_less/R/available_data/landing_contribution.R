###----------------------------------------------------------------------------------------------------------------------------------------------------------
### Evolution of landing calue and quantity by big segment size : Inf and Sup at 12 meters
###----------------------------------------------------------------------------------------------------------------------------------------------------------


##make a data frame of all vessels value productivity
df_productivity_euro <- df_sacrois_all_year_size %>%
  select(REF_YEAR, LE_EURO, SIZE) %>%
  group_by(REF_YEAR, SIZE) %>%
  summarise(LE_EURO = sum(LE_EURO))  %>%
  group_by(REF_YEAR, SIZE) %>%
  pivot_wider(names_from= SIZE, values_from= LE_EURO) %>%
  mutate(PROPinf = (`Inf. 12 m`/(`Inf. 12 m`+`Sup. 12 m`)*100),
         PROPsup = (`Sup. 12 m`/(`Inf. 12 m`+`Sup. 12 m`)*100)) %>%
  pivot_longer(cols = c(PROPinf, PROPsup), names_to = "SIZE", values_to= "PROP") %>%
  mutate(SIZE= factor(SIZE, levels = c('PROPinf', 'PROPsup'), ordered = TRUE)) %>%
  group_by(REF_YEAR, SIZE) %>%
  as.data.frame()

min_euro <- by(df_productivity_euro$PROP, df_productivity_euro$SIZE, min)
max_euro <- by(df_productivity_euro$PROP, df_productivity_euro$SIZE, max)

#Plot vessels value productivity
plot_productivity_euro <- ggplot(df_productivity_euro, aes(x= REF_YEAR, y=PROP, group= SIZE)) +
  geom_area(alpha=0.6 , size=.5, colour= "grey", aes(fill= SIZE)) +
  scale_fill_viridis(discrete = T) +
  ylab("% of annual landed value") +
  xlab("Year") +
  ggtitle("(A) : Landed value") +
  theme_ipsum(plot_title_size = 15) +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust = 1, size = 8),
        axis.text.y= element_text(size= 12),
        legend.title = element_blank(),
        legend.position = "none")

print(plot_productivity_euro)

###----------------------------------------------------------------------------------------------------------------------------------------------------------

##make a data frame of all vessels quantity productivity
df_productivity_kg <- df_sacrois_all_year_size %>%
  select(REF_YEAR, LE_KG, SIZE) %>%
  group_by(REF_YEAR, SIZE) %>%
  summarise(LE_KG = sum(LE_KG))  %>%
  group_by(REF_YEAR, SIZE) %>%
  pivot_wider(names_from= SIZE, values_from= LE_KG) %>%
  mutate(PROPinf = (`Inf. 12 m`/(`Inf. 12 m`+`Sup. 12 m`)*100),
         PROPsup = (`Sup. 12 m`/(`Inf. 12 m`+`Sup. 12 m`)*100))  %>%
  pivot_longer(cols = c(PROPinf, PROPsup), names_to = "SIZE", values_to= "PROP") %>%
  mutate(SIZE= factor(SIZE, levels = c('PROPinf', 'PROPna', 'PROPsup'), ordered = TRUE)) %>%
  group_by(REF_YEAR, SIZE) %>%
  as.data.frame()


min_kg <- by(df_productivity_kg$PROP, df_productivity_kg$SIZE, min)
max_kg <- by(df_productivity_kg$PROP, df_productivity_kg$SIZE, max)

#Plot vessels quantity productivity
plot_productivity_kg <- ggplot(df_productivity_kg, aes(x= REF_YEAR, y=PROP, group= SIZE)) +
  geom_area(alpha=0.6 , size=.5, colour= "grey", aes(fill= SIZE)) +
  scale_fill_viridis(discrete = T) +
  ylab("% of annual landed quantity") +
  xlab("Year") +
  ggtitle("(A) : Landed quantity") +
  theme_ipsum(plot_title_size = 15) +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust = 1, size = 10),
        axis.text.y= element_text(size= 15),
        legend.background = element_rect(fill = "white",
                                         size = 0.4,
                                         linetype = "solid",
                                         colour = "grey"),
        legend.title = element_blank(),
        legend.position= "right")

print(plot_productivity_kg)

###----------------------------------------------------------------------------------------------------------------------------------------------------------

##make a data frame of all vessels productivity by key species
df_productivity_euro_species <- df_sacrois_all_year_size %>%
  select(REF_YEAR, ESP_COD_FAO, LE_EURO, SIZE) %>%
  filter(ESP_COD_FAO %in% key_species) %>%
  group_by(REF_YEAR, ESP_COD_FAO, SIZE) %>%
  summarise(LE_EURO = sum(LE_EURO))  %>%
  group_by(REF_YEAR, ESP_COD_FAO, SIZE) %>%
  pivot_wider(names_from= SIZE, values_from= LE_EURO) %>%
  mutate(PROPinf = (`Inf. 12 m`/(`Inf. 12 m`+`Sup. 12 m`)*100),
         PROPsup = (`Sup. 12 m`/(`Inf. 12 m`+`Sup. 12 m`)*100),
         ESP_COD_FAO = factor(ESP_COD_FAO, levels = key_species)) %>%
  pivot_longer(cols = c(PROPinf, PROPsup), names_to = "SIZE", values_to= "PROP") %>%
  mutate(SIZE= factor(SIZE, levels = c('PROPinf', 'PROPsup'), ordered = TRUE)) %>%
  group_by(REF_YEAR, SIZE, ESP_COD_FAO) %>%
  as.data.frame()

min_kg <- by(df_productivity_euro_species$PROP, df_productivity_euro_species$ESP_COD_FAO, min)
max_kg <- by(df_productivity_euro_species$PROP, df_productivity_euro_species$ESP_COD_FAO, max)

#Plot proportion of vessels in declarative data / reference data for each segment siza
plot_productivity_euro_species <- ggplot(df_productivity_euro_species, aes(x= REF_YEAR, y=PROP, group= SIZE)) +
  geom_area(alpha=0.6 , size=.5, colour= "grey", aes(fill= SIZE)) +
  scale_fill_viridis(discrete = T) +
  facet_grid(~ ESP_COD_FAO) +
  ylab("% of annual landed value") +
  xlab("Year") +
  theme_ipsum(plot_title_size = 10) +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust = 1, size = 6),
        axis.text.y= element_text(size= 10),
        legend.background = element_rect(fill = "white",
                                         size = 0.4,
                                         linetype = "solid",
                                         colour = "grey"),
        legend.title = element_blank(),
        legend.position= "right")

print(plot_productivity_euro_species)

###----------------------------------------------------------------------------------------------------------------------------------------------------------
df_productivity_kg_species <- df_sacrois_all_year_size %>%
  select(REF_YEAR, ESP_COD_FAO, LE_KG, SIZE) %>%
  filter(ESP_COD_FAO %in% key_species) %>%
  group_by(REF_YEAR, ESP_COD_FAO, SIZE) %>%
  summarise(LE_KG = sum(LE_KG))  %>%
  group_by(REF_YEAR, ESP_COD_FAO, SIZE) %>%
  pivot_wider(names_from= SIZE, values_from= LE_KG) %>%
  mutate(PROPinf = (`Inf. 12 m`/(`Inf. 12 m`+`Sup. 12 m`)*100),
         PROPsup = (`Sup. 12 m`/(`Inf. 12 m`+`Sup. 12 m`)*100),
         ESP_COD_FAO = factor(ESP_COD_FAO, levels = key_species)) %>%
  pivot_longer(cols = c(PROPinf, PROPsup), names_to = "SIZE", values_to= "PROP") %>%
  mutate(SIZE= factor(SIZE, levels = c('PROPinf', 'PROPna', 'PROPsup'), ordered = TRUE)) %>%
  group_by(REF_YEAR, SIZE, ESP_COD_FAO) %>%
  as.data.frame()

plot_productivity_kg_species <- ggplot(df_productivity_kg_species, aes(x= REF_YEAR, y=PROP, group= SIZE)) +
  geom_area(alpha=0.6 , size=.5, colour= "grey", aes(fill= SIZE)) +
  scale_fill_viridis(discrete = T) +
  facet_grid(~ ESP_COD_FAO) +
  ylab("% of annual landed quantity") +
  xlab("") +
  theme_ipsum(plot_title_size = 10) +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust = 1, size = 6),
        axis.text.y= element_text(size= 10),
        legend.background = element_rect(fill = "white",
                                         size = 0.4,
                                         linetype = "solid",
                                         colour = "grey"),
        legend.title = element_blank(),
        legend.position= "right")

print(plot_productivity_kg_species)
