################################################################################
### Set seasons and zones for each metier_isis
################################################################################


###-----------------------------------------------------------------------------
### define zone, default season = LE_RECT or load user zones
if (file.exists(here(path_fishery_data_tidy, "user", "zones.csv"))) {
  cat(blue("Load user defined metier_isis aggregation csv file \n"))

  df_zones <- read_csv2(paste(path_fishery_data_tidy,
                              "user", "zones.csv", sep = "/"),
                        col_types = cols(LE_RECT = col_character())) %>%
    mutate(LE_RECT = factor(LE_RECT),
           zone = factor(zone),
           metier_isis = factor(metier_isis))

  ### check if zone file is not a dummy file
  if (nlevels(df_zones$zone) > 0) {
    ### merge zones into eflalo data frame
    df_eflalo <- left_join(df_eflalo, df_zones, by = "LE_RECT")

  } else {
    cat(blue("Missing zones in zone csv file \n"))
    df_eflalo <- df_eflalo %>%
      mutate(zone = factor(metier_isis))

    df_zones <- df_eflalo %>%
      dplyr::select(LE_RECT, metier_isis, zone) %>%
      distinct()

  }
} else {
  cat(blue("Zone set as LE_RECT \n"))
  df_eflalo <- df_eflalo %>%
    mutate(zone = factor(metier_isis))

  df_zones <- df_eflalo %>%
    dplyr::select(LE_RECT, metier_isis, zone) %>%
    distinct()
}

### create an sf object for plotting zones : zone with RECT
sf_zones <-  left_join(df_zones, sf_ices_rec,
                       by = c("LE_RECT" = "ICESNAME" )) %>%
  st_sf() %>%
  group_by(zone) %>%
  summarise(geometry = st_union(geometry)) %>%
  st_difference(., st_combine(sf_coast_line))

### create an sf object for plotting zones : ss-zone with SECT_COD_SACROIS
df_ss_zones <- df_eflalo %>%
  dplyr::select(SECT_COD_SACROIS_NIV6, metier_isis_isis, zone) %>%
  filter(! SECT_COD_SACROIS_NIV6 == "") %>%
  distinct()

sf_ss_zones <-  left_join(df_ss_zones, sf_ices_ss_rec,
                       by = c("SECT_COD_SACROIS_NIV6" = "ICESNAME" )) %>%
  st_sf() %>%
  group_by(zone) %>%
  summarise(geometry = st_union(geometry)) %>%
  st_difference(., st_combine(sf_coast_line))

###-----------------------------------------------------------------------------
### create a dummy file for the user to fill
if (file.exists(here(path_fishery_data_tidy, "user", "zones.csv")) == FALSE) {

  cat(blue("No user defined zones \n"))
    df_eflalo %>%
    dplyr::select(LE_RECT, metier_isis_isis) %>%
    distinct() %>%
    mutate(zone = NA) %>%
  write_csv2(x = .,
             file = paste(path_fishery_data_tidy,
                          "user", "zones.csv", sep = "/"))

  cat(blue("Create a csv file to define zone as a group of LE_RECT : ",
           paste(path_fishery_data_tidy, "user", "zones.csv", sep = "/"),
           "\n"))
}
