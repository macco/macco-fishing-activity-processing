################################################################################
### after the clustering of metier:
### the user can provide a table to aggregate metier that are similar
################################################################################

###-----------------------------------------------------------------------------
### check if aggregated file exists and load it
if (file.exists(paste(path_fishery_data_tidy,
                      "user", "metier_agg.csv", sep = "/"))) {

  cat(blue("Load user defined metier aggregation csv file \n"))
  df_metier_agg <- read_csv2(paste(path_fishery_data_tidy,
                                   "user", "metier_agg.csv", sep = "/")) %>%
    mutate(metier  = factor(metier ))

  ### check if aggregated file has all the levels from metier cluster in eflalo
  if (nlevels(df_metier_agg$metier ) == nlevels(df_eflalo$metier)) {

    df_eflalo <- left_join(df_eflalo, df_metier_agg, by = "metier") %>%
      mutate(metier_no_agg = metier,
             metier = metier_agg)
unique(df_sacrois$QAM_LIB)
} else {
    ### if not inform user to fix it
    metier_missing_levels <- which(!(levels(df_eflalo$metier) %in%
                                       levels(df_metier_agg$metier)))
    cat(blue("Missing metier in aggredated csv file : ",
             levels(df_eflalo$metier)[metier_missing_levels],
             "\n"))
  }

} else {

  ###-----------------------------------------------------------------------------
  ### if file does not exists generate a template and inform user

  cat("No user defined metier aggregation csv file, aggregation not performed \n")

  cat(blue("Create a csv file to aggegate metier at ",
           paste(path_fishery_data_tidy, "user", "metier_agg.csv", sep = "/"),
           "\n"))
  df_metier_agg <- df_eflalo %>%
    dplyr::select(metier) %>%
    distinct() %>%
    mutate(metier_agg = NA)

  write_csv2(x = df_metier_agg,
             file = paste(path_fishery_data_tidy,
                          "user", "metier_agg.csv", sep = "/"))
}

