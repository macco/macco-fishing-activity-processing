################################################################################
### Set metier as DCF-6 metier
################################################################################

source("R/species_analysis/set_species_key_and_other.R")

df_eflalo <- df_eflalo %>%
  ungroup() %>%
  mutate(METIER_DCF_6_COD = fct_recode(METIER_DCF_6_COD, "Missing" = ""),
         metier = as_factor(paste0(gear_isis, "_", METIER_DCF_6_COD)),
         metier = factor(ifelse(rowSums(across(all_of(eflalo_col_sp_key_kg))) == 0,
                                paste0(gear_isis, "_", "metier_OTH"),
                                as.character(metier)))) %>%
  group_by(metier) %>%
  mutate(n_seq_metier = n(),
          metier = factor(ifelse(n_seq_metier < minimum_n_seq_metier,
                                 paste0(gear_isis, "_", "low_activity"),
                                 as.character(metier)))) %>%
  droplevels()
