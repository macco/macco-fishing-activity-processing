################################################################################
### Boxplot : mean_landing given Species, year and super metier

################################################################################
df_metier_landing_withoutOTH_cl1 <- df_metier_landing %>%
  filter(! Species == 'OTH')%>% #Remove OTHER
  filter(cluster_metier == 1) %>%
  group_by(metier) %>%
  mutate(total_landing = sum(mean_landing)) %>%
  group_by(metier, Species) %>%
  mutate(prop_landing_species = mean_landing / total_landing)


plot_supermetier_landing_all_years_clust_cl1 <- ggplot(df_metier_landing_withoutOTH_cl1) +
  geom_boxplot(size=.2, colour= "grey",
               aes(x = Species, y = prop_landing_species,
                   fill = super_metier)) +
  #facet_wrap(~ cluster_metier, ncol= 3) +
  scale_fill_viridis(discrete = T) +
  ylab("Profil de débarquement standardisé \n") +
  xlab("\n Espèces") +
  ylim(c(0,1)) +
  ggtitle("Cluster 1 \n") +
  theme(axis.text.x = element_text(angle = 70, vjust = 0.5, hjust = 1, size = 10),
        axis.text.y= element_text(size= 12),
        legend.title = element_blank(),
        legend.position = "right")

print(plot_supermetier_landing_all_years_clust_cl1)

### save plots if save_plot == TRUE
if(save_plot) {
  f_save_plot(plot_supermetier_landing_all_years_clust_cl1,
              file_name = paste0("boxplot_super_metier_profil_cl1",
                                 min(year_vec),"_",
                                 max(year_vec)),
              path = path_figure,
              save = save_plot)
}

################################################################################
df_metier_landing_withoutOTH_cl2 <- df_metier_landing %>%
  filter(! Species == 'OTH')%>% #Remove OTHER
  filter(cluster_metier == 2) %>%
  group_by(metier) %>%
  mutate(total_landing = sum(mean_landing)) %>%
  group_by(metier, Species) %>%
  mutate(prop_landing_species = mean_landing / total_landing)


plot_supermetier_landing_all_years_clust_cl2 <- ggplot(df_metier_landing_withoutOTH_cl2) +
  geom_boxplot(size=.2, colour= "grey",
               aes(x = Species, y = prop_landing_species,
                   fill = super_metier)) +
  #facet_wrap(~ cluster_metier, ncol= 3) +
  scale_fill_viridis(discrete = T) +
  ylab("Profil de débarquement standardisé \n") +
  xlab("\n Espèces") +
  ylim(c(0,1)) +
  ggtitle("Cluster 2 \n") +
  theme(axis.text.x = element_text(angle = 70, vjust = 0.5, hjust = 1, size = 10),
        axis.text.y= element_text(size= 12),
        legend.title = element_blank(),
        legend.position = "right")

print(plot_supermetier_landing_all_years_clust_cl2)

### save plots if save_plot == TRUE
if(save_plot) {
  f_save_plot(plot_supermetier_landing_all_years_clust_cl2,
              file_name = paste0("boxplot_super_metier_profil_cl2",
                                 min(year_vec),"_",
                                 max(year_vec)),
              path = path_figure,
              save = save_plot)
}

################################################################################
df_metier_landing_withoutOTH_cl3 <- df_metier_landing %>%
  filter(! Species == 'OTH')%>% #Remove OTHER
  filter(cluster_metier == 3) %>%
  group_by(metier) %>%
  mutate(total_landing = sum(mean_landing)) %>%
  group_by(metier, Species) %>%
  mutate(prop_landing_species = mean_landing / total_landing)


plot_supermetier_landing_all_years_clust_cl3 <- ggplot(df_metier_landing_withoutOTH_cl3) +
  geom_boxplot(size=.2, colour= "grey",
               aes(x = Species, y = prop_landing_species,
                   fill = super_metier)) +
  #facet_wrap(~ cluster_metier, ncol= 3) +
  scale_fill_viridis(discrete = T) +
  ylab("Profil de débarquement standardisé \n") +
  xlab("\n Espèces") +
  ylim(c(0,1)) +
  ggtitle("Cluster 3 \n") +
  theme(axis.text.x = element_text(angle = 70, vjust = 0.5, hjust = 1, size = 12),
        axis.text.y= element_text(size= 12),
        legend.title = element_blank(),
        legend.position = "right")

print(plot_supermetier_landing_all_years_clust_cl3)

### save plots if save_plot == TRUE
if(save_plot) {
  f_save_plot(plot_supermetier_landing_all_years_clust_cl3,
              file_name = paste0("boxplot_super_metier_profil_cl3",
                                 min(year_vec),"_",
                                 max(year_vec)),
              path = path_figure,
              save = save_plot)
}


################################################################################
df_metier_landing_withoutOTH_cl4 <- df_metier_landing %>%
  filter(! Species == 'OTH')%>% #Remove OTHER
  filter(cluster_metier == 4) %>%
  group_by(metier) %>%
  mutate(total_landing = sum(mean_landing)) %>%
  group_by(metier, Species) %>%
  mutate(prop_landing_species = mean_landing / total_landing)


plot_supermetier_landing_all_years_clust_cl4 <- ggplot(df_metier_landing_withoutOTH_cl4) +
  geom_boxplot(size=.2, colour= "grey",
               aes(x = Species, y = prop_landing_species,
                   fill = super_metier)) +
  #facet_wrap(~ cluster_metier, ncol= 4) +
  scale_fill_viridis(discrete = T) +
  ylab("Profil de débarquement standardisé \n") +
  xlab("\n Espèces") +
  ylim(c(0,1)) +
  ggtitle("Cluster 4 \n") +
  theme(axis.text.x = element_text(angle = 70, vjust = 0.5, hjust = 1, size = 12),
        axis.text.y= element_text(size= 12),
        legend.title = element_blank(),
        legend.position = "right")

print(plot_supermetier_landing_all_years_clust_cl4)

### save plots if save_plot == TRUE
if(save_plot) {
  f_save_plot(plot_supermetier_landing_all_years_clust_cl4,
              file_name = paste0("boxplot_super_metier_profil_cl4",
                                 min(year_vec),"_",
                                 max(year_vec)),
              path = path_figure,
              save = save_plot)
}


################################################################################
df_metier_landing_withoutOTH_cl5 <- df_metier_landing %>%
  filter(! Species == 'OTH')%>% #Remove OTHER
  filter(cluster_metier == 5) %>%
  group_by(metier) %>%
  mutate(total_landing = sum(mean_landing)) %>%
  group_by(metier, Species) %>%
  mutate(prop_landing_species = mean_landing / total_landing)


plot_supermetier_landing_all_years_clust_cl5 <- ggplot(df_metier_landing_withoutOTH_cl5) +
  geom_boxplot(size=.2, colour= "grey",
               aes(x = Species, y = prop_landing_species,
                   fill = super_metier)) +
  #facet_wrap(~ cluster_metier, ncol= 5) +
  scale_fill_viridis(discrete = T) +
  ylab("Profil de débarquement standardisé \n") +
  xlab("\n Espèces") +
  ylim(c(0,1)) +
  ggtitle("Cluster 5 \n") +
  theme(axis.text.x = element_text(angle = 70, vjust = 0.5, hjust = 1, size = 12),
        axis.text.y= element_text(size= 12),
        legend.title = element_blank(),
        legend.position = "right")

print(plot_supermetier_landing_all_years_clust_cl5)

### save plots if save_plot == TRUE
if(save_plot) {
  f_save_plot(plot_supermetier_landing_all_years_clust_cl5,
              file_name = paste0("boxplot_super_metier_profil_cl5",
                                 min(year_vec),"_",
                                 max(year_vec)),
              path = path_figure,
              save = save_plot)
}


################################################################################
df_metier_landing_withoutOTH_cl6 <- df_metier_landing %>%
  filter(! Species == 'OTH')%>% #Remove OTHER
  filter(cluster_metier == 6) %>%
  group_by(metier) %>%
  mutate(total_landing = sum(mean_landing)) %>%
  group_by(metier, Species) %>%
  mutate(prop_landing_species = mean_landing / total_landing)


plot_supermetier_landing_all_years_clust_cl6 <- ggplot(df_metier_landing_withoutOTH_cl6) +
  geom_boxplot(size=.2, colour= "grey",
               aes(x = Species, y = prop_landing_species,
                   fill = super_metier)) +
  #facet_wrap(~ cluster_metier, ncol= 6) +
  scale_fill_viridis(discrete = T) +
  ylab("Profil de débarquement standardisé \n") +
  xlab("\n Espèces") +
  ylim(c(0,1)) +
  ggtitle("Cluster 6 \n") +
  theme(axis.text.x = element_text(angle = 70, vjust = 0.5, hjust = 1, size = 12),
        axis.text.y= element_text(size= 12),
        legend.title = element_blank(),
        legend.position = "right")

print(plot_supermetier_landing_all_years_clust_cl6)

### save plots if save_plot == TRUE
if(save_plot) {
  f_save_plot(plot_supermetier_landing_all_years_clust_cl6,
              file_name = paste0("boxplot_super_metier_profil_cl6",
                                 min(year_vec),"_",
                                 max(year_vec)),
              path = path_figure,
              save = save_plot)
}
