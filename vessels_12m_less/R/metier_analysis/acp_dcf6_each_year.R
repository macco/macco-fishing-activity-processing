################################################################################
### Perform clustering to get metier with HAC : for each year
################################################################################

#Principal Component Analysis (PCA) followed by a Hierarchical Agglomerative Clustering (HAC)
#Study the metier stability on each year with PCA and HAC
rm(df_metier_landing_wider)
cat("Perform clustering \n")

for (y in year_vec) {

  cat("Year:", y,
      "- make PCA and followed by a HAC \n")

###-----------------------------------------------------------------------------
## STEP 1 - Compil data frame with mean quantity species landing for each metier
cat(bgMagenta("STEP 1 - Compil data frame with mean quantity species landing for each metier"))

df_metier_landing <- pivot_longer(df_eflalo, cols = starts_with("LE_KG"),
                                  names_to = "Species") %>%
  filter(value > 0) %>%
  dplyr::select(Species, metier, REF_YEAR, value) %>% #value= landed quantity
  group_by(REF_YEAR, metier, Species) %>%
  mutate(Species = as_factor(str_sub(Species, 7, 9)),
         N_SPECIES= n(),
         metier= fct_recode(metier, 'missing' = ""),
         metier= as.character(metier),
         metier= ifelse(N_SPECIES== 1 & Species== 'OTH',
                        "metier_other",
                        metier),
         Species= fct_relevel(Species, "OTH", after = Inf)) %>% #NEW : group other_metier
  summarise(
    mean_landing= mean(value)
  )

df_metier_landing_wider <- df_metier_landing %>%
  pivot_wider(., names_from = Species, values_from= mean_landing, values_fill = 0)
row.names(df_metier_landing_wider)=df_metier_landing_wider$metier #Donnes un nom à chaque ligne

# reorder the columns using select
colnames(df_metier_landing_wider)
df_metier_landing_wider <- df_metier_landing_wider %>%
  dplyr::select(metier, OTH, SOL, HKE, MNZ, RJC, WHG, CET, NEP, RJN, MEG)

###-----------------------------------------------------------------------------
## STEP 2 - PLot a PCA to check the dominant species and verify what structures the catch patterns
cat(bgMagenta("STEP 2 - PLot a PCA to check the dominant species and verify what structures the catch patterns"))
ACP_metier_landing <- PCA(df_metier_landing_wider[,-c(1,2)]) #les variables qui structurant la variance sont ....

###-----------------------------------------------------------------------------
## STEP 3 - H_CLUST analysis : groups species observations by iterative pairwise clustering based on Ward's minimum variance criterion (Ward, 1963)
cat(bgMagenta(" STEP 3 - H_CLUST analysis : groups species observations by iterative pairwise clustering based on Ward's minimum variance criterion"))

# Distance calcul
dist_metier_landing_wider <- dist(df_metier_landing_wider[,-c(1,2)])
names(dist_metier_landing_wider)=df_metier_landing_wider$metier

# Clustering with ward.D method distance
hclust_dist_metier_landing_wider <- hclust(dist_metier_landing_wider, method = 'ward.D')
labels(dist_metier_landing_wider)
plot(hclust_dist_metier_landing_wider,
     cex= 0.5)

cat(bgMagenta(" Cut cluster = 6 "))

#Cut cluster=6
rect.hclust(hclust_dist_metier_landing_wider, k=6)

cut_hclust_dist_metier_landing_wider <- cutree(hclust_dist_metier_landing_wider, k=6)

plot(hclust_dist_metier_landing_wider, cex= 0.5, col= cut_hclust_dist_metier_landing_wider)
rect.hclust(hclust_dist_metier_landing_wider, k=6)

print(sort(cut_hclust_dist_metier_landing_wider))
table(sort(cut_hclust_dist_metier_landing_wider))

df_cut_hclust_dist_metier_landing_wider <- as.data.frame(cut_hclust_dist_metier_landing_wider) %>%
  rownames_to_column(var= 'metier') %>%
  rename(group_metier= cut_hclust_dist_metier_landing_wider)


###-----------------------------------------------------------------------------
## STEP 4 - JOIN the metier and clustering metier for calcul mean and sd
cat(bgMagenta(" STEP 4 - JOIN the metier and clustering metier for calcul mean and sd"))

df_metier_landing_clustering <- left_join(df_metier_landing,df_cut_hclust_dist_metier_landing_wider,
                                          by= 'metier')

dfs_metier_landing_clustering <- df_metier_landing_clustering %>%
  group_by(group_metier, Species) %>%
  summarise(
    mean_metier= mean(mean_landing),
    sd_metier=sd(mean_landing)
  )

###-----------------------------------------------------------------------------
## STEP 5 - PLOT catch profil with group metier (clustering)
cat(bgMagenta("STEP 5 - PLOT catch profil with group metier (clustering)"))

plot_df_metier_landing_clustering <- ggplot(df_metier_landing_clustering) +
  geom_boxplot(aes(x= Species, y=mean_landing, fill= as.factor(group_metier))) +
  ylim(c(0,500))

###-----------------------------------------------------------------------------
## STEP 6 -  Built super_metier
cat(bgMagenta(" STEP 6 -  Built super_metier"))

## In data frame with landing value
list_metier_cluster <- df_metier_landing_clustering %>%
  dplyr::select(group_metier, metier) %>%
  distinct() %>%
  mutate(super_metier= paste(substring(metier, 1,1),
                             group_metier,sep="_"))

table(list_metier_cluster$super_metier)


## Join super_metier in profil catch table
df_landing_super_metier <- left_join(df_metier_landing,list_metier_cluster,
                                     by= 'metier')

###-----------------------------------------------------------------------------
## STEP 7 -  Built super_metierPlot catch profil by super metier
cat(bgMagenta(" STEP 7 -  Plot catch profil by super metier"))

#plot with other species : catch profil for each super_metier
plot_list_metier_cluster <- ggplot(data = df_landing_super_metier,
                                   aes(x= as.factor(super_metier), y=mean_landing,
                                       fill= as.factor(Species),
                                       group=as.factor(Species))) +
  geom_col(alpha=0.8 , size=.5) +
  scale_fill_viridis(discrete = T) +
  ylab("Average quantity landed\n") +
  xlab("Super metier built") +
  ggtitle("Catch profil by super-metier with other species")

plot_list_metier_cluster

#plot without other species : catch profil for each super_metier
plot_list_metier_cluster <- ggplot(data = filter(df_landing_super_metier, Species != 'OTH'),
                                   aes(x= as.factor(super_metier), y=mean_landing,
                                       fill= as.factor(Species),
                                       group=as.factor(Species))) +
  geom_col(alpha=0.8 , size=.5) +
  scale_fill_viridis(discrete = T) +
  ylab("Average quantity landed\n") +
  xlab("Super metier built") +
  ggtitle("Catch profil by super-metier without other species")

plot_list_metier_cluster

###-----------------------------------------------------------------------------
## STEP 8 -  Built super_metierPlot catch profil by super metierJoin super_metier table with df_eflalo
cat(bgMagenta(" STEP 8 -  Join super_metier table with df_eflalo"))
  #A FAIRE AVEC CAT :List metier and super_metier

df_super_metier <- df_landing_super_metier %>%
  dplyr::select(metier, super_metier)

df_eflalo <- left_join(df_eflalo, df_super_metier, by= 'metier')

windows()
barplot(table(df_eflalo$super_metier),
        las=2)
windows()
plot_list_metier_cluster

}

###-----------------------------------------------------------------------------
## STEP 9 -  Plot catch profil for each super_metier : WITH ALL YEARS STUDIED
cat(bgMagenta(" STEP 9 -  Plot catch profil for each super_metier"))

cat(bgMagenta("With other species"))
# PLOT CATCH PROFIL : with other species
df_metier_landing_all_year_with_OTH <- pivot_longer(df_eflalo, cols = starts_with("LE_KG"),
                                           names_to = "Species") %>%
  filter(value > 0) %>%
  dplyr::select(Species, super_metier, REF_YEAR, value) %>% #value= landed quantity
  filter(! is.na(super_metier)) %>%
  group_by(REF_YEAR, super_metier, Species) %>%
  mutate(Species = as_factor(str_sub(Species, 7, 9))) %>%
  summarise(
    mean_landing= mean(value)
  )

plot_metier_landing_all_year <- ggplot(data = df_metier_landing_all_year,
                                       aes(x= as.factor(REF_YEAR), y=mean_landing,
                                           fill= as.factor(Species),
                                           group=as.factor(Species))) +
  geom_col(alpha=0.8 , size=.5) +
  scale_fill_viridis(discrete = T) +
  ylab("Average quantity landed\n") +
  xlab("Super metier built") +
  ggtitle("Catch profil by super-metier without other species") +
  facet_grid(~ super_metier)

plot_metier_landing_all_year


## BUILT THE SAME PLOT : without another species

cat(bgMagenta("Without other species"))
# PLOT CATCH PROFIL : with other species
df_metier_landing_all_year <- pivot_longer(df_eflalo, cols = starts_with("LE_KG"),
                                           names_to = "Species") %>%
  filter(value > 0) %>%
  dplyr::select(Species, super_metier, REF_YEAR, value) %>% #value= landed quantity
  filter(! is.na(super_metier),
         ! is.na(super_metier)) %>%
  group_by(REF_YEAR, super_metier, Species) %>%
  mutate(Species = as_factor(str_sub(Species, 7, 9))) %>%
  summarise(
    mean_landing= mean(value)
  )

plot_metier_landing_all_year <- ggplot(data = df_metier_landing_all_year,
                                       aes(x= as.factor(REF_YEAR), y=mean_landing,
                                           fill= as.factor(Species),
                                           group=as.factor(Species))) +
  geom_col(alpha=0.8 , size=.5) +
  scale_fill_viridis(discrete = T) +
  ylab("Average quantity landed\n") +
  xlab("Super metier built") +
  ggtitle("Catch profil by super-metier without other species") +
  facet_grid(~ super_metier)

plot_metier_landing_all_year
