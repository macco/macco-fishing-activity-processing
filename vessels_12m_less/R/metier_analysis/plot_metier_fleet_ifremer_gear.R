###-----------------------------------------------------------------------------
### plot gear composition per Metier isis proportion
df_metier_isis_prop_fleet_ifremer <- df_eflalo %>%
  group_by(REF_YEAR, metier_isis, S_FLOTTILLE_IFREMER_LIB) %>%
  summarise(n_seq = n()) %>%
  group_by(REF_YEAR, S_FLOTTILLE_IFREMER_LIB) %>%
  mutate(total_seq = sum(n_seq),
         prop_seq = n_seq/total_seq,
         # VE_FLT = fct_shuffle(VE_FLT),
         metier_isis = factor(metier_isis)) %>%
  # filter(prop_seq > 0.05) %>%
  droplevels()

colourCount = length(unique(df_metier_isis_prop_fleet_ifremer$metier_isis))
getPalette = colorRampPalette(brewer.pal(9, "Set1"))



list_plot_metier_isis_prop_fleet_ifremer <- df_metier_isis_prop_fleet_ifremer %>%
  group_by(S_FLOTTILLE_IFREMER_LIB) %>%
  group_split() %>%
  setNames(., nm = map(.x = ., ~ glue("{first(.x$S_FLOTTILLE_IFREMER_LIB)}"))) %>%
  purrr::map(.x = ., ~
  ggplot() +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust = 1)) +
  geom_col(data = .x,
           aes(x = REF_YEAR,
               y = prop_seq,
               fill = metier_isis)) +
  ylab("Number of fishing sequences") +
  xlab("Metier isis") +
  # ylim(0, 1) +
  scale_fill_manual(values = getPalette(colourCount), name = "Engin") +
  facet_wrap(~S_FLOTTILLE_IFREMER_LIB))

saveRDS(list_plot_metier_isis_prop_fleet_ifremer,
        file = here(path_data_tidy_plots,
                    "/list_plot_metier_isis_prop_fleet_ifremer.rds"))

###-----------------------------------------------------------------------------
### plot gear composition per Metier isis proportion
df_gear_prop_fleet_ifremer <- df_eflalo %>%
  group_by(REF_YEAR, VE_FLT, S_FLOTTILLE_IFREMER_LIB) %>%
  summarise(n_seq = n()) %>%
  group_by(REF_YEAR, S_FLOTTILLE_IFREMER_LIB) %>%
  mutate(total_seq = sum(n_seq),
         prop_seq = n_seq/total_seq,
         VE_FLT = fct_shuffle(VE_FLT)) %>%
  # filter(prop_seq > 0.05) %>%
  droplevels()

colourCount = length(unique(df_gear_prop_fleet_ifremer$VE_FLT))
getPalette = colorRampPalette(brewer.pal(9, "Set1"))



list_plot_gear_prop_fleet_ifremer <- df_gear_prop_fleet_ifremer %>%
  group_by(S_FLOTTILLE_IFREMER_LIB) %>%
  group_split() %>%
  setNames(., nm = map(.x = ., ~ glue("{first(.x$S_FLOTTILLE_IFREMER_LIB)}"))) %>%
  purrr::map(.x = ., ~
               ggplot() +
               theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust = 1)) +
               geom_col(data = .x,
                        aes(x = REF_YEAR,
                            y = prop_seq,
                            fill = VE_FLT)) +
               ylab("Number of fishing sequences") +
               xlab("Metier isis") +
               # ylim(0, 1) +
               scale_fill_manual(values = getPalette(colourCount),
                                 name = "Engin") +
               facet_wrap(~S_FLOTTILLE_IFREMER_LIB))


saveRDS(list_plot_gear_prop_fleet_ifremer,
        file = here(path_data_tidy_plots,
                    "/list_plot_gear_prop_fleet_ifremer.rds"))
