################################################################################
### Plot effort per LE_RECT
################################################################################

###-----------------------------------------------------------------------------
### plot of LE_EFF
###-----------------------------------------------------------------------------

###-----------------------------------------------------------------------------
### use purrr to make a plot-map for each metier and store it in a list
list_df_effort_metier_zone <- purrr::map(
  .x = levels(df_effort_metier_zone$metier),
  .f = ~{
    df_effort_metier_rectstat %>%
      filter(metier == .x)
  })


# list_df_effort_metier_zone =df_effort_metier_zone %>%
#   group_by(metier, zone) %>%
#   group_split()


list_hist_effort_metier_rectstat <- purrr::map(list_df_effort_metier_zone,
                                             ~{
                                               ggplot() +
                                                 geom_col(data = .x, aes(x = LE_RECT, y = per_LE_EFF)) +
                                                 facet_wrap(zone ~ MONTH, ncol = 3) +
                                                 theme(plot.title = element_text(size = 10, face = "bold"),
                                                       # legend.title = element_text(size = 10),
                                                       axis.text = element_text(size = 4),
                                                       axis.title = element_text(size = 8),
                                                       strip.text.x = element_text( size = 5,
                                                                                    margin = margin(.01, 0, .01, 0, "cm")),
                                                       legend.key.width = unit(1, 'cm'),
                                                       legend.key.height = unit(0.25, 'cm'),
                                                       legend.position = "bottom",
                                                       legend.direction = "horizontal"
                                                 ) +
                                                 ylim(0, 100) +
                                                 geom_hline(yintercept = threshold_effort_metier_zone * 100) +
                                                 ggtitle(paste0("Percentage of effort per rec for metier ", .x$metier, " (", LE_EFF_ORIGIN, ")"))
                                             })

###-----------------------------------------------------------------------------
### save plots if save_plot == TRUE
purrr::map2(.x = levels(df_effort_metier_rectstat$metier),
            .y = list_hist_effort_metier_rectstat,
            .f = ~{
              f_save_plot(.y,
                          file_name = paste0("hist_effort_metier_rectstat_", .x),
                          path = path_figure,
                          save = save_plot,
                          width = 29,
                          height = 21,
                          device = 'pdf',
                          units = 'cm')})
