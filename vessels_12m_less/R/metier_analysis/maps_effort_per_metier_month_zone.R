################################################################################
### Plot effort per LE_RECT
################################################################################

###-----------------------------------------------------------------------------
### set data.frame as sf object
sf_effort_metier_zone <- left_join(df_effort_metier_zone, sf_ices_rec,
                                     by = c("LE_RECT" = "ICESNAME")) %>%
  st_sf() %>%
  st_difference(., st_combine(sf_coast_line))

###-----------------------------------------------------------------------------
### plot of LE_EFF
###-----------------------------------------------------------------------------

###-----------------------------------------------------------------------------
### use purrr to make a plot-map for each metier and store it in a list
list_sf_effort_metier_zone <- purrr::map(
  levels(sf_effort_metier_zone$metier),
  ~{
    sf_effort_metier_zone %>%
      filter(metier == .x)
  })

list_maps_effort_metier_rectstat <- purrr::map(list_sf_effort_metier_zone,
                                             ~{
                                               plot_empty_map +
                                                 geom_sf(data = .x, aes(fill = per_LE_EFF), size = 0.01) +
                                                 # geom_sf(data = sf_region_rec, fill = NA, size = 0.01) +
                                                 geom_sf(data = sf_zones, fill = NA, aes(colour = zone), size = 0.05) +
                                                 scale_fill_viridis_c(name = "Effort") +
                                                 facet_wrap( ~ MONTH, ncol = 3) +
                                                 theme(plot.title = element_text(size = 10, face = "bold"),
                                                       # legend.title = element_text(size = 10),
                                                       axis.text = element_text(size = 4),
                                                       axis.title = element_text(size = 8),
                                                       strip.text.x = element_text( size = 5,
                                                                                    margin = margin(.01, 0, .01, 0, "cm")),
                                                       legend.key.width = unit(1, 'cm'),
                                                       legend.key.height = unit(0.25, 'cm'),
                                                       legend.position = "bottom",
                                                       legend.direction = "horizontal"
                                                 ) +
                                                 ggtitle(paste0("Percentage of effort for metier ", .x$metier, " (", LE_EFF_ORIGIN, ")"))
                                             })
list_maps_effort_metier_rectstat[[1]]

###-----------------------------------------------------------------------------
### save plots if save_plot == TRUE
purrr::map2(.x = levels(sf_effort_metier_rectstat$metier),
            .y = list_maps_effort_metier_rectstat,
            .f = ~{
              f_save_plot(.y,
                          file_name = paste0("maps_effort_metier_zone_per", .x),
                          path = path_figure,
                          save = save_plot,
                          width = 29,
                          height = 21,
                          device = 'pdf',
                          units = 'cm')})
