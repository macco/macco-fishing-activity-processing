###-----------------------------------------------------------------------------
### sum effort per LE_RECT
df_effort_metier_zone <- df_eflalo %>%
  dplyr::select(LE_EFF,
         REF_YEAR,
         MONTH,
         zone,
         metier) %>%
  group_by(REF_YEAR,
           MONTH,
           metier,
           zone) %>%
  summarise(LE_EFF_zone = sum(LE_EFF))


### sum effort per ICES rec stat
df_effort_metier_zone <- left_join(df_effort_metier_zone,
                                 dplyr::select(df_effort_metier_rectstat,
                                        -sum_LE_EFF,-per_LE_EFF)) %>%

  mutate(per_LE_EFF = round(LE_EFF / LE_EFF_zone  * 100, digits = 1))
