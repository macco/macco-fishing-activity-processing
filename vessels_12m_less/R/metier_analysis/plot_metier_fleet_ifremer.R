
###-----------------------------------------------------------------------------
### Compute summary for plotting

df_metier_isis_landing_all_years_fleetifremer <- pivot_longer(df_eflalo,
                                                              cols = starts_with("LE_KG"),
                                                              names_to = "Species") %>%
  filter(value > 0) %>%
  dplyr::select(Species, metier_isis, S_FLOTTILLE_IFREMER_LIB, value) %>% #value= landed quantity
  group_by(metier_isis, Species, S_FLOTTILLE_IFREMER_LIB) %>%
  mutate(Species = as_factor(str_sub(Species, 7, 9)),
         Species = fct_relevel(Species, "OTH", after = Inf)) %>%
  summarise(mean_landing = mean(value))


dfs_metier_isis_landing_fleet_ifremer <- df_metier_isis_landing_all_years_fleetifremer %>%
  group_by(metier_isis, S_FLOTTILLE_IFREMER_LIB) %>%
  mutate(total_landing = sum(mean_landing),
         prop_landing_species = mean_landing / total_landing)


### Compute summary for plotting
dfs_metier_isis_landing_fleet_ifremer_noOTH <- df_metier_isis_landing_all_years_fleetifremer %>%
  filter(Species != "OTH") %>%
  group_by(metier_isis, S_FLOTTILLE_IFREMER_LIB) %>%
  mutate(total_landing = sum(mean_landing)) %>%
  mutate(prop_landing_species = mean_landing / total_landing)


###-----------------------------------------------------------------------------
### plot landings profiles
list_plots_prop_landings_metier_isis_fleet_ifremer <- dfs_metier_isis_landing_fleet_ifremer %>%
  group_by(S_FLOTTILLE_IFREMER_LIB) %>%
  group_split() %>%
  setNames(., nm = map(.x = ., ~ glue("{first(.x$S_FLOTTILLE_IFREMER_LIB)}"))) %>%
  purrr::map(.x = ., ~ ggplot(data = .x,
                              aes(x = metier_isis,
                                  y = prop_landing_species,
                                  fill = Species,
                                  group = Species)) +
               theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust = 1)) +
               geom_col() +
               xlab("Metier isis") +
               ylab("Proportion of mean landings (kg)") +
               scale_fill_manual(values = scale_color_species_isis) +
               facet_grid(~S_FLOTTILLE_IFREMER_LIB, scales = "free_x") )

saveRDS(list_plots_prop_landings_metier_isis_fleet_ifremer,
        file = here(path_data_tidy_plots,
                    "/list_plots_prop_landings_metier_isis_fleet_ifremer.rds"))


###-----------------------------------------------------------------------------
### plot landings profiles
list_plots_prop_landings_metier_isis_fleet_ifremer_noOTH <- dfs_metier_isis_landing_fleet_ifremer_noOTH %>%
  group_by(S_FLOTTILLE_IFREMER_LIB) %>%
  group_split(.keep = TRUE) %>%
  setNames(., nm = map(.x = ., ~ glue("{first(.x$S_FLOTTILLE_IFREMER_LIB)}"))) %>%
  purrr::map( ~ ggplot(data = .x,
                              aes(x = metier_isis,
                                  y = prop_landing_species,
                                  fill = Species,
                                  group = Species)) +
               theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust = 1)) +
               geom_col() +
               xlab("Metier isis") +
               ylab("Proportion of mean landings (kg)") +
               scale_fill_manual(values = scale_color_species_isis) +
               facet_grid(~S_FLOTTILLE_IFREMER_LIB, scales = "free_x") )

saveRDS(list_plots_prop_landings_metier_isis_fleet_ifremer_noOTH,
        file = here(path_data_tidy_plots,
                    "/list_plots_prop_landings_metier_isis_fleet_ifremer_noOTH.rds"))
