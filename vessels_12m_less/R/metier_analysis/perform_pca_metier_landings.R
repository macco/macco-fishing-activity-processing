################################################################################
### Perform a PCA on metier given landings in KG
################################################################################

### Shape df for PCA
df_metier_landing_wider_pca <- df_metier_landing %>%
  dplyr::select(REF_YEAR, metier, Species, mean_landing) %>%
  pivot_wider(., names_from = Species,
              values_from = mean_landing,
              values_fill = 0)

###-----------------------------------------------------------------------------
### Perform PCA for each year
###-----------------------------------------------------------------------------
cat(bgMagenta("STEP 1 - Perform PCA for each year "))

list_ACP_metier_landing <- df_metier_landing_wider_pca %>%
  group_by(REF_YEAR) %>%
  group_split() %>%
  #set_names(., nm = map(.x = ., ~ glue("year_{first(.x$REF_YEAR)}"))) %>%
  purrr::map(~ .x %>% column_to_rownames(var = "metier") %>%
        dplyr::select(-c("REF_YEAR", "OTH")) %>%
        PCA(., graph = FALSE))

names(list_ACP_metier_landing) <- year_vec

plot_ACP_metier_landing <- map(list_ACP_metier_landing, ~
                                 plot(.x, choix = "var",
                                      autoLab = "yes",
                                      graph.type = c("ggplot")))

###-----------------------------------------------------------------------------
### Perform PCA for a mean year (mean of landings over the time period)
###-----------------------------------------------------------------------------
cat(bgMagenta("STEP 2 - Perform PCA for all years (mean year) "))

### Shape df for PCA
df_metier_landing_wider_all_years_pca <- df_metier_landing_all_years %>%
  pivot_wider(., names_from = Species,
              values_from = mean_landing,
              values_fill = 0)

###-----------------------------------------------------------------------------
### Perform PCA for all year
###-----------------------------------------------------------------------------
ACP_metier_landing_all_years <- df_metier_landing_wider_all_years_pca %>%
  dplyr::select(-gear_isis) %>%
  column_to_rownames(var = "metier") %>%
  #dplyr::select(-c("OTH")) %>%
  PCA(graph = FALSE)

plot_ACP_metier_landing_all_years <- plot(ACP_metier_landing_all_years,
                                          choix = "var", autoLab = "yes",
                                          graph.type = c("ggplot"))

### save plots if save_plot == TRUE
if(save_plot) {
  f_save_plot(plot_ACP_metier_landing_all_years,
              file_name = paste0("ACP_metier_landing_",
                                 min(year_vec),"_",
                                 max(year_vec)),
              path = path_figure,
              save = save_plot)
}
