################################################################################
### export effort for isis
################################################################################


f_isis_export_average_strategies <- function(df_effort){

  ### export one wide table per fleet
  df_isis_export_effort <- df_effort %>%
    ungroup() %>%
    select(MONTH, isis_metier_tag, prop_effort) %>%
    pivot_wider(names_from = MONTH,
                values_from = prop_effort,
                id_cols = isis_metier_tag )

  average_str_name <- df_effort %>%
    mutate(isis_fleet = as.character(isis_fleet),
           isis_fleet = str_replace(isis_fleet, "\\s", "_"),
           isis_fleet = str_replace(isis_fleet, ">", "inf"),
           isis_fleet = str_replace(isis_fleet, "<", "sup")) %>%
    pull(isis_fleet) %>%
    unique()

  write.table(df_isis_export_effort,
              sep = ";", row.names = FALSE,
              col.names = FALSE,
              file = paste0(path_isis_data, "/average_strategies_",
                            starting_year, "_", ending_year, "/average_str_",
                            average_str_name, ".csv"))
}



# fn_isis_export_average_strategies <- function(df_effort, var_str){
#   ### export one wide table per fleet
#   df_isis_export_effort <- df_effort %>%
#     ungroup() %>%
#     filter(isis_fleet == {{var_str}}) %>%
#     select(MONTH, isis_metier_tag, prop_effort) %>%
#     pivot_wider(names_from = MONTH,
#                 values_from = prop_effort,
#                 id_cols = isis_metier_tag )
#
#   average_str_name <- df_effort %>%
#     filter(isis_fleet == {{var_str}}) %>%
#     mutate(isis_fleet = as.character(isis_fleet),
#            isis_fleet = str_replace(isis_fleet, "\\s", "_"),
#            isis_fleet = str_replace(isis_fleet, ">", "inf"),
#            isis_fleet = str_replace(isis_fleet, "<", "sup")) %>%
#     pull(isis_fleet) %>%
#     unique()
#
#   write.table(df_isis_export_effort,
#               sep = ";", row.names = FALSE,
#               col.names = FALSE,
#               file = paste0(path_isis_data, "/AverageStrategies_",
#                             starting_year, "_", ending_year, "/average_str_",
#                             average_str_name, ".csv"))
#
# }
