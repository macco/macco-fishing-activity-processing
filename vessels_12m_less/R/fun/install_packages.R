################################################################################
### Install last version of VMS tools
### https://github.com/nielshintzen/vmstools/
################################################################################

### From vmstools webapge, install vmstools dependency:
vmstoolsPackages <-  c("cluster",
                       "data.table",
                       "doBy","maps",
                       "mapdata","maptools",
                       "PBSmapping",
                       "sp",
                       "remotes",
                       "FactoMineR",
                       "SOAR",
                       "amap",
                       "MASS",
                       "mda")

invisible(sapply(vmstoolsPackages, function(Package)
  if (!Package %in% installed.packages()[, 1]) {install.packages(Package)}))

### use remotes package to install vmstools version 0.75, last version in march 2020
if (!"vmstools" %in% installed.packages()[, 1]) {
  require("remotes")
  remotes::install_url("https://github.com/nielshintzen/vmstools/releases/download/0.75/vmstools_0.75.tar.gz")
}


if (!"COSTcore" %in% installed.packages()[, 1]) {
  if (! "tcltk2" %in% installed.packages()[, 1]) {install.packages( "tcltk2")}
  cat("Please install COST packages from CREDO ifremer server \n")
}
