f_metier_compute_dist_cluster_year <- function(df_data, df_data_ref) {

  ## df_data a data frame with :
  ## REF_YEAR, meiter, and proprtion of landings per species as columns (one column per species)

  ## df_data_ref a data frame with :
  ## metier cluster_metier  REF_YEAR


  ### Set reference data as matrix
  matrix_ref <- df_data_ref %>%
    column_to_rownames("metier") %>%
    select(-cluster_metier, -REF_YEAR) %>%
    select(order(colnames(.),
                 decreasing = FALSE))

  rownames(matrix_ref) <- paste0("Ref_", rownames(matrix_ref))

  ### Set data to matrix
  df_data_for_dist <- df_data %>%
    column_to_rownames("metier") %>%
    dplyr::select(-REF_YEAR) %>%
    select(order(colnames(.),
                 decreasing = FALSE))


  ## compute distance
  dit_fit <- proxy::dist(df_data_for_dist, matrix_ref,
                         method = "Euclidean", by_rows = TRUE)

  ## extract as matrix for tibble conversion
  matrix_dist <- matrix(dit_fit,
                        nrow = dim(dit_fit)[1],
                        ncol = dim(dit_fit)[2],
                        dimnames = list(rownames(dit_fit),
                                        colnames(dit_fit)))

  ## Set to data frame, compute minimum and get cluster from min dist
  df_dist <- matrix_dist %>%
    as_tibble() %>%
    mutate(metier = rownames(dit_fit)) %>%
    pivot_longer(., cols = starts_with("Ref"),
                 names_to = "metier_ref",
                 values_to = "dist") %>%
    mutate(metier_ref = str_sub(metier_ref, start = 5)) %>%
    group_by(metier) %>%
    slice(which.min(dist)) %>%
    left_join(., df_data_ref,
              by = c("metier_ref" = "metier")) %>%
    mutate(REF_YEAR = unique(df_data$REF_YEAR))
  return(df_dist)
}
