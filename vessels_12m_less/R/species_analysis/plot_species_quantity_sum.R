################################################################################
### plot quantity as a proportion of the cumulative sum
################################################################################

###-----------------------------------------------------------------------------
### create a data frame with species and quantity only
df_species <- df_sacrois %>%
  dplyr::select(REF_YEAR, ESP_COD_FAO, LE_EURO, LE_KG) %>%
  filter(!is.na(LE_EURO)) %>%
  group_by(ESP_COD_FAO, REF_YEAR) %>%
  summarise(LE_EURO = sum(LE_EURO),
            LE_KG = sum(LE_KG))  %>%
  group_by(REF_YEAR) %>%
  arrange(REF_YEAR, desc(LE_KG)) %>%
  mutate(cumSumKG = cumsum(LE_KG),
         sumKG = sum(LE_KG),
         propKG_cumSum = round(cumSumKG/sumKG, digits = 3)) %>%
  arrange(REF_YEAR, propKG_cumSum) %>%
  mutate(ESP_COD_FAO = fct_reorder2(ESP_COD_FAO, propKG_cumSum, REF_YEAR)) %>%
  mutate(species_selection = ifelse(threshold_KG_prop_species > propKG_cumSum,
                                    "keep", "discard")) %>%
  slice(1:60) #new pour plus de visibilitee

###-----------------------------------------------------------------------------
### make plot for proportion of revenue in kilo per species and year
list_plot_species_select <- purrr::map(.x = year_vec,
                                       .f = ~{
                                         filter(df_species, REF_YEAR == .x) %>%
                                           ungroup() %>%
                                           mutate(ESP_COD_FAO = fct_reorder(ESP_COD_FAO,
                                                                            propKG_cumSum)) %>%
                                           ggplot(., aes(x = ESP_COD_FAO,
                                                         y = propKG_cumSum,
                                                         fill = species_selection)) +
                                           geom_col() +
                                           ylab("Proportion in kilo") +
                                           xlab("Species") +
                                           ylim(0, 1) +
                                           # geom_hline(aes(yintercept = threshold_euro_prop_metier)) +
                                           facet_grid( ~  REF_YEAR) +
                                           theme(axis.text.x = element_text(angle = 90,
                                                                            vjust = 0.5,
                                                                            hjust = 1,
                                                                            size= 7),
                                                 legend.background = element_rect(fill = "lightblue",
                                                                                  size = 0.4,
                                                                                  linetype = "solid",
                                                                                  colour = "darkblue"),
                                                 legend.title = element_blank(),
                                                 legend.position=c(0.9, 0.7))
                                       })

### print plot
if (show_plot) {
  sapply(list_plot_species_select, print)
}

###-----------------------------------------------------------------------------
### save plots if save_plot == TRUE
purrr::map2(.x = year_vec,
            .y = list_plot_species_select,
            .f = ~{
              f_save_plot(.y,
                          file_name = paste0("species_select_", .x),
                          path = path_figure,
                          save = save_plot)})


saveRDS(list_plot_species_select,
        file = paste0(path_fishery_data_tidy_plots,
                      "/list_plot_species_select.rds"))
