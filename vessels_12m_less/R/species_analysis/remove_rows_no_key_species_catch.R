################################################################################
### Use with Caution : remove fishing sequence for which no key species are caught
################################################################################

###-----------------------------------------------------------------------------
### Remove rows where there is no catch of the key species
if (exists("key_species")) {
  if (!is.null(key_species)) {

    species_key_condtions <- map(key_species, str_subset,
                                 string = names(df_eflalo)) %>%
      unlist()  %>%
      paste(. ,'> 0', collapse =  " | ")


    df_eflalo <- df_eflalo %>%
      filter(eval(rlang::parse_expr(species_key_condtions)))

  }
} else {
  key_species <- NULL
}
