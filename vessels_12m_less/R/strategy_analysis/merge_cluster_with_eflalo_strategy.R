###-----------------------------------------------------------------------------
### join clustering results with eflalo_strategy
df_eflalo_strategy <- left_join(df_eflalo_strategy, df_strategy,
                                by = c("VE_REF",
                                       "metier_isis",
                                       # "metier_isis_REF_YEAR_MONTH",
                                       "fleet"))

###-----------------------------------------------------------------------------
### Compute summary for plotting
###-----------------------------------------------------------------------------

###
df_cluster_strategy_landing_all_years <- pivot_longer(df_eflalo_strategy,
                                                      cols = starts_with("LE_KG"),
                                                      names_to = "Species") %>%
  filter(value > 0) %>%
  dplyr::select(Species, cluster_strategy, value) %>% #value= landed quantity
  group_by(cluster_strategy, Species) %>%
  mutate(Species = as_factor(str_sub(Species, 7, 9)),
         Species = fct_relevel(Species, "OTH", after = Inf)) %>%
  summarise(mean_landing = mean(value),
            n_seq = n()) %>%
  group_by(cluster_strategy) %>%
  mutate(prop_landing_species = mean_landing / sum(mean_landing))

df_cluster_strategy_landing_all_years_no_OTH <- pivot_longer(df_eflalo_strategy,
                                                             cols = starts_with("LE_KG"),
                                                             names_to = "Species") %>%
  filter(value > 0) %>%
  dplyr::select(Species, cluster_strategy, value) %>% #value= landed quantity

  mutate(Species = as_factor(str_sub(Species, 7, 9)),
         Species = fct_relevel(Species, "OTH", after = Inf)) %>%
  filter(Species != "OTH") %>%
  group_by(cluster_strategy, Species) %>%
  summarise(mean_landing = mean(value),
            n_seq = n()) %>%
  group_by(cluster_strategy) %>%
  mutate(prop_landing_species = mean_landing / sum(mean_landing))


###
df_cluster_strategy_vessels <- df_strategy %>%
  select(VE_REF, cluster_strategy, fleet) %>%
  distinct() %>%
  group_by(cluster_strategy) %>%
  summarise(n_vessels = n())

###
df_cluster_strategy_metier_isis <- df_eflalo_strategy %>%
  group_by(REF_YEAR, MONTH, metier_isis, cluster_strategy, fleet) %>%
  summarise(n_seq = n()) %>%
  group_by(MONTH, metier_isis, cluster_strategy, fleet) %>%
  summarise(mean_n_seq = mean(n_seq)) %>%
  group_by(MONTH, cluster_strategy, fleet) %>%
  mutate(prop_metier_isis = mean_n_seq / sum(mean_n_seq))
