

###-----------------------------------------------------------------------------
###
plot_fleet_n_vessels <- df_n_vessel_per_fleet %>%
  select(fleet, n_vessel_per_fleet) %>%
  distinct() %>%
  ggplot() +
  geom_col(     aes(x = fleet, y = n_vessel_per_fleet)) +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust = 1)) +
  ylab("Number of vessels") +
  xlab("Super strategy")


f_save_plot(plot_fleet_n_vessels,
            file_name = paste0("plot_fleet_n_vessels"),
            path = path_figure,
            save = save_plot)


if (show_plot) {
  print(plot_fleet_n_vessels)
}

list_plot_fleet <- list(plot_fleet_n_vessels = plot_fleet_n_vessels)

saveRDS(list_plot_fleet,
        file = here(path_data_tidy_plots, "list_plot_fleet.rds"))



