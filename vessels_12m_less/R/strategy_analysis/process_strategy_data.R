################################################################################
### Summarize strategys data for plotting
################################################################################

###-----------------------------------------------------------------------------
### Summarize landings per species as the mean for each year and metier
df_strategy_landing_all_years <- pivot_longer(df_eflalo,
                                              cols = starts_with("LE_KG"),
                                              names_to = "Species") %>%
  filter(value > 0) %>%
  dplyr::select(Species, strategy_isis, metier_isis, value) %>% #value= landed quantity
  group_by(strategy_isis, metier_isis, Species) %>%
  mutate(Species = as_factor(str_sub(Species, 7, 9)),
         Species = fct_relevel(Species, "OTH", after = Inf)) %>%
  summarise(mean_landing = mean(value))


###-----------------------------------------------------------------------------
### extract unique Vessel, year and strategy
df_strategy_vessel <- df_eflalo %>%
  dplyr::select(VE_REF, strategy_isis, REF_YEAR) %>%
  distinct()

###-----------------------------------------------------------------------------
### count number of vessels per strategy and year
dfs_strategy_nvessel_per_year <- df_strategy_vessel %>%
  group_by(REF_YEAR, strategy_isis) %>%
  summarise(n_vessels = n())

###-----------------------------------------------------------------------------
df_strategy_nvessel_mean_per_year <- df_strategy_vessel %>%
  group_by(REF_YEAR, strategy_isis) %>%
  summarise(n_vessels = n()) %>%
  group_by(strategy_isis) %>%
  summarise(n_vessels_mean = mean(n_vessels))

###-----------------------------------------------------------------------------
### extract vessels with more than one strategy = which is not possible in ISIS
dfs_strategy_vessel_nyear <- df_strategy_vessel %>%
  group_by(VE_REF, strategy_isis) %>%
  distinct() %>%
  summarise(n_year = n())

