###-----------------------------------------------------------------------------
### plot metier zone
source("R/metier_analysis/map_metier_zone.R")

###-----------------------------------------------------------------------------
### plot effort per LE_RECT
source("R/metier_analysis/compute_effort_metier_rect.R")
source("R/metier_analysis/plot_effort_per_metier_month_rect.R") #Dans le cas où on garde 80% de l'effort total
source("R/metier_analysis/maps_effort_per_metier_month_rect.R")

###-----------------------------------------------------------------------------
### plot effort per zones : # de rect-stat > Zones dans le cas où on renseigne les zones
## NE CONCERNE PAS LA DESCRIPTION DES (-) de 12m
#if (nlevels(df_eflalo$LE_RECT) > nlevels(df_eflalo$zone) &
    #make_maps) {

  #source("R/metier_analysis/compute_effort_metier_zone.R")
  #source("R/metier_analysis/plot_effort_per_zones.R")
  #source("R/metier_analysis/plot_effort_per_metier_month_zone.R")
  #source("R/metier_analysis/maps_effort_per_metier_month_zone.R")
#}
