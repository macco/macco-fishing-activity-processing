################################################################################
### Create a buffer from the coastline (sf_coast_line) and
### extract isis grid square inside the buffer
################################################################################

###-----------------------------------------------------------------------------
### Buffer of ~ 20 miles, 1 miles ~ 1.6 km
### 0.1 ~ 11.1 km
sf_coast_line_buffer <- sf_coast_line %>%
  st_buffer(dist = 0.3)

###-----------------------------------------------------------------------------
sf_0_10_isis_square <-  st_intersection(isis_grid_sf_rect,
                         sf_coast_line_buffer)

###-----------------------------------------------------------------------------
### make map of buffer
map_sf_0_10_isis_square <- ggplot() +
  geom_sf(data = isis_grid_sf_rect) +
  geom_sf(data = sf_0_10_isis_square, fill = "red")

###-----------------------------------------------------------------------------
### extract info from isis grid square
df_0_10_isis_square <- sf_0_10_isis_square %>%
  st_drop_geometry() %>%
  dplyr::select(square_isis) %>%
  distinct()
