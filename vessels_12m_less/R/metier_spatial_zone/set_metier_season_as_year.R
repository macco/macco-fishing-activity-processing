################################################################################
### Set metier seasons
################################################################################

###-----------------------------------------------------------------------------
### define season, default season = REF_YEAR
df_eflalo <- df_eflalo %>%
  mutate(season = REF_YEAR)

###-----------------------------------------------------------------------------
### set metier zone season
df_eflalo <- df_eflalo %>%
  mutate(metier_zone = paste(metier, zone, sep = "_"),
         metier_zone_season = paste(metier, zone, season, sep = "_")) %>%
  group_by()
