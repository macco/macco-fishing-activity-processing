################################################################################
### Make map for internship analysis : make mapo with Sacrois data
################################################################################



###-----------------------------------------------------------------------------
### Average sum effort per LE_RECT for considering years
sf_effort_rect_stat <- df_eflalo %>%
  select(SECT_COD_SACROIS_NIV5, REF_YEAR, LE_EFF, metier_isis) %>%
  filter(metier_isis %in% metier_isis_name) %>%
  mutate(across(where(is.factor), forcats::fct_drop)) %>%
  group_by(SECT_COD_SACROIS_NIV5, REF_YEAR, metier_isis) %>%
  summarise(LE_EFF = sum(LE_EFF)) %>%
  right_join(., df_eflalo_rect_stat_full_metier) %>%
  mutate(LE_EFF = ifelse(is.na(LE_EFF), 0, LE_EFF)) %>%
  group_by(REF_YEAR, metier_isis) %>%
  mutate(sum_LE_EFF = sum(LE_EFF),
         per_LE_EFF = round(LE_EFF / sum_LE_EFF * 100, digits = 1)) %>%
  # filter(per_LE_EFF > 0.01) %>%
  # mutate(sum_LE_EFF = sum(LE_EFF),
  #        per_LE_EFF = round(LE_EFF / sum_LE_EFF * 100, digits = 1)) %>%
  group_by(SECT_COD_SACROIS_NIV5, metier_isis) %>%
  summarise(per_LE_EFF_mean = mean(per_LE_EFF),
            LE_EFF_mean = mean(LE_EFF))  %>%
  filter(metier_isis %in% metier_isis_name) %>%
  group_by(metier_isis) %>%
  mutate(LE_EFF_total_rect_stat = sum(LE_EFF_mean, na.rm = TRUE)) %>%
  mutate(across(contains("LE_EFF"), ~ ifelse(. == 0, NA, .))) %>%
  left_join(., sf_ices_rec,
            by = c("SECT_COD_SACROIS_NIV5" = "ICESNAME")) %>%
  st_sf()

df_effort_rect_stat <- sf_effort_rect_stat %>%
  st_drop_geometry()

###-----------------------------------------------------------------------------
### plot of % of LE_EFF
list_map_effort_rect_stat_metier <- sf_effort_rect_stat %>%
  group_by(metier_isis) %>%
  group_split() %>%
  set_names(., nm = map(.x = ., ~ glue("{first(.x$metier_isis)}"))) %>%
  map(.x = ., ~ f_map_var(df_data = .x,
                          var_to_fill = "LE_EFF_mean",
                          legend_label =   "Effort (h)",
                          var_to_fill_range = NULL,
                          var_to_facet = NULL,
                          title_label = glue("ICES rectangle"),
                          title_var = NULL,
                          round_scale = TRUE,
                          coast_line_map = plot_empty_map))

list_plots_effort_rect_stat_metier$list_map_effort_rect_stat_metier <- list_map_effort_rect_stat_metier


list_map_effort_rect_stat_metier_per <- sf_effort_rect_stat %>%
  group_by(metier_isis) %>%
  group_split() %>%
  set_names(., nm = map(.x = ., ~ glue("{first(.x$metier_isis)}"))) %>%
  map(.x = ., ~ f_map_var(df_data = .x,
                          var_to_fill = "per_LE_EFF_mean",
                          legend_label =   "Percentage effort (h)",
                          var_to_fill_range = NULL,
                          var_to_facet = NULL,
                          title_label = glue("ICES rectangle"),
                          title_var = NULL,
                          round_scale = TRUE,
                          coast_line_map = plot_empty_map) +
        geom_sf(data = isis_grid_sf_rect, fill = NA))

list_plots_effort_rect_stat_metier$list_map_effort_rect_stat_metier_per <- list_map_effort_rect_stat_metier_per



sf_effort_ss_rect_stat_metier <-  df_eflalo %>%
  select(SECT_COD_SACROIS_NIV6, REF_YEAR, LE_EFF, metier_isis) %>%
  filter(metier_isis %in% metier_isis_name) %>%
  mutate(across(where(is.factor), forcats::fct_drop)) %>%
  group_by(SECT_COD_SACROIS_NIV6, REF_YEAR, metier_isis) %>%
  summarise(LE_EFF = sum(LE_EFF)) %>%
  right_join(., df_eflalo_ss_rect_stat_full) %>%
  mutate(LE_EFF = ifelse(is.na(LE_EFF), 0, LE_EFF)) %>%
  group_by(REF_YEAR, metier_isis) %>%
  mutate(sum_LE_EFF = sum(LE_EFF),
         per_LE_EFF = round(LE_EFF / sum_LE_EFF * 100, digits = 1)) %>%
  # filter(per_LE_EFF > 0.01) %>%
  # mutate(sum_LE_EFF = sum(LE_EFF),
  #        per_LE_EFF = round(LE_EFF / sum_LE_EFF * 100, digits = 1)) %>%
  group_by(SECT_COD_SACROIS_NIV6, metier_isis) %>%
  summarise(per_LE_EFF_mean = mean(per_LE_EFF),
            LE_EFF_mean = mean(LE_EFF))

effort_ss_rect_stat_missing <- sf_effort_ss_rect_stat_metier %>%
  filter(SECT_COD_SACROIS_NIV6 == "")

sf_effort_ss_rect_stat_filter <- df_eflalo %>%
  select(SECT_COD_SACROIS_NIV6, REF_YEAR, LE_EFF, metier_isis) %>%
  filter(metier_isis %in% metier_isis_name) %>%
  mutate(across(where(is.factor), forcats::fct_drop)) %>%
  group_by(SECT_COD_SACROIS_NIV6, REF_YEAR, metier_isis) %>%
  filter(SECT_COD_SACROIS_NIV6 != "") %>%
  summarise(LE_EFF = sum(LE_EFF)) %>%
  right_join(., df_eflalo_ss_rect_stat_full) %>%
  mutate(LE_EFF = ifelse(is.na(LE_EFF), 0, LE_EFF)) %>%
  # group_by(SECT_COD_SACROIS_NIV5) %>%
  group_by(REF_YEAR, metier_isis) %>%
  mutate(sum_LE_EFF = sum(LE_EFF),
         per_LE_EFF = round(LE_EFF / sum_LE_EFF * 100, digits = 1)) %>%
  # filter(per_LE_EFF > 0.01) %>%
  # mutate(sum_LE_EFF = sum(LE_EFF),
  # per_LE_EFF = round(LE_EFF / sum_LE_EFF * 100, digits = 1)) %>%
  group_by(SECT_COD_SACROIS_NIV6, metier_isis) %>%
  summarise(per_LE_EFF_mean = mean(per_LE_EFF),
            LE_EFF_mean = mean(LE_EFF)) %>%
  mutate(across(contains("LE_EFF"), ~ ifelse(. == 0, NA, .))) %>%
  right_join(., sf_ices_ss_rec,
             by = c("SECT_COD_SACROIS_NIV6" = "ICESNAME")) %>%
  st_sf() %>%
  filter(metier_isis %in% metier_isis_name)


###|----------------------------------------------------------------------------
###|compute missing effort when using sub rectangles only
sf_effort_ss_rect_stat <-  select(df_eflalo_ss_rect_stat_full,
                                  metier_isis, REF_YEAR, SECT_COD_SACROIS_NIV6) %>%
  left_join(., select(df_eflalo, SECT_COD_SACROIS_NIV6, REF_YEAR, LE_EFF, metier_isis)) %>%
  mutate(LE_EFF = ifelse(is.na(LE_EFF), 0, LE_EFF)) %>%
  group_by(SECT_COD_SACROIS_NIV6, REF_YEAR, metier_isis) %>%
  summarise(LE_EFF = sum(LE_EFF)) %>%
  # group_by(SECT_COD_SACROIS_NIV5) %>%
  group_by(REF_YEAR, metier_isis) %>%
  mutate(sum_LE_EFF = sum(LE_EFF),
         per_LE_EFF = round(LE_EFF / sum_LE_EFF * 100, digits = 1)) %>%
  filter(per_LE_EFF > 0.01) %>%
  mutate(sum_LE_EFF = sum(LE_EFF),
         per_LE_EFF = round(LE_EFF / sum_LE_EFF * 100, digits = 1)) %>%
  group_by(SECT_COD_SACROIS_NIV6, metier_isis) %>%
  summarise(per_LE_EFF_mean = mean(per_LE_EFF)) %>%
  left_join(., sf_ices_ss_rec,
            by = c("SECT_COD_SACROIS_NIV6" = "ICESNAME")) %>%
  st_sf()

effort_ss_rect_stat_missing <- sf_effort_ss_rect_stat %>%
  filter(SECT_COD_SACROIS_NIV6 == "") %>%
  rename(per_LE_EFF_missing = per_LE_EFF_mean) %>%
  mutate(per_LE_EFF_missing = round(per_LE_EFF_missing, digits = 0)) %>%
  select(metier_isis, per_LE_EFF_missing) %>%
  st_drop_geometry()


sf_effort_ss_rect_stat_filter <- left_join(sf_effort_ss_rect_stat_filter,
                                           effort_ss_rect_stat_missing)

###-----------------------------------------------------------------------------
### plot of % of LE_EFF
list_map_effort_ss_rect_stat_metier <- sf_effort_ss_rect_stat_filter %>%
  group_by(metier_isis) %>%
  group_split() %>%
  set_names(., nm = map(.x = ., ~ glue("{first(.x$metier_isis)}"))) %>%
  map(.x = ., ~ f_map_var(df_data = .x,
                          var_to_fill = "LE_EFF_mean",
                          legend_label =   "Effort (h)",
                          var_to_fill_range = NULL,
                          var_to_facet = NULL,
                          title_label = glue("Sub-rectangle ({unique(.x$per_LE_EFF_missing)}% of missing effort at the rectangle scale)"),
                          title_var = NULL,
                          round_scale = TRUE,
                          coast_line_map = plot_empty_map))

list_plots_effort_rect_stat_metier$list_map_effort_ss_rect_stat_metier <- list_map_effort_ss_rect_stat_metier

###-----------------------------------------------------------------------------
### plot of % of LE_EFF
list_map_effort_ss_rect_stat_metier_per <- sf_effort_ss_rect_stat_filter %>%
  group_by(metier_isis) %>%
  group_split() %>%
  set_names(., nm = map(.x = ., ~ glue("{first(.x$metier_isis)}"))) %>%
  map(.x = ., ~ f_map_var(df_data = .x,
                          var_to_fill = "per_LE_EFF_mean",
                          legend_label =   "Percentage effort (h)",
                          var_to_fill_range = NULL,
                          var_to_facet = NULL,
                          title_label = glue("Sub-rectangle ({unique(.x$per_LE_EFF_missing)}% of missing effort at the rectangle scale)"),
                          title_var = NULL,
                          round_scale = TRUE,
                          coast_line_map = plot_empty_map))

list_plots_effort_rect_stat_metier$list_map_effort_ss_rect_stat_metier_per <- list_map_effort_ss_rect_stat_metier_per

saveRDS(list_plots_effort_rect_stat_metier,
        file = glue("{path_data_tidy_plots}/list_plots_effort_rect_stat_metier.rds"))
