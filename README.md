# MACCO : Processing fishing data to parametrize ISIS-fish 2010-2022

## Objectives

- Reproducible methods to compute fisheries input for ISIS-fish models.
- Build reproducible and generic documentation describing the methods.

## Run the analysis over period 2010-2022

 The project is organised in 4 main sub-projetcts 
 "process_outputs_for_isis_fish", "vessels_12m_plus", "vessels_12m_plus_vms", "vessels_12m_less"
 sharing "data" and "shared_scripts" folders.
 Each sub-project need to be run sequentially in the following order :
 1."vessels_12m_plus" over period 2010-2018
 2."vessels_12m_plus_vms" over period 2010-2018
 3."vessels_12m_less" over period 2010-2020
 settings control variables in 0_control_file.R of each sub-project folder
 4."vessels_12m_plus" over period 2010-2022
 5."vessels_12m_less" over period 2010-2022
 updating  control variables in 0_control_file.R of each sub-project folder
 6. process_outputs_for_isis_fish
 
## Data requirement

- sacrois flux as Rdata files (one for each year i.e. `SACROISJour_Flux_2020_v3.3.12.RData`) are available at <https://sih.ifremer.fr/>
- Reference tables for vessels and species (<https://sih.ifremer.fr/>)
- ICES rectangles and areas for producing maps 

data should be organised in folder raw\ as follows :
-InterCatch_data
-ref_navs
-ref_spatial
-ref_species
-sacrois
-valpena
-vms_3_3
-vms_tacsat_eflalo
and the localisation of the raw floder shoud be defined in \shared_scripts\path_data\raw.r



## R packages requirement

All pakcages used for the analysis are avaiblable on CRAN, excepts for Ifremer COSTs packages. They can be installed manually from zip files available at  `\\nantes\simulateur\ActiviteDePeche\Analyses_generiques`. The analysis can be performed without COSTs packages installed.

## R packages management with renv

The `renv` package is a tool to manage R packages version. `renv` helps manage library paths to help isolate R project dependencies. Existing tools used for managing R packages (e.g. install.packages(), remove.packages()) should work as they did before.

If user encounters problem in installing or loading CRAN R packages, use the command `renv::restore()` (see help <https://rstudio.github.io/renv/articles/collaborating.html>). If the error persists, user can delete manually `renv` folder and `renv.lock`, but will loose the availability to work with the packages version used in this project.

## Fork the project

In order to use the proposed method and scripts, the user should fork the project. The forked project is a copy of the original repository by now owned by the user, the original repository is called the upstream. Forking a repository allows for file editing and versioning without interfering with the original repository. It also keeps the link with the original repository.

In order to fork a repository, the user can follow the steps proposed here : <https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html>

To set the original repository as the upstream repository, user can open a terminal in the forked repository and run:

1. `git remote add upstream https://gitlab.ifremer.fr/isis-fish-tools/data-processing.git` to set the remote
2. `git fetch upstream` retrieve the changes made in the original repository
3. `git merge upstream/master` merge the changes from the original repository to the forked repository

more help about fork and upstream can be found here : <https://www.atlassian.com/git/tutorials/git-forks-and-upstreams>

## How to smartly use the code 

### Test different approaches to set metiers (or strategies, fleets ...)

In order to compare and track changes, the user should create a fishery for each combination of state variables that he want to compare. For example, the user want to compare a clustering analysis for metier and the metier defined in sacrois date by the `METIER_COD_SACROIS` variable. It is recommended to create to fishery example sharing the same state variables excepts de metier state variable and compare the outputs of the two methods after.

### Where to save my custom codes

If generic R codes developed here to do not fulfill your needs, the best place to save your custom codes is in the fishery directory (`path_fishery` : i.e. `R/fishery_examples/bob_macco`). If your codes are generic and could interest other users do not hesitate to ask for a merge request (see help <https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork>).  

## Notes

- Fishing effort (df_eflalo$LE_EFF) is extracted as default from sacrois data as TP_NAVIRES_SACROIS: Temps de pêche consolidé (‘de référence’) de la séquence en heures décimales, issu du croisement entre temps de pêche géolocalisé et temps de pêche SACAPT, en privilégiant la source GEOLOC lorsque celle-ci est renseignée.
- Ifremer fleet segmentation is set to default. DCF fleet segmentation is not implemented yet.
- Duplicated rows in sacrois data are removed based on:
- ORIGINE_ESP_COD_FAO: SIPA_CAPTURES or SIPA_DEBARQUEMENTS are kept, OFIMER are discarded
