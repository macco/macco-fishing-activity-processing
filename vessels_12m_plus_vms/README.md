# Definition of metier zone using spatial clustering and vms data

The `main.R` file run the analysis.

1. In the `R/control.R` choose which species to work with and which years. 
  For example :

```  
starting_year <- 2010
ending_year <- 2019

###-----------------------------------------------------------------------------
### select species
key_species <- c("SOL", "HKE", "NEP",
                 "LDB", # Lepidorhombus boscii
                 "MEG", # Lepidorhombus Whiffiagonis
                 "MON", # Lophius piscatorius
                 "ANK", # Lophius budegassa
                 "MNZ", # Lophius spp
                 "WHG", # Merlangius merlangus
                 "RJC", # Raja calvata
                 "RJN", # Raja naevus
                 "CET"  # Dicologlossa cuneata
)
```

2. Source the `main.R` with R to obtain a list of results with the `R/process_data/clustering_with_geoclust_fn.R` script.

