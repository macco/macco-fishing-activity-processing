###-----------------------------------------------------------------------------
### sum effort data per month
dfs_vms_effort_month <- df_vms_effort %>%
  as_tibble() %>%
  dplyr::select(month, square, fishing_time) %>%
  group_by(across(where(is.factor))) %>%
  summarise(fishing_time = mean(fishing_time),
            fishing_time_log = log(fishing_time))
