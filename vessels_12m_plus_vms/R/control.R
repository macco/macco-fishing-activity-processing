################################################################################
### Put together variables that are used later in the code to perform option choice
################################################################################

################################################################################
### Session global variable
################################################################################
set.seed(1)
path_data_raw <- "/home/jblecomt/data_base"

###-----------------------------------------------------------------------------
### if true load raw data and process filtering
load_raw_data <- FALSE

###-----------------------------------------------------------------------------
### if true erase tidy data generated previously
erase_tidy_data <- FALSE

if (load_raw_data == TRUE) {
  erase_tidy_data <- TRUE
}

###-----------------------------------------------------------------------------
### if true erase results of the metier analysis (i.e. clustering)
erase_coda <- FALSE

###-----------------------------------------------------------------------------
isis_grid_type <- "species"

###-----------------------------------------------------------------------------
### if true erase tidy data generated previously
save_plot <- FALSE

###-----------------------------------------------------------------------------
### select species
key_species <- NULL

###-----------------------------------------------------------------------------
### select species
key_species <- c("SOL", "HKE", "NEP",
                 "LDB", # Lepidorhombus boscii
                 "MEG", # Lepidorhombus Whiffiagonis
                 "MON", # Lophius piscatorius
                 "ANK", # Lophius budegassa
                 "MNZ", # Lophius spp
                 "WHG", # Merlangius merlangus
                 "RJC", # Raja calvata
                 "RJN", # Raja naevus
                 "CET"  # Dicologlossa cuneata
)

if (is.null(key_species)) {
  key_species_name <- NULL
} else {
  key_species_name <- "macco_spp"
}


###-----------------------------------------------------------------------------
### select species
key_gear <- NULL

###-----------------------------------------------------------------------------
### select year from 2010 to 2019
starting_year <- 2010
ending_year <- 2018
year_vec <- starting_year:ending_year
n_year <- length(year_vec)

###-----------------------------------------------------------------------------
###
select_vessel_size <- "superior_to_12m"

