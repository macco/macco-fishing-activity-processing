
f_summarize_vms <- function(df_vms,
                            group_var_sum,
                            var_to_summarize,
                            group_var_mean) {
  
  ###-----------------------------------------------------------------------------
  ### sum catch data per month
  dfs_vms <- df_vms %>%
    as_tibble() %>%
    group_by(across(all_of(group_var_sum))) %>%
    summarise(across(all_of(var_to_summarize), sum)) %>%
    group_by(across(all_of(group_var_mean))) %>%
    summarise(across(where(is.numeric),
                     list(mean = mean,
                          sd = sd,
                          f_cv = f_cv,
                          sum = sum))) %>%
    mutate_if(is.numeric, list(log = log))
  
  return(dfs_vms)
  
}
