f_plot_effort_patchwork <- function(fleet_2_plot,
                                    list_spatial_clust = NULL,
                                    list_map_isis_month_effort = NULL,
                                    list_map_isis_effort = NULL,
                                    inset = FALSE,
                                    as_list = TRUE) {
  if (as_list == TRUE) {
    list_plot <- list()  
  } else {
    list_plot <- NULL
  }
  
  
  
  ###---------------------------------------------------------------------------
  ### spatial clust plots
  if (!is.null(list_spatial_clust)) {
    if (!is.null( list_spatial_clust[[fleet_2_plot]]$plot_cluster)) {
      
      plot_cluster <- list_spatial_clust[[fleet_2_plot]]$plot_cluster   + 
        theme(legend.position = "none")
      
      plot_map_cluster <- list_spatial_clust[[fleet_2_plot]]$map_cluster  + 
        labs(x = NULL, y = NULL) + 
        theme(legend.position = "none")
    } else {
      plot_map_cluster <- NULL
      plot_cluster <- NULL
    }
  } else {
    plot_map_cluster <- NULL
    plot_cluster <- NULL
  }
  
  ###---------------------------------------------------------------------------
  ### maps of effort per month
  if (!is.null(list_map_isis_month_effort)) {
    
    plot_month <- list_map_isis_month_effort[[fleet_2_plot]] +
      # theme(plot.margin = unit(c(0, 2, 0, 0),"mm")) + 
      labs(x = NULL, y = NULL) + 
      ggtitle("")
  } else {
    plot_month <- NULL
  }
  
  ###---------------------------------------------------------------------------
  ### maps of effort average
  if (!is.null(list_map_isis_effort)) {
    plot_average <- list_map_isis_effort[[fleet_2_plot]] +
      ggtitle("Average") + 
      labs(x = NULL, y = NULL)
  } else {
    plot_average <- NULL
  }

  # if (!is.null(list_map_isis_effort) & !is.null(list_map_isis_month_effort)) {
  #   my_viridis_scale <- scale_fill_viridis_c(name = "Effort (in hours)")
  # 
  #   # Now edit the plots in place
  #   set_scale_union(plot_month,
  #                   plot_average,
  #                   scale = my_viridis_scale)
  # }
  
  ###---------------------------------------------------------------------------
  ### 
  if (!is.null(list_map_isis_effort) & !is.null(list_map_isis_month_effort) & 
      inset == FALSE) {
    
    layout <- c(patchwork::area(t = 2, l = 1, b = 5, r = 4),
                patchwork::area(t = 2, l = 5, b = 5, r = 7))
    
    patch_month_average <- plot_month + plot_average + 
      plot_annotation(title = fleet_2_plot) + 
      plot_layout(design = layout) + 
      plot_layout(guides = 'collect')  &
      theme(legend.position = 'bottom')
    
    list_plot$patch_month_average <- patch_month_average
    
  } else {
    patch_month_average <- NULL
  }
  
  ###---------------------------------------------------------------------------
  ### 
  if (!is.null(list_map_isis_effort) & !is.null(list_map_isis_month_effort) & 
      inset == TRUE) {
    
    patch_month_average_col <- (plot_month + theme(
      plot.margin = unit(c(1,5,1,1), 'cm'))) + 
      inset_element((plot_average +
                       theme(plot.title = element_text(hjust = 1))),
                    left = 0.8,
                    bottom = 0.03,
                    right = 1,
                    top = 0.5, 
                    on_top = FALSE,
                    align_to = 'full') + 
      plot_annotation(title = fleet_2_plot) + 
      # plot_layout(design = layout) + 
      plot_layout(guides = 'collect')  &
      theme(legend.position = 'bottom')
    
    
    if (as_list == TRUE) {
      list_plot$patch_month_average_col <- patch_month_average_col
    } else {
      list_plot <- patch_month_average_col
    }
    
  } else {
    patch_month_average_col <- NULL
  }
  
  
  
  ###---------------------------------------------------------------------------
  ### 
  if (!is.null(plot_month) & !is.null(plot_average) &
      !is.null(plot_cluster)) {
    
    patch_full <- ((plot_month + theme(legend.position = "none")) |
                     plot_average + 
                     plot_layout(guides = 'collect')  &
                     theme(legend.position = 'bottom')) /
      (plot_cluster | plot_map_cluster) + 
      plot_annotation(title = fleet_2_plot)
    
    if (as_list == TRUE) {
    list_plot$patch_full <- patch_full
    } else {
      list_plot <- patch_full
    }
    
  } else{
    patch_full <- NULL
  }
  
  ###---------------------------------------------------------------------------
  ### 
  if (!is.null(plot_cluster) &  !is.null(plot_map_cluster)) {
    
    patch_full <- (plot_cluster | plot_map_cluster) + 
      plot_annotation(title = fleet_2_plot)
    
    if (as_list == TRUE) {
      list_plot$patch_full <- patch_full
    } else {
      list_plot <- patch_full
    }
    
  } else{
    patch_full <- NULL
  }
  
  return(list_plot)
}
