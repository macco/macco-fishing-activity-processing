f_map_var_month_species <- function(df_data,
                            species_fao_cod,
                            var_to_fill,
                            var_to_facet,
                            month_to_plot = NULL,
                            title_label = NULL){
  ### filter species
  df_data <- df_data %>%
    filter(espece_fao_cod  == species_fao_cod)
  
  ### filter month
  if (!is.null(month_to_plot)) {
    df_data <- df_data %>%
      filter(month  == month_to_plot)
  }
  
  plot_empty_map +
    geom_sf(data = df_data, aes(fill = .data[[var_to_fill]]), size = 0.001) +
    scale_fill_viridis_c(name = "") +
    facet_wrap( ~ .data[[var_to_facet]], ncol = 3) +
    ggtitle(paste(title_label,
                   unique(df_data$espcece_fao_lib),
                   "in month",
                   unique(df_data$month), sep = " "))
}