################################################################################
### Plot a map of vms effort data per month and species
################################################################################

if (file.exists(here(path_data_tidy_plots, "list_map_vms_month_species_gear.rds"))) {
  
  list_map_vms_month_species_gear <- read_rds(here(path_data_tidy_plots,
                                              "list_map_vms_month_species_gear.rds"))
  
} else {
  var_to_fill_name <- "fishing_time_mean"
  var_to_group <- c("month", "espece_fao_cod", "engin_fao_cod")
  
  list_map_vms_month_species_gear  <- df_vms_month_gear %>%
    group_by(across(all_of(var_to_group))) %>%
    group_split() %>%
    map(~ f_map_var(df_data = .x,
                    var_to_fill = "fishing_time_mean",
                    # var_to_facet = "month",
                    title_label = paste0("Fishing time ",
                                         .x$engin_fao_cod, 
                                         " in hours")))
  
  names(list_map_vms_month_species_gear) <- df_vms_month_gear %>%
    group_by(across(all_of(var_to_group))) %>%
    group_split() %>%
    map(~ f_map_var_name(df_data = .x, var_to_fill = var_to_fill_name))
  
  
  ### save complete list as rds file
  saveRDS(list_map_vms_month_species_gear,
          file = here(path_data_tidy_plots, "list_map_vms_month_species_gear.rds"))
}

### save figure
walk2(.x = names(list_map_vms_month_species_gear),
      .y = list_map_vms_month_species_gear,
      ~{
        f_save_plot(plot = .y,
                    file_name = .x,
                    path = path_figure,
                    save = FALSE,
                    width = 21,
                    height = 29,
                    device = 'pdf',
                    units = 'cm')})
