############################################################################### #
### Load reference harbour
################################################################################

if (file.exists(here(path_data_tidy_vms, "df_ref_harbour.rds"))){
  df_ref_harbour <- read_rds(file = here(path_data_tidy_vms, "df_ref_harbour.rds"))

} else{
df_ref_harbour <- read_csv2(here(path_data_raw,
                                 "ref_port",
                                 "terrestrialLocations_2020-07-20_07-04.csv"),
                              ) %>%
  # filter(PAYS_LIB == "France") %>%
  select(PORT_COD, PORT_LIB, DPT_INSEE_LIB, FACADE_MARITIME_LIB) %>%
  mutate(across(where(is.character), ~ str_replace_all(., pattern = "é", replacement = "e")),
         across(where(is.character), ~ str_replace_all(., pattern = "è", replacement = "e")),
         across(where(is.character), ~ str_replace_all(., pattern = "ô", replacement = "o")),
         fleet = case_when(DPT_INSEE_LIB %in% c("Morbihan", "Finistere", "Finistère", "Loire-Atlantique", "Ille-et-Vilaine") ~ "bob_north",
                           DPT_INSEE_LIB %in% c("Charente-Maritime", "Gironde", "Landes",  "Vendee", "Vendée") ~ "bob_south",
                           DPT_INSEE_LIB %in% c("Manche", "Pas-de-Calais", "Cotes-d'Armor",  "Nord", "Calvados", "Seine-Maritime") ~ "channel",
                           DPT_INSEE_LIB %in% c("Herault", "Bouches-du-Rhone", "Alpes-Maritimes",  "Var", "Corse-du-Sud", "Haute-Corse", "Pyrenees-Orientales") ~ "medit")) %>%
  distinct()

saveRDS(df_ref_harbour, file = here(path_data_tidy_vms,
                                    "df_ref_harbour.rds"))
}  




# df_SUPER_QAM <- data.frame(
#   SUPER_QAM_LIB_id = c(3, 3, 1, 2, 3, 2, 2, 2, 1, 1, 3, 2, 2, 3, 2, 3, 3, 3,
#                        1, 1, 1, 1, 1, 1, 2, 2, 2, 1, 1, 2, 2, 4, 4, 4, 4,1,4),
#   QAM_LIB = c("Bayonne", "Marennes", "Paimpol", "Guilvinec", "La Rochelle",
#               "Vannes", "Audierne", "Auray", "Saint-Brieuc", "Cherbourg",
#               "Ile d'Oleron", "Lorient", "Concarneau", "L'Ile-d'Yeu",
#               "Saint-Nazaire", "Bordeaux", "Les Sables-d'Olonne", "Arcachon",
#               "Morlaix", "Dieppe", "Saint-Malo", "Caen", "Boulogne-sur-mer",
#               "Fecamp", "Brest", "Camaret", "Douarnenez", "Dunkerque",
#               "Le Havre", "Nantes", "Noirmoutier",
#               "Sete", "Port-Vendres", "Martigues", "Marseille", "Boulogne-sur-Mer", "Port-Vendres")) %>%
#   mutate(SUPER_QAM_LIB = factor(SUPER_QAM_LIB_id,
#                                 labels = c("channel",
#                                            "bob_north",
#                                            "bob_south",
#                                            "medit")))
# 
# df_ref_harbour <- left_join(df_ref_harbour, df_SUPER_QAM,
#                             by = c("PORT_LIB" = "QAM_LIB"))
