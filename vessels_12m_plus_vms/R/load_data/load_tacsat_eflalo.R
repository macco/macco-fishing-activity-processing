################################################################################
### load tac-sat eflalo data from Baptiste
################################################################################

###-------------------------------------------------------------------------
### extract key and other species
source("R/load_data/set_species_key_and_other.R")

###-----------------------------------------------------------------------------
### load catch data
dir.create(here(path_data_tidy_vms, "tacsat"),
           showWarnings = FALSE)

if (file.exists(here(path_data_tidy_vms, "tacsat",
                     paste0("df_eflalo_effort_",
                            key_species_name,
                            starting_year, "_",
                            ending_year,
                            "isis_grid_",
                            isis_grid_type,
                            ".rds"))) & 
    load_raw_data == FALSE) {
  
  df_vms_effort <- read_rds(here(path_data_tidy_vms, "tacsat",
                                 paste0("df_eflalo_effort_",
                                        key_species_name,
                                        starting_year, "_",
                                        ending_year,
                                        "isis_grid_",
                                        isis_grid_type,
                                        ".rds")))
  
} else {
  
  df_vms_effort <- list.files(path = here(path_data_raw, "vms_tacsat_eflalo"),
                              full.names = TRUE, 
                              pattern = paste(year_vec, collapse = "|")
                              # pattern = c("2010")
                              ) %>%
    map_df(.x = ., ~ f_load_tacsat_eflalo(file = .x,
                                          filter_by_fishing_State = TRUE,
                                          filter_dcf6 = TRUE,
                                          filter_vessel_sup12m = TRUE,
                                          key_species = NULL, 
                                          spatial_grid = isis_grid_sf,
                                          summarize_effort = TRUE,
                                          grouping_var = c("square_isis",
                                                           "LE_MET_level6",
                                                           "FT_LHAR")))
  
  saveRDS(df_vms_effort, file = here(path_data_tidy_vms, "tacsat",
                                     paste0("df_eflalo_effort_",
                                            key_species_name,
                                            starting_year, "_",
                                            ending_year,
                                            "isis_grid_",
                                            isis_grid_type,
                                            ".rds")))
}
