---
title: "Untitled"
author: "Jean-Baptiste Lecomte"
date: '2022-09-09'
output: pdf_document
classoption: landscape
---

```{r setup, include = FALSE}
#knitr option
knitr::opts_chunk$set(
  include = TRUE,        
  echo = FALSE,
  cache = FALSE,
  warning = FALSE,
  message = FALSE,
  progress = FALSE,
  verbose = FALSE,
  # dev = 'png',
  fig.height = 20,
  fig.width = 20,
  # dpi = 300,
  # out.width = 700,
  # out.height = 550,
  fig.retina = 1,
  autodep = FALSE)

options(scipen = 999)
```

## R Markdown


```{r effort-plot4, dpi = 600}
for (p in names(list_pathwork_plots)) {
  # plot.new()
  print(list_effort_pathwork_plots[[p]]$patch_month_average_col)
}
```


