################################################################################
### Preparing the dataframes for the Atlas
################################################################################

cat(bgBlue("4: preparing the dataframes for the Atlas \n"))#
dir.create(glue("{path_fishery_data_tidy}/atlas"),
           showWarnings = FALSE, recursive = TRUE)


############ PARTIE ESPECES

#### 1) debarquements par esp/metier_isis/statut - cumul trimestre par an - moyenne annee (2015-2022)
### dans etape 7 : R/estim_fishing_activity_parameters/3_fit_model_WithIntercatch.R - landings per quarter per metier per species
df_sacrois_key_species_ag_foreign_final <- readRDS(
  file = glue("{path_sacrois_data_tidy}/df_sacrois_key_species_ag_foreign_final_{year_range[1]}_{year_range[2]}.rds")) %>%
  filter(REF_YEAR >=2015)



####table metier_isis - esp_capturables- landingsYearMean

##########Preparation des tables pour la synthèse
####table metier_isis - esp_capturables- landingsYearMean

TableEspCaptMetierISIS <- df_sacrois_key_species_ag_foreign_final %>%
  filter(REF_YEAR >=2015) %>%
  group_by(REF_YEAR, metier_isis, ESP_COD_FAO) %>%
  mutate(landingsYear = sum(landings)) %>%
  dplyr::select(REF_YEAR, metier_isis, ESP_COD_FAO, landingsYear) %>%
  distinct() %>%
  group_by(metier_isis, ESP_COD_FAO) %>%
  mutate(landingsYearMean = mean(landingsYear)) %>%
  dplyr::select(metier_isis, ESP_COD_FAO, landingsYearMean) %>%
  distinct() %>%
  ungroup()

saveRDS(TableEspCaptMetierISIS,
        glue("{path_fishery_data_tidy}/atlas/df_TableEspCaptMetierISIS.rds"))

#### table unitaire metier_isis/DCF6/DCF5
# identifier le metier DCF6 qui capture le plus dans metier_isis
### agregate sacrois by year

df_sacrois_key_species <- read_rds(glue("{path_sacrois_data_tidy}/df_sacrois_key_species_{year_range[1]}_{year_range[2]}.rds"))


df_sacrois_key_species_agYear_DCF6 <- df_sacrois_key_species %>%
  filter(METIER_DCF_6_COD != "Missing") %>%
  group_by(REF_YEAR, metier_isis, METIER_DCF_6_COD) %>%
  mutate(LE_EFF_sum = sum(LE_EFF),
         NB_LE = n()) %>%
  dplyr::select(REF_YEAR, metier_isis, LE_EFF_sum, NB_LE, METIER_DCF_6_COD) %>%
  ungroup() %>%
  distinct() %>%
  group_by(metier_isis, METIER_DCF_6_COD) %>%
  mutate(LE_EFF_sum_mean = mean(LE_EFF_sum),
         NB_LE_mean = mean(NB_LE)) %>%
  dplyr::select(metier_isis, LE_EFF_sum_mean, NB_LE_mean, METIER_DCF_6_COD) %>%
  ungroup() %>%
  distinct() %>%
  mutate(part1 = str_split_fixed(METIER_DCF_6_COD, "_", 2)[,1],
         part2 = str_split_fixed(METIER_DCF_6_COD, "_", 4)[,3],
         part3 = str_split_fixed(METIER_DCF_6_COD, "_", 3)[,3],
         part2fin = if_else(str_split_fixed(part2, "=", 2)[,1] == ">", part2,
                            paste(str_split_fixed(METIER_DCF_6_COD, "_", 5)[, 3],
                                  str_split_fixed(METIER_DCF_6_COD, "_", 5)[, 4], sep = "-"))) %>%
  unite("Metier_tache1", c(part1, part2fin), sep ="_", remove = FALSE) %>%
  dplyr::select(!contains("part"))

#source("R/export_isis/metier6_to_isis_flow.r")
#graphe des liens non uniques
# corresp_tab <- df_sacrois_key_species_agYear_DCF6 %>%
#   dplyr::select(metier_isis, METIER_DCF_6_COD) %>%
#   distinct() %>% filter(!is.na(metier_isis))
# metier6_to_isis_flow(corresp_tab[1:100,])
#metier5_to_isis_flow(corresp_tab5)
###-----------------------------------------------------------------------------
df_Corresp_Metier_isis_Metier_DCF6_max_effort <- df_sacrois_key_species_agYear_DCF6 %>%
  group_by(metier_isis) %>%
  slice_max(LE_EFF_sum_mean)

saveRDS(df_Corresp_Metier_isis_Metier_DCF6_max_effort,
        glue("{path_fishery_data_tidy}/atlas/Table_Corresp_Metier_isis_MetierDCF6_max_effort.rds"))

###-----------------------------------------------------------------------------
df_Corresp_Metier_isis_Metier_DCF6_max_seq <- df_sacrois_key_species_agYear_DCF6 %>%
  group_by(metier_isis) %>%
  slice_max(NB_LE_mean)


saveRDS(df_Corresp_Metier_isis_Metier_DCF6_max_seq,
        glue("{path_fishery_data_tidy}/atlas/Table_Corresp_Metier_isis_MetierDCF6_max_seq.rds"))

###-----------------------------------------------------------------------------
df_sacrois_key_species_agYear_DCF5 <- df_sacrois_key_species %>%
  filter(METIER_DCF_5_COD!= "") %>%
  group_by(REF_YEAR,metier_isis,METIER_DCF_5_COD) %>%
  mutate(LE_EFF_sum = sum(LE_EFF), NB_LE = n()) %>%
  dplyr::select(REF_YEAR,metier_isis,LE_EFF_sum,NB_LE,METIER_DCF_5_COD) %>% ungroup() %>% distinct()  %>%
  group_by(metier_isis,METIER_DCF_5_COD) %>%
  mutate(LE_EFF_sum_mean = mean(LE_EFF_sum), NB_LE_mean = mean(NB_LE)) %>%
  dplyr::select(metier_isis,LE_EFF_sum_mean,NB_LE_mean,METIER_DCF_5_COD) %>% ungroup() %>% distinct()

###-----------------------------------------------------------------------------
df_Corresp_Metier_isis_Metier_DCF5_max_effort <- df_sacrois_key_species_agYear_DCF5%>%
  group_by(metier_isis) %>% slice_max(LE_EFF_sum_mean)

saveRDS(df_Corresp_Metier_isis_Metier_DCF5_max_effort,
        glue("{path_fishery_data_tidy}/atlas/Table_Corresp_Metier_isis_Metier_DCF5_max_effort.rds"))

###-----------------------------------------------------------------------------
df_Corresp_Metier_isis_Metier_DCF5_max_seq <- df_sacrois_key_species_agYear_DCF5%>%
  group_by(metier_isis) %>% slice_max(NB_LE_mean)

saveRDS(df_Corresp_Metier_isis_Metier_DCF5_max_seq,
        glue("{path_fishery_data_tidy}/atlas/Table_Corresp_Metier_isis_Metier_DCF5_max_seq.rds"))


###-----------------------------------------------------------------------------
exportTableFromShinyNov2023 <- read_rds(paste0("C:/Users/smahevas/C-GIT/Git_ISIS-Fish/0_MACCO/macco-fishing-activity-processing/data/raw/bob_macco/ShinyTache1/","exportTableFromShinyNov2023.rds"))

TableEspStatusGearYear <- exportTableFromShinyNov2023 %>%
  dplyr::select(FAO,Status,gear,year) %>%
  distinct() %>%
  left_join(., df_Corresp_Metier_isis_Metier_DCF6_max_seq,
            by = join_by(gear == MetierDCF6_red)) %>%
  dplyr::select(metier_isis, METIER_DCF_6_COD, FAO, Status, year)

saveRDS(TableEspStatusGearYear,
        glue("{path_fishery_data_tidy}/atlas/TableEspStatusGearYear.rds"))


df_Key_Esp_Status_Gear_Year <- TableEspStatusGearYear %>%
  filter(FAO%in%key_species) %>%
  distinct() %>%
  left_join(.,
            df_Corresp_Metier_isis_Metier_DCF6_max_seq,
            by = join_by(gear == MetierDCF6_red)) %>%
  dplyr::select(metier_isis, METIER_DCF_6_COD, FAO, Status,year)

saveRDS(df_Key_Esp_Status_Gear_Year,
        glue("{path_fishery_data_tidy}/atlas/TableKeyEspStatusGearYear.rds"))

ggplot(data = df_Key_Esp_Status_Gear_Year %>% filter(year == 2015)) +
  geom_tile(
    aes(x = metier_isis,
        y = FAO,
        fill = status))



### 2) esp/metier_isis (qui capture l'esp)/zone_pop/zone_metier/zone intersection : visualisation spatiale des recouvrements
### dans etape 4 : df zone_metier_isis
dfs_eflalo_metier_isis_zone2010_2022 <- readRDS("C:/Users/smahevas/C-GIT/Git_ISIS-Fish/0_MACCO/macco-fishing-activity-processing/data/tidy/bob_macco/eflalo/dfs_eflalo_metier_isis_zone2010_2022.rds")


### 3) esp/metier_isis (qui capture l'esp)/zone_pop/zone_metier/zone intersection/Prop_Effort_entre_zone_metiers : visualisation spatiale de le distribution des debar par metier


### dans etape 6 :  Compute effort for ISIS-Fish - load ratio of effort for metier with 2 zones computed from vms data
dfs_ratio_effort_metier_zone_high_low <- readr::read_rds(here(path_fishery_data_tidy,
                                                              "vms_processed",
                                                              "dfs_ratio_effort_metier_zone_high_low.rds"))






#### PARTIE METIERS

### PARTIE 1
##1) Metiers ss other: df_sacrois_key_species_ag_foreign_final - 2015-2022 - somme sur les trimestres/ an  - VOIR SI ON MET LES MOYENNES OU SI C'EST JEROME QUI LES CALCULE POUR AVOIR AUSSI L'EVOLUTION

TableEspCaptMetierISIS_YEAR_SSOTHER_20152022 <-df_sacrois_key_species_ag_foreign_final %>%
  filter(REF_YEAR >=2015) %>%
  group_by(REF_YEAR, metier_isis, ESP_COD_FAO) %>%
  mutate(landingsYear = sum(landings)) %>%
  dplyr::select(REF_YEAR, metier_isis, ESP_COD_FAO, landingsYear) %>%
  distinct() %>% #supression des repetitions trimestre


  TableEspCaptMetierISIS_YEAR_SSOTHER_20152022_MEAN <- TableEspCaptMetierISIS_YEAR_SSOTHER_20152022 %>%
  group_by(metier_isis, ESP_COD_FAO) %>%
  mutate(landingsYearMean = mean(landingsYear)) %>%
  dplyr::select(metier_isis, ESP_COD_FAO, landingsYearMean) %>%
  distinct() %>%
  ungroup()


## 2) Metiers ss other: TableEspCaptMetierISIS_YEAR_SSOTHER_20152022 - proportion par metier sur les moyennes
TableEspCaptMetierISIS_YEAR_SSOTHER_20152022_Prop <- TableEspCaptMetierISIS_YEAR_SSOTHER_20152022_MEAN %>%
  group_by(metier_isis, landingsYearMean) %>%
  mutate(landingsYearMeanTot = sum(landingsYearMean),
         landingsYearMeanProp = landingsYearMeanTot/landingsYearMean) %>%
  dplyr::select(metier_isis, ESP_COD_FAO, landingsYearMeanProp) %>%
  distinct()

## 3) Metiers avec other: df_sacrois_ ?? - 2015-2022 - somme sur les trimestres/ an puis moyenne

TableEspCaptMetierISIS_YEAR_AvecOTHER_20152022_Prop <-df_sacrois_?? %>%
  filter(REF_YEAR >=2015) %>%
  group_by(REF_YEAR, metier_isis, ESP_COD_FAO) %>%
  mutate(landingsYear = sum(landings)) %>%
  dplyr::select(REF_YEAR, metier_isis, ESP_COD_FAO, landingsYear) %>%
  distinct() %>% #supression des repetitions trimestre
  group_by(metier_isis, ESP_COD_FAO) %>%
  mutate(landingsYearMean = mean(landingsYear)) %>%
  dplyr::select(metier_isis, ESP_COD_FAO, landingsYearMean) %>%
  distinct() %>%
  ungroup()  %>%
  group_by(metier_isis, landingsYearMean) %>%
  mutate(landingsYearMeanTot = sum(landingsYearMean),
         landingsYearMeanProp = landingsYearMeanTot/landingsYearMean) %>%
  dplyr::select(metier_isis, ESP_COD_FAO, landingsYearMeanProp) %>%
  distinct()


###PARTIE 2
## Tables zones Metier selon la flottille pour les moins de 15 m et 15-inf pour les plus de 15m : fleet_isis,fleet_agisis, strategie_isis, zone_metier_isis?? (Z_unique, Z_low, Z_high)





#### PARTIE FLOTTILLES
### PARTIE 1
## 1) Flottille ISIS : fleet_isis, strategie_isis, year, nb bateaux
#  pour faire un graphique des nombres de bateaux par flottille isis par an entre 2015-2022 (somme sur les strategies) et avoir la liste des strategies



### PARTIE 2
## 1) Strategie ISIS : strategie_isis, year, month, prop_met, prop_met_mean (histogramme par mois)

## 2) Strategie ISIS : strategie_isis, year, nb_bateaux, inactivite  effort (?) - A DISCUTER

