###-----------------------------------------------------------------------------
### Presentation commune à tous les metiers tables
###-----------------------------------------------------------------------------

tbl_metier_isis_param <- data.frame(thema = "metier_isis",
                                    year_start = 2010,
                                    year_end = 2022,
                                    month = -1,
                                    stade = -1,
                                    data_type = "catch",
                                    rectangle = -1)

###-----------------------------------------------------------------------------
### Profil des débarquements/esp macco pour chaque metier

df_metier_isis_key_species_prop_landings_year <- readRDS(
  glue("{path_fishery_data_tidy}/atlas/df_metier_isis_key_species_prop_landings_year.rds"))

map_number <- map_number_init
map_number <- 70

description_metier_isis_key_species_prop_landings_year <- data.frame(thema = tbl_metier_isis_param$thema,
                                                                     map_number = map_number,
                                                                     description = "Table profil des débarquements par espèce macco pour chaque metier")
if(export_to_db) {
  dbWriteTable(macco.db,
               "description_cartes",
               description_metier_isis_key_species_prop_landings_year,
               append = TRUE,
               row.names = FALSE)
}

tbl_metier_isis_key_species_prop_landings_year <- df_metier_isis_key_species_prop_landings_year %>%
  dplyr::rename(values = landingsYearMean_prop,
                metier = metier_isis,
                species = ESP_COD_FAO) %>%
  dplyr::mutate(thema = tbl_metier_isis_param$thema,
                map_number = map_number,
                year_start = tbl_metier_isis_param$year_start,
                year_end = tbl_metier_isis_param$year_end,
                month = tbl_metier_isis_param$month,
                stade = tbl_metier_isis_param$stade,
                data_type = tbl_metier_isis_param$data_type,
                rectangle = tbl_metier_isis_param$rectangle,
                date_insert = date()) %>%
  dplyr::select(thema, map_number, year_start, year_end, month, metier,
                species, stade, data_type, values, rectangle)

if(export_to_db) {
  ### db_write si table et non un objet sf
  dbWriteTable(macco.db,
               'cartes',
               tbl_metier_isis_key_species_prop_landings_year,
               append = TRUE,
               row.names = FALSE)
}

###-----------------------------------------------------------------------------
### Profil des débarquements/esp macco + oth pour chaque metier

df_metier_isis_all_species_prop_landings_year <- readRDS(
  glue("{path_fishery_data_tidy}/atlas/df_metier_isis_all_species_prop_landings_year.rds"))

map_number <- map_number + 1
map_number <- 70 + 1

description_metier_isis_all_species_prop_landings_year <- data.frame(thema = tbl_metier_isis_param$thema,
                                                                     map_number = map_number,
                                                                     description = "Table profil des débarquements par espèce macco et autre pour chaque metier")
if(export_to_db) {
  dbWriteTable(macco.db,
               "description_cartes",
               description_metier_isis_all_species_prop_landings_year,
               append = TRUE,
               row.names = FALSE)
}

tbl_metier_isis_all_species_prop_landings_year <- df_metier_isis_all_species_prop_landings_year %>%
  dplyr::rename(values = landingsYearMean_prop,
                metier = metier_isis,
                species = ESP_COD_FAO) %>%
  dplyr::mutate(thema = tbl_metier_isis_param$thema,
                map_number = map_number,
                year_start = tbl_metier_isis_param$year_start,
                year_end = tbl_metier_isis_param$year_end,
                month = tbl_metier_isis_param$month,
                stade = tbl_metier_isis_param$stade,
                data_type = tbl_metier_isis_param$data_type,
                rectangle = tbl_metier_isis_param$rectangle,
                date_insert = date()) %>%
  dplyr::select(thema, map_number, year_start, year_end, month, metier,
                species, stade, data_type, values, rectangle)

if(export_to_db) {
  ### db_write si table et non un objet sf
  dbWriteTable(macco.db,
               'cartes',
               tbl_metier_isis_all_species_prop_landings_year,
               append = TRUE,
               row.names = FALSE)
}

###-----------------------------------------------------------------------------
df_metier_isis_key_species_avg_landings_year <- readRDS(
  glue("{path_fishery_data_tidy}/atlas/df_metier_isis_key_species_avg_landings_year.rds"))

map_number <- map_number + 1
map_number <- 72


description_metier_isis_all_species_prop_landings_year <- data.frame(thema = tbl_metier_isis_param$thema,
                                                                     map_number = map_number,
                                                                     description = "Débarquements moyen annuel par espèce macco pour chaque metier")
if(export_to_db) {
  dbWriteTable(macco.db,
               "description_cartes",
               description_metier_isis_all_species_prop_landings_year,
               append = TRUE,
               row.names = FALSE)
}

tbl_metier_isis_key_species_avg_landings_year <- df_metier_isis_key_species_avg_landings_year %>%
  dplyr::rename(values = landingsYearMean,
                metier = metier_isis,
                species = ESP_COD_FAO) %>%
  dplyr::mutate(thema = tbl_metier_isis_param$thema,
                map_number = map_number,
                year_start = tbl_metier_isis_param$year_start,
                year_end = tbl_metier_isis_param$year_end,
                month = tbl_metier_isis_param$month,
                stade = tbl_metier_isis_param$stade,
                data_type = tbl_metier_isis_param$data_type,
                rectangle = tbl_metier_isis_param$rectangle,
                date_insert = date())  %>%
  dplyr::select(thema, map_number, year_start, year_end, month, metier,
                species, stade, data_type, values, rectangle)
if(export_to_db) {
  ### db_write si table et non un objet sf
  dbWriteTable(macco.db,
               'cartes',
               tbl_metier_isis_key_species_avg_landings_year,
               append = TRUE,
               row.names = FALSE)
}

###-----------------------------------------------------------------------------
### Presentation commune à tous les strategies tables
###-----------------------------------------------------------------------------
tbl_stategy_isis_param <- data.frame(thema = "strategy_isis",
                                     year_start = 2010,
                                     year_end = 2022,
                                     month = -1,
                                     stade = -1,
                                     species = -1,
                                     metier = "fleet_isis", ### a checker sur les autres tables
                                     data_type = "vessels",
                                     rectangle = -1)

###-----------------------------------------------------------------------------
dfs_navs_per_strategy_input_avg <- readRDS(glue("{path_fishery_data_tidy}/atlas/dfs_navs_per_strategy_input_avg.rds"))

map_number <- map_number + 1
map_number <- 73

description_navs_per_strategy_input_avg <- data.frame(thema = tbl_stategy_isis_param$thema,
                                                      map_number = map_number,
                                                      description = "Nombre de navires par stratégie et flottilles (colonnes species et metier)")
if(export_to_db) {
  dbWriteTable(macco.db,
               "description_cartes",
               description_navs_per_strategy_input_avg,
               append = TRUE,
               row.names = FALSE)
}

tbl_navs_per_strategy_input_avg <- dfs_navs_per_strategy_input_avg %>%
  dplyr::rename(values = n_vessel,
                metier = fleet_isis,
                species = strategy_isis) %>%
  dplyr::mutate(thema = tbl_stategy_isis_param$thema,
                map_number = map_number,
                year_start = tbl_stategy_isis_param$year_start,
                year_end = tbl_stategy_isis_param$year_end,
                month = tbl_stategy_isis_param$month,
                stade = tbl_stategy_isis_param$stade,
                data_type = tbl_stategy_isis_param$data_type,
                rectangle = tbl_stategy_isis_param$rectangle,
                date_insert = date())  %>%
  dplyr::select(thema, map_number, year_start, year_end, month, metier,
                species, stade, data_type, values, rectangle)

if(export_to_db) {
  ### db_write si table et non un objet sf
  dbWriteTable(macco.db,
               'cartes',
               tbl_navs_per_strategy_input_avg,
               append = TRUE,
               row.names = FALSE)

}

###-----------------------------------------------------------------------------
df_fleet_strategy_isis <- readRDS(glue("{path_fishery_data_tidy}/atlas/df_fleet_strategy_isis.rds"))

map_number <- map_number + 1
map_number <- 74

description_fleet_strategy_isis <- data.frame(thema = tbl_stategy_isis_param$thema,
                                                      map_number = map_number,
                                                      description = "Table correspondance flottilles-strategies (colonnes metier-species)")
if(export_to_db) {
  dbWriteTable(macco.db,
               "description_cartes",
               description_fleet_strategy_isis,
               append = TRUE,
               row.names = FALSE)
}

tbl_navs_fleet_strategy_isis <- df_fleet_strategy_isis %>%
  dplyr::rename(metier = fleet_isis,
                species = strategy_isis) %>%
  dplyr::mutate(thema = tbl_stategy_isis_param$thema,
                map_number = map_number,
                values = 1,
                year_start = tbl_stategy_isis_param$year_start,
                year_end = tbl_stategy_isis_param$year_end,
                month = tbl_stategy_isis_param$month,
                stade = tbl_stategy_isis_param$stade,
                data_type = tbl_stategy_isis_param$data_type,
                rectangle = tbl_stategy_isis_param$rectangle,
                date_insert = date())  %>%
  dplyr::select(thema, map_number, year_start, year_end, month, metier,
                species, stade, data_type, values, rectangle)

if(export_to_db) {
  ### db_write si table et non un objet sf
  dbWriteTable(macco.db,
               'cartes',
               tbl_navs_fleet_strategy_isis,
               append = TRUE,
               row.names = FALSE)

}

###-----------------------------------------------------------------------------

dfs_prop_metier_zone_isis_strategy <- readRDS(glue("{path_fishery_data_tidy}/atlas/dfs_prop_metier_zone_isis_strategy.rds"))

map_number <- map_number + 1
map_number <- 75

description_prop_metier_zone_isis_strategy <- data.frame(thema = tbl_stategy_isis_param$thema,
                                              map_number = map_number,
                                              description = "Proportion de chaque métier par mois par strategie (colonne species = strategies)")
if(export_to_db) {
  dbWriteTable(macco.db,
               "description_cartes",
               description_prop_metier_zone_isis_strategy,
               append = TRUE,
               row.names = FALSE)
}

tbl_prop_metier_zone_isis_strategy <- dfs_prop_metier_zone_isis_strategy %>%
  dplyr::rename(values = prop_effort_metier_isis_avg,
                month = MONTH,
                metier = metier_zone_fleet_isis_import,
                species = strategy_isis ) %>%
  dplyr::mutate(thema = tbl_stategy_isis_param$thema,
                map_number = map_number,
                year_start = tbl_stategy_isis_param$year_start,
                year_end = tbl_stategy_isis_param$year_end,
                stade = tbl_stategy_isis_param$stade,
                data_type = tbl_stategy_isis_param$data_type,
                rectangle = tbl_stategy_isis_param$rectangle,
                date_insert = date())  %>%
  dplyr::select(thema, map_number, year_start, year_end, month, metier,
                species, stade, data_type, values, rectangle)

if(export_to_db) {
  ### db_write si table et non un objet sf
  dbWriteTable(macco.db,
               'cartes',
               tbl_prop_metier_zone_isis_strategy,
               append = TRUE,
               row.names = FALSE)
}
