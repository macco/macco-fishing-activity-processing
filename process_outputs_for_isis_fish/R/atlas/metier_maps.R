### Load sf object for 12m less

if(file.exists(here(path_data_tidy,
                    "vessels_12m_less",
                    "metier_zone",
                    "sf_effort_isis.rds"))){

  sf_effort_isis_12m_less <- readRDS(
    file = here(path_data_tidy,
                "vessels_12m_less",
                "metier_zone",
                "sf_effort_isis.rds"))  %>%
    dplyr::select(square_isis,
                  metier_isis,
                  metier_zone_fleet_isis,
                  LE_EFF_mean,
                  geometry)  %>%
    mutate(zone = "unique")
}


if(file.exists(here(path_data_tidy,
                    "vessels_12m_plus",
                    "metier_zone",
                    "sf_effort_isis.rds"))){

  list_spatial_clust_effort_metier_zone <- readRDS(
    file = here(path_data_tidy,
                "vessels_12m_plus",
                "metier_zone",
                "list_spatial_clust_effort_metier_zone.rds"))

  sf_effort_isis_12m_plus_aggregated <- map_dfr(list_spatial_clust_effort_metier_zone,
                                     ~ .x$df_spatial) %>%
    dplyr::select(square_isis,
                  metier_zone,
                  fishing_time_mean,
                  cluster,
                  geometry)

  df_vms_metier_zone_isis <- readr::read_rds(here(path_data_tidy,
                                                  "vessels_12m_plus_vms",
                                                  "isis",
                                                  "df_vms_metier_zone_isis.rds")) %>%
    rename(LE_RECT = ICESNAME)



  sf_effort_isis_12m_plus <- inner_join(df_vms_metier_zone_isis,
                                        sf_effort_isis_12m_plus_aggregated) %>%
    rename(LE_EFF_mean = fishing_time_mean) %>%
    mutate(square_isis = factor(square_isis))

  ###---------------------------------------------------------------------------
  ### for fleets without vms data
  sf_effort_isis_missing_vms <- readRDS(
          file = here(path_data_tidy,
                      "vessels_12m_plus",
                      "metier_zone",
                      "sf_effort_isis.rds")) %>%
    dplyr::select(square_isis,
                  metier_isis,
                  metier_zone_fleet_isis,
                  LE_EFF_mean,
                  geometry) %>%
    mutate(zone = "unique")
}

sf_effort_isis <- bind_rows(sf_effort_isis_12m_less,
                            sf_effort_isis_missing_vms,
                            sf_effort_isis_12m_plus) %>%
  mutate(key = glue("{metier_isis}-{metier_zone_fleet_isis}-{square_isis}")) %>%
  group_by(key) %>%
  slice_head(n=1)
