
f_palette_for_factor <- function(values_d,
                       palette = "Set1"){

  require("RColorBrewer")

  if (palette %in% rownames(RColorBrewer::brewer.pal.info)) {

  possibleColors <- colorRampPalette(brewer.pal(brewer.pal.info[palette, ]$maxcolors,
                                                 palette))(length(values_d))
  names(possibleColors) <- values_d

  return(possibleColors)} else {
    stop(cat(palette, " not a RColorBrewer palette \n"))
  }

}
