#######
### Load InterCatchData for foreign Catches and Landings - attention les landings de intercatch sont en tonnes
### Data preparation
### selection of country with data in 8a 8b, with important catch, remove FRA
### keep BE, ES and UK (removing country landing less than 50 tonnes over the 13 years)
### Format data with same gear, species, length then French dataset (sacrois)
### Lophius = MON
#####replace <10m by 0-10, >=40m by 40-80, 24<40m by 24-40, 10<24m by 18-24
##### assumption 10<24m = 18-24, vessel_length=-9 changed according Sonia Algorithm
#### new assumption (but PS will be put in Gear M and no landings of keyspecies)
######

if (file.exists(paste0(pathtidydataintercatch,"/Mixfish_BoB_landingsEffortForeign8ab_BEESUK_isis.rds"))) {

  Mixfish_BoB_landingsEffortForeign8ab_BEESUK_isis <-read_rds(paste0(pathtidydataintercatch,"/Mixfish_BoB_landingsEffortForeign8ab_BEESUK_isis.rds"))
} else {

 # Mixfish_BoB_effort <- readRDS(paste0(pathdataintercatch,"/Mixfish_BoB_effort.rds"))
 # Mixfish_BoB_landingsValues <- readRDS(paste0(pathdataintercatch,"/Mixfish_BoB_landingsValues.rds"))
 # df avant 2023 ajouter rename(metier=lvl4)

  Mixfish_BoB_effort <- readRDS(paste0(pathdataintercatch,"/Mixfish_BoB_effort_Nov2023.rds"))
  Mixfish_BoB_landingsValues <- readRDS(paste0(pathdataintercatch,"/Mixfish_BoB_landingsValues_Nov2023.rds"))

  ### remove French data and focus in 8ab
  ###country : replace UKS, UKE par UKN, filter macco species
  ###Gear : P replaced by M
  Mixfish_BoB_effortForeign8ab <- Mixfish_BoB_effort %>% ungroup() %>%
    filter(country != "FRA" & area %in% c("27.8.a", "27.8.b", "8.b")) %>%
    mutate(country=replace(country, country %in% c("UKN","UKS","UKE"),"UK")) %>%
    mutate(gear = substr(metier,1,1)) %>% mutate(gear=replace(gear,gear == "P", "M" ))%>%
    mutate(gearComp = substr(metier,1,3))

### attention
## dans intercatch pour la baudroie, il y a uniquement MON (Lophius piscatorius)
## dans sacrois baudroie = MNZ et inclu MON et ANK (Lophius budegassa)

  Mixfish_BoB_landingsValuesForeign8ab <- Mixfish_BoB_landingsValues %>% ungroup() %>%
    filter(country != "FRA" & area %in% c("27.8.a", "27.8.b", "8.b") & species %in% c("SOL","NEP","HKE","MON","MEG","RJC","RJN")) %>%
   # mutate(species=replace(species,species == "MON", "MNZ" )) %>%
    mutate(country=replace(country, country %in% c("UKN","UKS","UKE"),"UK")) %>%
    mutate(gear = substr(metier,1,1)) %>% mutate(gear=replace(gear,gear == "P", "M" )) %>%
    mutate(gearComp = substr(metier,1,3))


  ### WITHOUT removing French data and focus in 8ab
  ###country : replace UKS, UKE par UKN, filter macco species
  ###Gear : P replaced by M
  Mixfish_BoB_effortForeign8abWithFR <- Mixfish_BoB_effort %>% ungroup() %>%
    filter(area %in% c("27.8.a", "27.8.b", "8.b")) %>%
    mutate(country=replace(country, country %in% c("UKN","UKS","UKE"),"UK")) %>%
    mutate(gear = substr(metier,1,1)) %>% mutate(gear=replace(gear,gear == "P", "M" ))%>%
    mutate(gearComp = substr(metier,1,3))

  Mixfish_BoB_landingsValuesForeign8abWithFR <- Mixfish_BoB_landingsValues %>% ungroup() %>%
    filter(area %in% c("27.8.a", "27.8.b", "8.b") & species %in% c("SOL","NEP","HKE","MON","MEG","RJC","RJN")) %>%
    #mutate(species=replace(species,species == "MON", "MNZ" )) %>%
    mutate(country=replace(country, country %in% c("UKN","UKS","UKE"),"UK")) %>%
    mutate(gear = substr(metier,1,1)) %>% mutate(gear=replace(gear,gear == "P", "M" )) %>%
    mutate(gearComp = substr(metier,1,3))


  ###pour info il y a des 0 - Dans Mixfish ils les enlèvent !
  # L0<- Mixfish_BoB_landingsValuesForeign8ab %>% filter(landings==0)
  # E0 <- Mixfish_BoB_effortForeign8ab %>% filter(days_at_sea==0)



  ######landings per species per country
  #explore country
  # LandingsCountrySpecies <- Mixfish_BoB_landingsValuesForeign8ab  %>%
  #   group_by(country,species) %>% mutate(landingsTot=sum(landings)) %>%ungroup() %>% select(landingsTot,country,species) %>% filter(landingsTot>50)
  #
  # length(unique(LandingsCountrySpecies$year))
  #
  # p <- ggplot(data=LandingsCountrySpecies, aes(x=country, y =landingsTot,color=species)) + geom_point() +
  #   ggtitle("landings")
  # p

  ######country : keep BE, ES and UK (removing country landing less than 50 t over the 13 years)
  Mixfish_BoB_effortForeign8ab_BEESUK <- Mixfish_BoB_effortForeign8ab %>%
    filter(country %in% c("ES","BE","UK"))

  Mixfish_BoB_landingsValuesForeign8ab_BEESUK <- Mixfish_BoB_landingsValuesForeign8ab %>%
    filter(country %in% c("ES","BE","UK"))

  ######country : keep FRA, BE, ES and UK (removing country landing less than 50 t over the 13 years)
  Mixfish_BoB_effortForeign8ab_BEESUKWithFR <- Mixfish_BoB_effortForeign8abWithFR %>%
    filter(country %in% c("FRA","ES","BE","UK"))

  Mixfish_BoB_landingsValuesForeign8ab_BEESUKWithFR <- Mixfish_BoB_landingsValuesForeign8abWithFR %>%
    filter(country %in% c("FRA","ES","BE","UK"))

  ##### Vessel Length classes
  #explore vessel_length=-9
  # vesselLengthClasses <- Mixfish_BoB_effortForeign8ab_BEESUK %>% select(vessel_length) %>% distinct()
  # table(Mixfish_BoB_effortForeign8ab_BEESUK$vessel_length,Mixfish_BoB_effortForeign8ab_BEESUK$metier)[3,]
  # table(Mixfish_BoB_effortForeign8ab_BEESUK$vessel_length,Mixfish_BoB_effortForeign8ab_BEESUK$country)
  # test <- Mixfish_BoB_effortForeign8ab_BEESUK %>% filter(country == "ES" & vessel_length=="-9")
  # table(test$metier,test$area)
  #
  # test <- test %>%  group_by(year,country,area,species) %>% mutate(landingsTot=sum(landings)) %>%ungroup() %>% select(year,landingsTot,country,area,species)
  # testHKE <- test %>% filter(species=="HKE")
  # p <- ggplot(data=testHKE, aes(x=year, y =landingsTot,color=country,shape=area)) + geom_point() +
  #   geom_line() +
  #   ggtitle("HKE landings vessel_length=-9")
  # p

  ####replace -9 according Sonia algorithm
  gear_comp <- 'GNS'
  Mixfish_BoB_effortForeign8ab_BEESUK <- Mixfish_BoB_effortForeign8ab_BEESUK %>%
    mutate(vessel_length = ifelse(gearComp == gear_comp & (vessel_length == -9 | vessel_length == "") , '24<40m', vessel_length))
  Mixfish_BoB_landingsValuesForeign8ab_BEESUK<- Mixfish_BoB_landingsValuesForeign8ab_BEESUK %>%
    mutate(vessel_length = ifelse(gearComp == gear_comp & (vessel_length == -9 | vessel_length == ""), '24<40m', vessel_length))
  Mixfish_BoB_effortForeign8ab_BEESUKWithFR <- Mixfish_BoB_effortForeign8ab_BEESUKWithFR  %>%
    mutate(vessel_length = ifelse(gearComp == gear_comp & (vessel_length == -9 | vessel_length == ""), '24<40m', vessel_length))
  Mixfish_BoB_landingsValuesForeign8ab_BEESUKWithFR <- Mixfish_BoB_landingsValuesForeign8ab_BEESUKWithFR  %>%
    mutate(vessel_length = ifelse(gearComp == gear_comp & (vessel_length == -9 | vessel_length == ""), '24<40m', vessel_length))

  gear_comp <- 'GTR'
  Mixfish_BoB_effortForeign8ab_BEESUK <- Mixfish_BoB_effortForeign8ab_BEESUK %>%
    mutate(vessel_length = ifelse(gearComp == gear_comp & (vessel_length == -9 | vessel_length == ""), '10<24m', vessel_length))
  Mixfish_BoB_landingsValuesForeign8ab_BEESUK<- Mixfish_BoB_landingsValuesForeign8ab_BEESUK %>%
    mutate(vessel_length = ifelse(gearComp == gear_comp & (vessel_length == -9 | vessel_length == ""), '10<24m', vessel_length))
  Mixfish_BoB_effortForeign8ab_BEESUKWithFR <- Mixfish_BoB_effortForeign8ab_BEESUKWithFR %>%
    mutate(vessel_length = ifelse(gearComp == gear_comp & (vessel_length == -9 | vessel_length == ""), '10<24m', vessel_length))
  Mixfish_BoB_landingsValuesForeign8ab_BEESUKWithFR<- Mixfish_BoB_landingsValuesForeign8ab_BEESUKWithFR %>%
    mutate(vessel_length = ifelse(gearComp == gear_comp & (vessel_length == -9 | vessel_length == ""), '10<24m', vessel_length))

  gear_comp <- 'LLS'
  Mixfish_BoB_effortForeign8ab_BEESUK <- Mixfish_BoB_effortForeign8ab_BEESUK %>%
    mutate(vessel_length = ifelse(gearComp == gear_comp & (vessel_length == -9 | vessel_length == ""), '24<40m', vessel_length))
  Mixfish_BoB_landingsValuesForeign8ab_BEESUK<- Mixfish_BoB_landingsValuesForeign8ab_BEESUK %>%
    mutate(vessel_length = ifelse(gearComp == gear_comp & (vessel_length == -9 | vessel_length == ""), '24<40m', vessel_length))
  Mixfish_BoB_effortForeign8ab_BEESUKWithFR <- Mixfish_BoB_effortForeign8ab_BEESUKWithFR %>%
    mutate(vessel_length = ifelse(gearComp == gear_comp & (vessel_length == -9 | vessel_length == ""), '24<40m', vessel_length))
  Mixfish_BoB_landingsValuesForeign8ab_BEESUKWithFR <- Mixfish_BoB_landingsValuesForeign8ab_BEESUKWithFR %>%
    mutate(vessel_length = ifelse(gearComp == gear_comp & (vessel_length == -9 | vessel_length == ""), '24<40m', vessel_length))


  #### new assumption (but PS will be put in Gear M and no landings of keyspecies)
  gear_comp <- 'PS_'
  Mixfish_BoB_effortForeign8ab_BEESUK <- Mixfish_BoB_effortForeign8ab_BEESUK %>%
    mutate(vessel_length = ifelse(gearComp == gear_comp & (vessel_length == -9 | vessel_length == ""), '24<40m', vessel_length))
  Mixfish_BoB_landingsValuesForeign8ab_BEESUK<- Mixfish_BoB_landingsValuesForeign8ab_BEESUK %>%
    mutate(vessel_length = ifelse(gearComp == gear_comp & (vessel_length == -9 | vessel_length == ""), '24<40m', vessel_length))
  Mixfish_BoB_effortForeign8ab_BEESUKWithFR <- Mixfish_BoB_effortForeign8ab_BEESUKWithFR %>%
    mutate(vessel_length = ifelse(gearComp == gear_comp & (vessel_length == -9 | vessel_length == ""), '24<40m', vessel_length))
  Mixfish_BoB_landingsValuesForeign8ab_BEESUKWithFR<- Mixfish_BoB_landingsValuesForeign8ab_BEESUKWithFR %>%
    mutate(vessel_length = ifelse(gearComp == gear_comp & (vessel_length == -9 | vessel_length == ""), '24<40m', vessel_length))


  #Mixfish_BoB_landingsValuesForeign8ab_BEESUK %>% filter(gearComp=='PS_') %>% summarise(sum(landings))

  #####replace <10m by 0-10, >=40m by 40-80, 24<40m by 24-40, 10<24m by 18-24
  ##### assumption 10<24m = 18-24
  Mixfish_BoB_effortForeign8ab_BEESUK <-  Mixfish_BoB_effortForeign8ab_BEESUK %>%
    mutate(vessel_length_isis=recode_factor(vessel_length,
                                            "<10m" = "0-10",
                                            ">=40m"="40-80",
                                            "24<40m"="24-40",
                                            "10<24m"="18-24"))

  Mixfish_BoB_landingsValuesForeign8ab_BEESUK <- Mixfish_BoB_landingsValuesForeign8ab_BEESUK %>% ungroup() %>%
    mutate(vessel_length_isis=recode_factor(vessel_length,
                                            "<10m" = "0-10",
                                            ">=40m"="40-80",
                                            "24<40m"="24-40",
                                            "10<24m"="18-24"))
  Mixfish_BoB_effortForeign8ab_BEESUKWithFR <-  Mixfish_BoB_effortForeign8ab_BEESUKWithFR %>%
    mutate(vessel_length_isis=recode_factor(vessel_length,
                                            "<10m" = "0-10",
                                            ">=40m"="40-80",
                                            "24<40m"="24-40",
                                            "10<24m"="18-24"))

  Mixfish_BoB_landingsValuesForeign8ab_BEESUKWithFR <- Mixfish_BoB_landingsValuesForeign8ab_BEESUKWithFR %>% ungroup() %>%
    mutate(vessel_length_isis=recode_factor(vessel_length,
                                            "<10m" = "0-10",
                                            ">=40m"="40-80",
                                            "24<40m"="24-40",
                                            "10<24m"="18-24"))

  ####### variables isis et  transformation landings en kg pour etre comparable à sacrois
  ####### lpue = landings / effort (en heures)
  Mixfish_BoB_landingsValuesForeign8ab_BEESUK_isis <- Mixfish_BoB_landingsValuesForeign8ab_BEESUK   %>%
    mutate(metier_isis=metier) %>%
    mutate(metier_zone_isis=paste(metier,area,sep="-")) %>%
    mutate(fleet_isis=paste(country,vessel_length_isis,sep="-")) %>%
    mutate(landings = landings*1000) %>%
    select(fleet_isis,metier_isis,metier_zone_isis,gear,year,quarter,species,landings)

  Mixfish_BoB_effortForeign8ab_BEESUK_isis <- Mixfish_BoB_effortForeign8ab_BEESUK   %>%
    mutate(metier_isis=metier) %>%
    mutate(metier_zone_isis=paste(metier,area,sep="-")) %>%
    mutate(fleet_isis=paste(country,vessel_length_isis,sep="-")) %>%
    select(fleet_isis,metier_isis,metier_zone_isis,gear,year,quarter,days_at_sea)

# attention ne s'applique pas aux français (les definitions metier_isis et fleet_isis sont dans eflalo)
  # Mixfish_BoB_landingsValuesForeign8ab_BEESUKWithFR_isis <- Mixfish_BoB_landingsValuesForeign8ab_BEESUKWithFR   %>%
  #   mutate(metier_isis=metier) %>%
  #   mutate(metier_zone_isis=paste(metier,area,sep="-")) %>%
  #   mutate(fleet_isis=paste(country,vessel_length_isis,sep="-")) %>%
  #   select(fleet_isis,metier_isis,metier_zone_isis,gear,year,quarter,species,landings)
  #
  # Mixfish_BoB_effortForeign8ab_BEESUKWithFR_isis <- Mixfish_BoB_effortForeign8ab_BEESUKWithFR   %>%
  #   mutate(metier_isis=metier) %>%
  #   mutate(metier_zone_isis=paste(metier,area,sep="-")) %>%
  #   mutate(fleet_isis=paste(country,vessel_length_isis,sep="-")) %>%
  #   select(fleet_isis,metier_isis,metier_zone_isis,gear,year,quarter,days_at_sea)

  saveRDS(Mixfish_BoB_effortForeign8ab_BEESUK_isis,
          file = paste0(pathtidydataintercatch,"/Mixfish_BoB_effortForeign8ab_BEESUK_isis.rds"))

  #saveRDS(Mixfish_BoB_effortForeign8ab_BEESUKWithFR_isis,
  #        file = paste0(pathtidydataintercatch,"/Mixfish_BoB_effortForeign8ab_BEESUKWithFR_isis.rds"))
  # sum(is.na(Mixfish_BoB_effortForeign8ab_BEESUK_isis$days_at_sea))
  # sum(is.na(Mixfish_BoB_landingsValuesForeign8ab_BEESUK_isis$landings))



  Mixfish_BoB_landingsEffortForeign8ab_BEESUK_isis <- inner_join(Mixfish_BoB_landingsValuesForeign8ab_BEESUK_isis,Mixfish_BoB_effortForeign8ab_BEESUK_isis,
                                                                 by=c('fleet_isis','metier_isis','metier_zone_isis','year','quarter')) %>%
    mutate(gear_isis=gear.x,REF_YEAR=year,QUARTER=quarter,ESP_COD_FAO=species) %>% select(-c(gear.y,gear.x)) %>%
    mutate(lpue=landings/(days_at_sea*24),lpue_log_1 = log(lpue + 1))

  #Mixfish_BoB_landingsEffortForeign8ab_BEESUKWithFR_isis <- inner_join(Mixfish_BoB_landingsValuesForeign8ab_BEESUKWithFR_isis,Mixfish_BoB_effortForeign8ab_BEESUKWithFR_isis,
    #                                                              by=c('fleet_isis','metier_isis','metier_zone_isis','year','quarter')) %>%
    # mutate(gear_isis=gear.x,REF_YEAR=year,QUARTER=quarter,ESP_COD_FAO=species) %>% select(-c(gear.y,gear.x)) %>%
    # mutate(lpue=landings/(days_at_sea*24),lpue_log_1 = log(lpue + 1))

  ### ATTENTION IL Y A DES NA et des Inf du aux 0
  ### comme dans MIXFISH on  enlève les lignes avec des NA (car certains 0 dans les landings ont des values !=0)
  Mixfish_BoB_landingsEffortForeign8ab_BEESUK_isis <- Mixfish_BoB_landingsEffortForeign8ab_BEESUK_isis %>% filter(!is.na(lpue)) %>% filter(lpue != Inf)

  #Mixfish_BoB_landingsEffortForeign8ab_BEESUKWithFR_isis <- Mixfish_BoB_landingsEffortForeign8ab_BEESUKWithFR_isis %>% filter(!is.na(lpue)) %>% filter(lpue != Inf)


  saveRDS(Mixfish_BoB_landingsEffortForeign8ab_BEESUK_isis,
          file = paste0(pathtidydataintercatch,"/Mixfish_BoB_landingsEffortForeign8ab_BEESUK_isis.rds"))
  # saveRDS(Mixfish_BoB_landingsEffortForeign8ab_BEESUKWithFR_isis,
  #         file = paste0(pathtidydataintercatch,"/Mixfish_BoB_landingsEffortForeign8ab_BEESUKWithFR_isis.rds"))

}
######Exploration graphique
######landings per species
### NEP landings by foreign fleets can be neglected
#
# LandingsCountrySpecies <- Mixfish_BoB_landingsValuesForeign8ab_BEESUK  %>%
#   group_by(year,country,area,species) %>% mutate(landingsTot=sum(landings)) %>%ungroup() %>% select(year,landingsTot,country,area,species)
#
# LandingsCountryHKE <- LandingsCountrySpecies %>% filter(species=="HKE")
# p <- ggplot(data=LandingsCountryHKE, aes(x=year, y =landingsTot,color=country,shape=area)) + geom_point() +
#   geom_line() +
#   ggtitle("HKE landings")
# p
#
# LandingsCountrySOL <- LandingsCountrySpecies %>% filter(species=="SOL")
# p <- ggplot(data=LandingsCountrySOL, aes(x=year, y =landingsTot,color=country,shape=area)) + geom_point() +
#   geom_line() +
#   ggtitle("SOL landings")
# p
#
# LandingsCountryNEP <- LandingsCountrySpecies %>% filter(species=="NEP")
# p <- ggplot(data=LandingsCountryNEP, aes(x=year, y =landingsTot,color=country,shape=area)) + geom_point() +
#   geom_line() +
#   ggtitle("NEP landings")
# p
#
# LandingsCountryMEG <- LandingsCountrySpecies %>% filter(species=="MEG")
# p <- ggplot(data=LandingsCountryMEG, aes(x=year, y =landingsTot,color=country,shape=area)) + geom_point() +
#   geom_line() +
#   ggtitle("MEG landings")
# p
#
# LandingsCountryMON <- LandingsCountrySpecies %>% filter(species=="MON")
# p <- ggplot(data=LandingsCountryMON, aes(x=year, y =landingsTot,color=country,shape=area)) + geom_point() +
#   geom_line() +
#   ggtitle("MON landings")
# p

#
# metier4Gear <- Mixfish_BoB_effortForeign8ab %>% select(metier,area) %>% distinct()
#
# #######landings per metier
#
# LandingsAreaMetierSpecies <- Mixfish_BoB_landingsValuesForeign8ab  %>%
#   group_by(year,area,species,metier) %>% mutate(landingsTot=sum(landings)) %>%ungroup() %>% select(year,landingsTot,area,species,metier)
#
# LandingsAreaMetierSpeciesHKE <- LandingsAreaMetierSpecies %>% filter(species=="HKE")
# p <- ggplot(data=LandingsAreaMetierSpeciesHKE, aes(x=year, y =landingsTot,color=metier,shape=area)) + geom_point() + geom_line()
#   ggtitle("HKE landings")
# p
