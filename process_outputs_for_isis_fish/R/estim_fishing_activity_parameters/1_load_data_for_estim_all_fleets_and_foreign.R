################################################################################
### Load data for estimation of fishing coefficient
### Eflalo format in not suitable, it needs a long-table format such as sacrois
### but with the newly computed variables (metier, strategy, fleet)
### with all vessels of the fshery
### a common time scale between forign vessels, >12m and<12m lust be selected : here quarter
###et reduire les captures de MNZ à celle de MON
### ratio MON=0.62*MNZ (cf WGBIE 2022)
###
#####  save table for validation
#####  save table metier_isis - esp_capturables- landingsYearMean ---> valeur de ref des TF et atlas
################################################################################
if (file.exists(glue("{path_sacrois_data_tidy}/df_sacrois_key_species_ag_foreign_final_{year_range[1]}_{year_range[2]}.rds")) &
    erase_tidy_data == FALSE){

  df_sacrois_key_species_ag_foreign_final <- readRDS(
          file = glue("{path_sacrois_data_tidy}/df_sacrois_key_species_ag_foreign_final_{year_range[1]}_{year_range[2]}.rds"))


} else {


  cat("4_1: Load eflalo final dataset with isis variables (metier, fleet, strategy, gear) >12m and <12m \n")
  ###-----------------------------------------------------------------------------
  ###-----------------------------------------------------------------------------
  if (file.exists(here(path_fishery_data_tidy,
                       "eflalo",
                       glue("df_eflalo_all_fleets{year_range[1]}_{year_range[2]}.rds")))) {

    df_eflalo <- read_rds(here(path_fishery_data_tidy,
                               "eflalo",
                               glue("df_eflalo_all_fleets{year_range[1]}_{year_range[2]}.rds")))

  } else {
    stop("Run 5_0_fleet_definition.R to create df_eflalo with metier isis")
  }



  if (file.exists(paste0(path_intercatch_data_tidy,
                         "/Mixfish_BoB_landingsEffortForeign8ab_BEESUK_isis.rds"))) {

    Mixfish_BoB_landingsEffortForeign8ab_BEESUK_isis <-read_rds(paste0(path_intercatch_data_tidy,
                                                                       "/Mixfish_BoB_landingsEffortForeign8ab_BEESUK_isis.rds"))
  } else {
    stop("Run InterCatchExploration to create FleetMetier_isis_LandingsEffort for foreign fleets")
  }

  Mixfish_BoB_landingsEffortForeign8ab_BEESUK_isis_red <- Mixfish_BoB_landingsEffortForeign8ab_BEESUK_isis %>%
    mutate(QUARTER = case_when(substr(QUARTER, 1, 1) == 'Q' ~ substr(QUARTER, 2, 2),
                               TRUE ~ QUARTER)) %>%
    dplyr::select(REF_YEAR, QUARTER, metier_isis, fleet_isis, ESP_COD_FAO,
                  gear_isis, landings, lpue, lpue_log_1)

  # unique(Mixfish_BoB_landingsEffortForeign8ab_BEESUK_isis_red$REF_YEAR)
  # unique(Mixfish_BoB_landingsEffortForeign8ab_BEESUK_isis_red$QUARTER)
  # unique(Mixfish_BoB_landingsEffortForeign8ab_BEESUK_isis_red$metier_isis)
  # unique(Mixfish_BoB_landingsEffortForeign8ab_BEESUK_isis_red$fleet_isis)
  # unique(Mixfish_BoB_landingsEffortForeign8ab_BEESUK_isis_red$ESP_COD_FAO)
  # unique(Mixfish_BoB_landingsEffortForeign8ab_BEESUK_isis_red$gear_isis)
  # sum(is.na(Mixfish_BoB_landingsEffortForeign8ab_BEESUK_isis_red$lpue))
  # sum(is.na(Mixfish_BoB_landingsEffortForeign8ab_BEESUK_isis_red$lpue_log_1))
  #

  ###----------------------------------------------------------------------------
  ### list of metier_OTH : metiers caughting species not in key_species (removed
  ### from the estimation of catchability factors)
  ### listMetiersOTH
  df_MetiersOTH <- df_eflalo %>%
    dplyr::select(metier_isis, fleet_isis, metier_zone_fleet_isis, vessel_type_isis) %>%
    distinct() %>%
    mutate(metier_isis = as.character(metier_isis)) %>%
    filter(grepl("metier_OTH", metier_isis)) %>%
    mutate(metier_zone_fleet_isis = case_when(is.na(metier_zone_fleet_isis) ~ paste0(metier_isis, "_Zunique @ ", fleet_isis),
                                              TRUE ~ as.character(metier_zone_fleet_isis)),
           metier_zone_fleet_isis_import = case_when(vessel_type_isis != "12-15" ~ paste0(metier_isis, "_Zunique @ 15-Inf"),
                                                     TRUE ~ paste(metier_isis, "_Zunique @ ", fleet_isis,sep="")),
           metier_zone_fleet_isis_import = factor(metier_zone_fleet_isis_import))

  saveRDS(df_MetiersOTH,
          file = paste0(path_eflalo_data, "/df_MetiersOTH.rds"))

  ###-----------------------------------------------------------------------------
  ### select eflalo columns to merge with sacrois
  ### and compute LPUE (landings per unit effort)
  dfl_eflalo_key_species <- df_eflalo %>%
    dplyr::select(-starts_with("LE_EURO_")) %>%
    pivot_longer(. ,
                 cols = starts_with("LE_KG"),
                 names_prefix = "LE_KG_",
                 names_to = "ESP_COD_FAO",
                 values_to = "LE_KG") %>%
    mutate(lpue = LE_KG / LE_EFF,
           lpue_log_1 = log(lpue + 1),
           LE_ID = factor(LE_ID)) %>%
    mutate(across(where(is.factor), fct_infreq)) %>%
    droplevels()

  ###-----------------------------------------------------------------------------
  ### extract species caught per metier_isis
  df_metier_isis_all_species  <- dfl_eflalo_key_species  %>%
    dplyr::select(metier_isis, ESP_COD_FAO, LE_KG) %>%
    filter(ESP_COD_FAO != "OTH", !str_detect(metier_isis, "OTH")) %>%
    group_by(metier_isis, ESP_COD_FAO) %>%
    summarise(LE_KG_mean = mean(LE_KG)) %>%
    group_by(metier_isis) %>%
    mutate(LE_KG_prop = round(LE_KG_mean / sum(LE_KG_mean), digits = 2))

  df_metier_isis_key_species <- df_metier_isis_all_species %>%
    filter(LE_KG_prop > treshold_species_caught_per_metier_isis)

  ### shape data frame to merge with eflalo long
  df_metier_isis_key_species_caught <- df_metier_isis_all_species %>%
    filter(LE_KG_prop > treshold_species_caught_per_metier_isis) %>%
    dplyr::select(metier_isis, ESP_COD_FAO) %>%
    distinct() %>%
    group_by(metier_isis) %>%
    summarise(Species_caugth = paste0(ESP_COD_FAO, collapse = "-"))

  ### shape data frame to merge with eflalo long without filtering on key species
  df_metier_isis_all_species_caught <- df_metier_isis_all_species %>%
    dplyr::select(metier_isis, ESP_COD_FAO) %>%
    distinct() %>%
    group_by(metier_isis) %>%
    summarise(Species_caugth = paste0(ESP_COD_FAO, collapse = "-"))

  ### filter eflalo data to keep the couple metier_isis species caught by each metier_isis
  df_sacrois_key_species <- left_join(dfl_eflalo_key_species,
                                      df_metier_isis_key_species_caught,
                                      by = "metier_isis") %>%
    filter(str_detect(pattern = ESP_COD_FAO,  string = Species_caugth))

  saveRDS(df_sacrois_key_species,
          file = glue("{path_sacrois_data_tidy}/df_sacrois_key_species_{year_range[1]}_{year_range[2]}.rds"))

  ###-----------------------------------------------------------------------------
  ### agregate sacrois by year and quarter et reduire les captures de MNZ à celle de MON
  ### ratio MON=0.62*MNZ (cf WGBIE 2022)

  df_sacrois_key_species_ag <- df_sacrois_key_species %>%
    mutate(QUARTER = substr(quarters(MAREE_DATE_DEP), 2, 2)) %>%
    group_by(REF_YEAR, QUARTER, metier_isis, fleet_isis, ESP_COD_FAO) %>%
    mutate(LE_KG_sum = sum(LE_KG),
           LE_EFF_sum = sum(LE_EFF),
           REF_YEAR = as.numeric(as.character(REF_YEAR)),
           LE_KG_sum = if_else(ESP_COD_FAO == "MNZ", 0.62 * LE_KG_sum, LE_KG_sum),
           ESP_COD_FAO = replace(ESP_COD_FAO, ESP_COD_FAO == "MNZ", "MON"),
           metier_isis = as.character(metier_isis),
           fleet_isis = as.character(fleet_isis),
           gear_isis = as.character(gear_isis),
           lpue = LE_KG_sum / LE_EFF_sum,
           lpue_log_1 = log(lpue + 1),
           landings = LE_KG_sum) %>%
    dplyr::select(REF_YEAR, QUARTER, metier_isis, fleet_isis, ESP_COD_FAO,
                  gear_isis, landings, lpue, lpue_log_1) %>%
    ungroup() %>%
    distinct()

  # tester si il faut faire ungroup() et distinct()

  saveRDS(df_sacrois_key_species_ag,
          file =  glue("{path_sacrois_data_tidy}/df_sacrois_key_species_ag_{year_range[1]}_{year_range[2]}.rds"))

  ###-----------------------------------------------------------------------------
  df_sacrois_all_species <- left_join(dfl_eflalo_key_species,
                                      df_metier_isis_all_species_caught,
                                      by = "metier_isis")

  saveRDS(df_sacrois_all_species,
          file = glue("{path_sacrois_data_tidy}/df_sacrois_all_species_{year_range[1]}_{year_range[2]}.rds"))

  df_sacrois_all_species_ag <- df_sacrois_all_species %>%
    mutate(QUARTER = substr(quarters(MAREE_DATE_DEP), 2, 2)) %>%
    group_by(REF_YEAR, QUARTER, metier_isis, fleet_isis, ESP_COD_FAO) %>%
    mutate(landings = sum(LE_KG),
           REF_YEAR = as.numeric(as.character(REF_YEAR)),
           landings = if_else(ESP_COD_FAO == "MNZ", 0.62 * landings,landings),
           ESP_COD_FAO = replace(ESP_COD_FAO, ESP_COD_FAO == "MNZ", "MON"),
           metier_isis = as.character(metier_isis),
           fleet_isis = as.character(fleet_isis),
           gear_isis = as.character(gear_isis)) %>%
    dplyr::select(REF_YEAR, QUARTER, metier_isis, fleet_isis, ESP_COD_FAO,
                  gear_isis, landings) %>%
    ungroup() %>%
    distinct()

  saveRDS(df_sacrois_all_species_ag,
          file =  glue("{path_sacrois_data_tidy}/df_sacrois_all_species_ag_{year_range[1]}_{year_range[2]}.rds"))

  ###verif que testall= eflalo >testkey
  testkey <- df_sacrois_key_species_ag %>%
    filter(REF_YEAR == 2017) %>%
    group_by(ESP_COD_FAO) %>%
    mutate(landingstotkey = sum(landings)) %>%
    select(ESP_COD_FAO,landingstotkey) %>%
    ungroup() %>%
    distinct()

  testall <- df_sacrois_all_species_ag %>%
    filter(REF_YEAR==2017) %>%
    group_by(ESP_COD_FAO) %>%
    mutate(landingstotall = sum(landings)) %>%
    select(ESP_COD_FAO,landingstotall) %>%
    ungroup() %>%
    distinct()
  test <- full_join(testall, testkey,
                    by = "ESP_COD_FAO")

  testeflalo <- df_eflalo %>%
    filter(REF_YEAR == 2017) %>%
    dplyr::select(starts_with("LE_KG_")) %>%
    apply(2, sum)

  #table(df_sacrois_key_species$fleet_isis,df_sacrois_key_species$ESP_COD_FAO)
  #table(Mixfish_BoB_landingsEffortForeign8ab_BEESUK_isis_red$gear_isis,Mixfish_BoB_landingsEffortForeign8ab_BEESUK_isis_red$ESP_COD_FAO)

  df_sacrois_key_species_ag <- readRDS(glue("{path_sacrois_data_tidy}/df_sacrois_key_species_ag_{year_range[1]}_{year_range[2]}.rds"))

  #unique(df_sacrois_key_species_ag$fleet_isis)

  ###-----------------------------------------------------------------------------
  # verifier si les unites sont bonnes pour les etrangers avant de faire le rowbind : il faut x 1000 les landings des etrangers - corrigé dans le load intercatch
  testMix <- Mixfish_BoB_landingsEffortForeign8ab_BEESUK_isis_red %>%
    filter(REF_YEAR == 2017) %>%
    group_by(ESP_COD_FAO) %>%
    mutate(landingstot = sum(landings)) %>%
    select(ESP_COD_FAO, landingstot) %>%
    ungroup() %>%
    distinct()

  testMixall <- full_join(testall, testMix,
                          by = "ESP_COD_FAO") %>%
    mutate(landTOT = landingstot + landingstotall) %>%
    rename(FRsacroisForeignIntercatch = landTOT,
           FRsacrois = landingstotall,
           ForeignIntercatch = landingstot)

  dataplot <- testMixall %>%
    gather(key="type_land",value="landingsOrcatches",2:4) %>%
    filter(!(ESP_COD_FAO %in% c("CET","WHG")))

  ggplot(data = dataplot,
         aes(x = ESP_COD_FAO, y = landingsOrcatches, fill = type_land)) +
    geom_bar(stat="identity",position='dodge') +
    theme(axis.text.x = element_text(angle = 90))


  df_sacrois_key_species_ag_foreign <- bind_rows(df_sacrois_key_species_ag,
                                                 Mixfish_BoB_landingsEffortForeign8ab_BEESUK_isis_red)

  saveRDS(df_sacrois_key_species_ag_foreign,
          file = glue("{path_sacrois_data_tidy}/df_sacrois_key_species_ag_foreign_{year_range[1]}_{year_range[2]}.rds"))

  ###-----------------------------------------------------------------------------
  ####nettoyage des données pour ajustement du modele et preparation des variables

  df_sacrois_key_species_ag_foreign <- readRDS(glue("{path_sacrois_data_tidy}/df_sacrois_key_species_ag_foreign_{year_range[1]}_{year_range[2]}.rds"))

  ###-----------------------------------------------------------------------------
  ### recoding necessary variables : REF_YEAR as factor, Gear_isis = Mixed into M,
  ### FleetLength for efficiency

  df_sacrois_key_species_ag_foreign <- df_sacrois_key_species_ag_foreign %>%
    ungroup() %>%
    #filter(ESP_COD_FAO %in% c("SOL","NEP","HKE","MNZ","MEG","RJC","RJN")) %>%
    filter(ESP_COD_FAO %in% c("SOL", "NEP","HKE", "MON", "MEG", "RJC", "RJN")) %>%
    mutate(REF_YEAR = as.factor(REF_YEAR),
           gear_isis = replace(gear_isis, gear_isis == "Mixed", "M")) %>%
    droplevels()

  df_sacrois_key_species_ag_foreign_final <- df_sacrois_key_species_ag_foreign %>%
    mutate(lengthmin = str_split(fleet_isis, "-", simplify = T)[, 2],
           lengthmax = str_split(fleet_isis, "-", simplify = T)[, 3],
           fleetLength = paste(lengthmin, lengthmax, sep = "-"))

  saveRDS(df_sacrois_key_species_ag_foreign_final,
          file = glue("{path_sacrois_data_tidy}/df_sacrois_key_species_ag_foreign_final_{year_range[1]}_{year_range[2]}.rds"))

  # verif
  # table(df_sacrois_key_species_ag_foreign_final$gear_isis,df_sacrois_key_species_ag_foreign_final$ESP_COD_FAO)
  # test <-table(df_sacrois_key_species_ag_foreign_final$metier_isis,df_sacrois_key_species_ag_foreign_final$fleet_isis)
  # unique(df_sacrois_key_species_ag_foreign_final$REF_YEAR)
  # unique(df_sacrois_key_species_ag_foreign_final$QUARTER)
  # unique(df_sacrois_key_species_ag_foreign_final$metier_isis)
  # unique(df_sacrois_key_species_ag_foreign_final$fleet_isis)
  # unique(df_sacrois_key_species_ag_foreign_final$ESP_COD_FAO)
  # unique(df_sacrois_key_species_ag_foreign_final$gear_isis)
  # sum(is.na(df_sacrois_key_species_ag_foreign_final$lpue))
  # sum(df_sacrois_key_species_ag_foreign_final$lpue==Inf)
  # sum(is.na(df_sacrois_key_species_ag_foreign_final$lpue_log_1))
  # test2 <-df_sacrois_key_species_ag_foreign%>% filter(metier_isis=="O_NS_CEP"&ESP_COD_FAO=="NEP")

}
