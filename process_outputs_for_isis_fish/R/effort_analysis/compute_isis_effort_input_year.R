################################################################################
### Compute isis effort for input and calibration
################################################################################

###-----------------------------------------------------------------------------
### metier_isis_zone per fleet et strategy

dfs_metier_zone_isis_per_fleet <- dfs_eflalo_metier_isis_zone %>%
  dplyr::select(metier_zone_isis,
                fleet_isis,
                metier_isis,
                #               metier_zone_fleet_isis,
                metier_zone_fleet_isis_import) %>%
  distinct()

dfs_month_per_fleet <- expand_grid(fleet_isis = unique(df_eflalo$fleet_isis),
                                   MONTH = unique(df_eflalo$MONTH),
                                   REF_YEAR = unique(df_eflalo$REF_YEAR))

dfs_fleet_isis_strategy_isis <- expand(df_eflalo,
                                       nesting(fleet_isis, strategy_isis))

dfs_metier_zone_isis_per_fleet_month <- left_join(dfs_metier_zone_isis_per_fleet,
                                                  dfs_month_per_fleet) %>%
  left_join(., dfs_fleet_isis_strategy_isis)


###-----------------------------------------------------------------------------
### compute number of navs per strategy_isis and year
dfs_navs_per_strategy_year <- df_eflalo %>%
  dplyr::select(REF_YEAR, fleet_isis, strategy_isis, VE_REF) %>%
  distinct() %>%
  group_by(REF_YEAR, fleet_isis, strategy_isis) %>%
  summarise(n_vessel = n())

###-----------------------------------------------------------------------------
### compute mean effort per year strategy_isis and month
dfs_effort_per_strategy_year_month <- df_eflalo %>%
  group_by(REF_YEAR, strategy_isis, MONTH) %>%
  summarise(effort_strategy_total = sum(LE_EFF)) %>%
  left_join(., dfs_navs_per_strategy_year) %>%
  mutate(effort_strategy_per_nav = effort_strategy_total / n_vessel)

###-----------------------------------------------------------------------------
dfs_navs_per_strategy_input <- dfs_navs_per_strategy_year


###-----------------------------------------------------------------------------
### Choose how to compute the effort per strategy_isis as input
dfs_effort_per_strategy_month_input <- dfs_effort_per_strategy_year_month

###---------------------------------------------------------------------------
### compute mean effort per year strategy_isis, month and metier_isis
dfs_effort_per_strategy_year_month_metier_isis_input <- df_eflalo %>%
  group_by(REF_YEAR, MONTH, strategy_isis, fleet_isis,
           #          metier_fleet_isis, metier_isis) %>%
           metier_isis) %>%
  summarise(effort_metier_isis_total = sum(LE_EFF)) %>%
  left_join(., dfs_navs_per_strategy_input) %>%
  mutate(effort_metier_isis_per_nav = effort_metier_isis_total / n_vessel)

###-----------------------------------------------------------------------------
### compute percentage of effort per metier_isis given strategy_isis
dfs_effort_percentage_metier_isis <- left_join(dfs_effort_per_strategy_year_month_metier_isis_input,
                                               dfs_effort_per_strategy_month_input) %>%
  mutate(prop_effort_metier_isis = round(effort_metier_isis_per_nav / effort_strategy_per_nav, 3),
         percentage_effort_metier_isis = prop_effort_metier_isis * 100)

###-----------------------------------------------------------------------------
###----reste à distribuer l'effort de metier_isisxfleet_isis sur metier_zone_isis

dfs_effort_percentage_metier_isis_ratio_zone <- left_join(dfs_effort_percentage_metier_isis,
                                                          dfs_ratio_effort_metier_zone_high_low,
                                                          by = c("fleet_isis", "metier_isis")) %>%
  mutate(prop_fishing_time_zone = ifelse(is.na(prop_fishing_time_zone), 1,
                                         prop_fishing_time_zone),
         metier_zone_isis = as.character(metier_zone_isis),
         metier_zone_isis = ifelse(is.na(metier_zone_isis), paste0(metier_isis,"_Zunique"),
                                   metier_zone_isis),
         prop_effort_metier_isis = prop_effort_metier_isis * prop_fishing_time_zone)



dfs_prop_metier_zone_isis <- dfs_effort_percentage_metier_isis_ratio_zone %>%
  group_by(REF_YEAR) %>%
  select(prop_effort_metier_isis,
         metier_zone_isis,
         fleet_isis,
         strategy_isis,
         REF_YEAR ,
         MONTH) %>%
  right_join(.,
             dfs_metier_zone_isis_per_fleet_month,
             by = join_by(metier_zone_isis, fleet_isis, strategy_isis,
                          REF_YEAR, MONTH)) %>%
  mutate(prop_effort_metier_isis = ifelse(is.na(prop_effort_metier_isis), 0,
                                          prop_effort_metier_isis),
         prop_effort_metier_isis = round(prop_effort_metier_isis, digits = 3),
         metier_zone_fleet_isis_import = if_else( (strategy_isis == "Caseyeurs Métiers de l hameçon exclusifs-10-12") | (strategy_isis == "Fileyeurs Métiers de l hameçon exclusifs-10-12"),
                                                  paste(as.character(metier_zone_isis),as.character(strategy_isis),sep=" @ "), metier_zone_fleet_isis_import))


###-----------------------------------------------------------------------------
### mois dois commencer de 0
### Metiers = les metiers de fleet_isis
dfs_prop_metier_zone_isis %>%
  mutate(fr.ifremer.isisfish.types.Month = as.numeric(as.character(MONTH)) - 1,
         Metier = as.character(metier_zone_fleet_isis_import),
         Strategy = as.character(strategy_isis)) %>%
  group_by(Strategy, REF_YEAR) %>%
  group_split() %>%
  purrr::map(.x = ., ~ write_as_semantic_matrix(.x,
                                                filename = file.path(path_fishery_data_tidy_isis,
                                                                     "str_prop_met",
                                                                     "years",
                                                                     paste0(unique(.x$REF_YEAR),
                                                                            "_" ,unique(.x$Strategy), ".txt")),
                                                val_col = "prop_effort_metier_isis",
                                                var_cols = c("Metier", "fr.ifremer.isisfish.types.Month"),
                                                complete = TRUE))
