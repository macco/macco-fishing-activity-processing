###-----------------------------------------------------------------------------
### Compute inactivity in days and export mean value for strategy of +12m and -12m
###-----------------------------------------------------------------------------

#----------------------------------------------------------------------------------
######### Pour eflalo_all_fleets (+12m et -12m)
#----------------------------------------------------------------------------------

###-----------------------------------------------------------------------------
### compute number of navs per strategy_isis and year
dfs_navs_per_strategy_year <- df_eflalo %>%
  dplyr::select(REF_YEAR, strategy_isis, VE_REF) %>%
  distinct() %>%
  group_by(REF_YEAR, strategy_isis) %>%
  summarise(n_vessel = n())

saveRDS(dfs_navs_per_strategy_year, file = file.path(path_fishery_data_tidy_isis,
                                                     "inactivite",
                                                     "years",
                                                     "dfs_navs_per_strategy_year.rds"))

###-----------------------------------------------------------------------------
### compute mean effort per year strategy_isis and month
dfs_effort_per_strategy_year_month <- df_eflalo %>%
  group_by(REF_YEAR, strategy_isis, MONTH) %>%
  summarise(effort_strategy_total = sum(LE_EFF)) %>%
  left_join(., dfs_navs_per_strategy_year) %>%
  mutate(effort_strategy_per_nav = effort_strategy_total / n_vessel)

# ###-----------------------------------------------------------------------------
# ### compute mean effort per year strategy_isis and month
# dfs_effort_per_strategy_year_month <- df_eflalo %>%
#   group_by(REF_YEAR, strategy_isis, MONTH, VE_REF) %>%
#   summarise(effort_strategy_total = sum(LE_EFF)) %>%
#   group_by(strategy_isis, MONTH, REF_YEAR) %>%
#   summarise(effort_strategy_per_nav = mean(effort_strategy_total))

###-----------------------------------------------------------------------------
dfs_inactivity_year <- left_join(dfs_effort_per_strategy_year_month,
                                 df_days_in_month) %>%
  mutate(inactivity_hours = hours_in_month - effort_strategy_per_nav,
         inactivity_days = inactivity_hours / 24,
         inactivity = case_when(effort_isis_scale == "days" ~ inactivity_days,
                                effort_isis_scale == "hours" ~ inactivity_hours))

saveRDS(dfs_inactivity_year, file = file.path(path_fishery_data_tidy_isis,
                                              "inactivite",
                                              "years",
                                              "dfs_inactivity_year.rds"))

###-----------------------------------------------------------------------------
### pour saisir dans la bdd isis
dfs_inactivity_mean <- dfs_inactivity_year %>%
  group_by(strategy_isis, MONTH) %>%
  summarise(inactivity = mean(inactivity_days))

dfs_inactivity_mean_export <- dfs_inactivity_year %>%
  ungroup %>%
  tidyr::expand(. ,strategy_isis, nesting(MONTH, days_in_month)) %>%
  left_join(., dfs_inactivity_mean) %>%
  mutate(inactivity = ifelse(is.na(inactivity),
                             days_in_month,
                             inactivity))


###-----------------------------------------------------------------------------
### export
dir.create(file.path(path_data_tidy,
                     "isis",
                     "inactivite"),
           showWarnings = FALSE)

if (erase_coda) {
  map(list.files(file.path(path_data_tidy,
                           "isis",
                           "inactivite"),
                 full.names = TRUE),
      file.remove)
}

dfs_inactivity_mean_export %>%
  mutate(inactivity = round(inactivity, 3) %>% as.character()) %>%
  rename(Strategy=strategy_isis) %>%
  mutate(Strategy=as.character(Strategy)) %>%
  with(., split(inactivity, Strategy)) %>%
  iwalk(function(val, id) writeLines(val,
                                     file.path(path_fishery_data_tidy_isis,
                                               "inactivite",
                                               paste0(id, ".txt"))))





# dfs_inactivity_year_export %>%
#   mutate(inactivity = round(inactivity, 3) %>%
#            as.character()) %>%
#   group_by(strategy_isis, REF_YEAR) %>%
#   group_split() %>%
#   purrr::map(.x = ., ~  writeLines(.x$inactivity,
#                                    file.path(path_data_tidy,
#                                              "isis",
#                                              "inactivite",
#                                              "years",
#                                              paste0(unique(.x$REF_YEAR), "_",
#                                                     unique(.x$strategy_isis),
#                                                     ".txt"))))


#### si eflalo ne contient pas les moins de 12m
### ajout des strategies moins de 12m

# dfs_inactivity_year_moins12m <- readRDS("../macco-fishing-activity-processing-under-12m/data/tidy/bob_macco/isis/inactivite/years/dfs_inactivity_year.rds")
#
#
# dfs_inactivity_year_moins12m_export_grid <- tidyr::expand_grid(strategy_isis = unique((dfs_inactivity_year_moins12m$strategy_isis)),
#                                                                REF_YEAR = unique((dfs_inactivity_year_moins12m$REF_YEAR)),
#                                                                nesting(MONTH = as.character(df_days_in_month$MONTH),
#                                                                        days_in_month = df_days_in_month$days_in_month))
#
#
#
# dfs_inactivity_year_export_moins12m <- left_join(dfs_inactivity_year_moins12m_export_grid,
#                                                  dfs_inactivity_year_moins12m) %>%
#   mutate(inactivity = ifelse(is.na(inactivity_days),
#                              days_in_month,
#                              inactivity_days)) %>%
#   ungroup() %>%
#   mutate(strategy_isis = as.character(strategy_isis)) %>%
#   select(REF_YEAR, strategy_isis, MONTH, inactivity) %>%
#   distinct() %>%
#   mutate(strategy_isis = as.factor(strategy_isis),
#          strategy_isis = recode_factor(strategy_isis,
#                                        "Caseyeurs Métiers de l'hameçon exclusifs-10-12" = "Caseyeurs Métiers de l hameçon exclusifs-10-12",
#                                        "Fileyeurs Métiers de l'hameçon exclusifs-10-12" = "Fileyeurs Métiers de l hameçon exclusifs-10-12"),
#          strategy_isis = as.character(strategy_isis))

