################################################################################
### Compute isis effort for input and calibration
################################################################################

###-----------------------------------------------------------------------------
### export
import::here(.from = "R/export_isis/isis_semantic_matrix.R", .directory = here::here(),
             write_as_semantic_matrix
)
import::here(.from = "R/export_isis/utils_misc.R", .directory = here::here(),
             sep_char
)

###-----------------------------------------------------------------------------
### metier_isis_zone per fleet
dfs_metier_zone_isis_per_fleet <- dfs_eflalo_metier_isis_zone %>%
  dplyr::select(metier_zone_isis,
                fleet_isis,
                metier_zone_fleet_isis) %>%
  distinct()

dfs_month_per_fleet <- expand_grid(fleet_isis = unique(df_eflalo$fleet_isis),
                                   MONTH = unique(df_eflalo$MONTH))

dfs_fleet_isis_strategy_isis <- expand(df_eflalo, nesting(fleet_isis, strategy_isis))

dfs_metier_zone_isis_per_fleet_month <- left_join(dfs_metier_zone_isis_per_fleet,
                                                  dfs_month_per_fleet) %>%
  left_join(., dfs_fleet_isis_strategy_isis)

###-----------------------------------------------------------------------------
### compute number of navs per strategy_isis and year
dfs_navs_per_strategy_year <- df_eflalo %>%
  dplyr::select(REF_YEAR, fleet_isis, strategy_isis, VE_REF) %>%
  distinct() %>%
  group_by(REF_YEAR, fleet_isis, strategy_isis) %>%
  summarise(n_vessel = n())

###-----------------------------------------------------------------------------
### compute mean effort per year strategy_isis and month
dfs_effort_per_strategy_year_month <- df_eflalo %>%
  group_by(REF_YEAR, strategy_isis, MONTH) %>%
  summarise(effort_strategy_total = sum(LE_EFF)) %>%
  left_join(., dfs_navs_per_strategy_year) %>%
  mutate(effort_strategy_per_nav = effort_strategy_total / n_vessel)

###-----------------------------------------------------------------------------
### Choose how to compute the number of vessels per strategy_isis as input
### last year
if (strategy_n_navs_input == "last_year") {

  dfs_navs_per_strategy_input <- dfs_navs_per_strategy_year %>%
    filter(REF_YEAR == ending_year) %>%
    ungroup() %>%
    select(-REF_YEAR)

}

### mean over the entire time series
if (strategy_n_navs_input == "mean_over_years") {

  dfs_navs_per_strategy_input <- dfs_navs_per_strategy_year %>%
    group_by(strategy_isis) %>%
    summarise(n_vessel = round(mean(n_vessel), digits = 0))
}

###-----------------------------------------------------------------------------
### Choose how to compute the effort per strategy_isis as input

### last year
if (strategy_effort_input == "last_year") {

  dfs_effort_per_strategy_month_input <- dfs_effort_per_strategy_year_month %>%
    filter(REF_YEAR == ending_year) %>%
    ungroup() %>%
    select(-REF_YEAR)

  ###---------------------------------------------------------------------------
  ### compute mean effort per year strategy_isis, month and metier_isis
  dfs_effort_per_strategy_year_month_metier_isis_input <- df_eflalo %>%
    filter(REF_YEAR == ending_year) %>%
    group_by(MONTH, strategy_isis, fleet_isis,
              metier_fleet_isis, metier_isis) %>%
    summarise(effort_metier_isis_total = sum(LE_EFF)) %>%
    left_join(., dfs_navs_per_strategy_input) %>%
    mutate(effort_metier_isis_per_nav = effort_metier_isis_total / n_vessel)
}

### mean over the entire time series
if (strategy_effort_input == "mean_over_years") {

  dfs_effort_per_strategy_month_input <- dfs_effort_per_strategy_year_month %>%
    group_by(strategy_isis, MONTH) %>%
    summarise(effort_strategy_per_nav = mean(effort_strategy_per_nav))

  ###---------------------------------------------------------------------------
  ### compute mean effort per year strategy_isis, month and metier_isis
  dfs_effort_per_strategy_year_month_metier_isis_input <- df_eflalo %>%
    group_by(REF_YEAR, MONTH, strategy_isis, fleet_isis, metier_fleet_isis, metier_isis) %>%
    summarise(effort_metier_isis_total = sum(LE_EFF)) %>%
    left_join(., dfs_navs_per_strategy_input) %>%
    mutate(effort_metier_isis_per_nav = effort_metier_isis_total / n_vessel)
}

###-----------------------------------------------------------------------------
### compute percentage of effort per metier_isis given strategy_isis
dfs_effort_percentage_metier_isis <- left_join(dfs_effort_per_strategy_year_month_metier_isis_input,
                                               dfs_effort_per_strategy_month_input) %>%
  mutate(prop_effort_metier_isis = round(effort_metier_isis_per_nav / effort_strategy_per_nav, 3),
         percentage_effort_metier_isis = prop_effort_metier_isis * 100)


###-----------------------------------------------------------------------------
test <- left_join(dfs_effort_percentage_metier_isis,
                  dfs_ratio_effort_metier_zone_high_low,
                  by = c("metier_fleet_isis",
                         "fleet_isis", "metier_isis")) %>%
  mutate(prop_fishing_time_zone = ifelse(is.na(prop_fishing_time_zone), 1,
                                          prop_fishing_time_zone),
         metier_zone_isis = as.character(metier_zone_isis),
         metier_zone_isis = ifelse(is.na(metier_zone_isis), paste0(metier_isis,"_Zunique"),
                                   metier_zone_isis),
         prop_effort_metier_isis = prop_effort_metier_isis * prop_fishing_time_zone)


dfs_prop_metier_zone_isis <- test %>%
  ungroup() %>%
  select(prop_effort_metier_isis,
         metier_zone_isis,     fleet_isis ,
         strategy_isis, MONTH) %>%
  right_join(., dfs_metier_zone_isis_per_fleet_month) %>%
  mutate(prop_effort_metier_isis = ifelse(is.na(prop_effort_metier_isis), 0,
                                          prop_effort_metier_isis),
         prop_effort_metier_isis = round(prop_effort_metier_isis, digits = 3))

###-----------------------------------------------------------------------------
dir.create(file.path(path_data_tidy,
                     "isis",
                     "str_prop_met"),
           showWarnings = FALSE)

if (erase_coda) {
  map(list.files(file.path(path_data_tidy,
                           "isis",
                           "str_prop_met"),
                 full.names = TRUE),
      file.remove)
}

###-----------------------------------------------------------------------------
dfs_prop_metier_zone_isis %>%
  mutate(fr.ifremer.isisfish.types.Month = as.numeric(as.character(MONTH)) - 1, # mois dois commencer de 0
         Metier = as.character(metier_zone_fleet_isis)) %>%
  split(., ~ strategy_isis) %>%
  iwalk(function(val, id) write_as_semantic_matrix(val,
                                                   filename = file.path(path_data_tidy,
                                                                        "isis", "str_prop_met",
                                                                        paste0(id, ".txt")),
                                                   val_col = "prop_effort_metier_isis",
                                                   var_cols = c("Metier", "fr.ifremer.isisfish.types.Month"),
                                                   complete = TRUE
  ))
