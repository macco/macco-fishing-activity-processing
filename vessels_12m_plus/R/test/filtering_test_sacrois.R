filter(df_sacrois_raw, SEQ_ID %in% c(31312417))

df_sacrois_duplicated <- df_sacrois %>%
  group_by(LE_ID_ESP) %>%
  filter(n() > 1) %>%
  arrange(LE_ID_ESP, SEQ_ID)
dim(df_sacrois_duplicated)

df_sacrois_duplicated_SIPA_CAPTURES <- df_sacrois_duplicated %>%
  filter(ORIGINE_ESP_COD_FAO == "SIPA_CAPTURES") %>%
  group_by(LE_ID_ESP) %>%
  filter(n() > 1)

dim(df_sacrois_duplicated_SIPA_CAPTURES)

test = df_sacrois_duplicated[c(3,4),]
filter(df_sacrois_raw, SEQ_ID %in% test$SEQ_ID) %>%
  arrange(ESP_COD_FAO)
