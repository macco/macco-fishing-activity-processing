if (file.exists(paste0(path_sacrois_data_tidy, "/df_sacrois_", y, ".rds")) &
    load_raw_data == FALSE) {

  cat("Year:", y,
      "- sacrois rds file already generated and available at",
      path_sacrois_data_tidy, "\n")

  df_sacrois <- read_rds(paste0(path_sacrois_data_tidy,
                                "/df_sacrois_",
                                y, ".rds"))

} else {

  cat("Year:", y, "- load sacrois raw data \n")

  ###-----------------------------------------------------------------------
  ### load sacrois data per year
  load(paste0(path_sacrois_data_raw, '/',
              sacrois_data_file[str_which(sacrois_data_file,
                                          as.character(y))]))

  ###-----------------------------------------------------------------------
  ### tidy data
  df_sacrois_raw <- get(paste0("NAVIRES_MOIS_MAREES_JOUR_IFR_", y)) %>%
    filter(### and filter by country and ices zones
      PAVILLON == "FRA",
      SECT_COD_SACROIS_NIV3 %in% ices_zones) %>%
    mutate(SEQ_ID = factor(SEQ_ID))

  df_sacrois <- df_sacrois_raw %>%
    ### select columns
    dplyr::select(MAREE_ID,
                  ESP_COD_FAO,
                  NAVS_COD,
                  ENGIN_COD,
                  PAVILLON,
                  MAREE_DATE_DEP,
                  MAREE_DATE_RET,
                  SEQ_ID,
                  DATE_SEQ,
                  SECT_COD_SACROIS_NIV5,
                  SECT_COD_SACROIS_NIV3,
                  TP_NAVIRE_SACROIS,
                  TPS_MER,
                  QUANT_POIDS_VIF_SACROIS,
                  MONTANT_EUROS_SACROIS,
                  METIER_DCF_6_COD,
                  METIER_DCF_5_COD,
                  METIER_COD_SACROIS,
                  ORIGINE_ESP_COD_FAO) %>%
    ### remove empty variables
    filter(NAVS_COD != "",
           DATE_SEQ  != "",
           SECT_COD_SACROIS_NIV5 != "",
           ENGIN_COD != "",
           ESP_COD_FAO != "",
           TP_NAVIRE_SACROIS != "",
           !is.na(TP_NAVIRE_SACROIS),
           TPS_MER != "",
           !is.na(TPS_MER),
           !is.na(QUANT_POIDS_VIF_SACROIS),
           !is.na(MONTANT_EUROS_SACROIS))

  ###---------------------------------------------------------------------------
  ### Number of rows removes because of NAs
  diag_sacrois_filtered_NA <- list(df_diag = data.frame("year" = y,
                                                        "n_rows_removed" = dim(df_sacrois_raw)[1] - dim(df_sacrois)[1],
                                                        "percentage_removed" = round((1 - dim(df_sacrois)[1] / dim(df_sacrois_raw)[1]) * 100, digits = 1)),
                                   "seq_id_rows_removed" = anti_join(df_sacrois_raw, df_sacrois) %>% pull(SEQ_ID))
  saveRDS(diag_sacrois_filtered_NA,
          file = paste0(path_diag_data_tidy,
                        "/diag_sacrois_filtered_NA_", y, ".rds"))

  ###---------------------------------------------------------------------------
  ### rename columns to match eflalo format
  df_sacrois <-  df_sacrois %>%
    rename(VE_REF = NAVS_COD,
           VE_FLT = ENGIN_COD,
           VE_COU = PAVILLON,
           LE_DAT = DATE_SEQ,
           LE_RECT = SECT_COD_SACROIS_NIV5,
           LE_DIV = SECT_COD_SACROIS_NIV3,
           LE_KG = QUANT_POIDS_VIF_SACROIS,
           LE_EURO = MONTANT_EUROS_SACROIS) %>%
    mutate(LE_FISHING_TIME = TP_NAVIRE_SACROIS,
           LE_TIME_AT_SEA = TPS_MER) %>%
    rename_at(all_of(LE_EFF_ORIGIN), ~ "LE_EFF") %>%
    ### create variable to match eflalo format
    mutate(SEQ_ID = factor(SEQ_ID),
           VE_REF = factor(VE_REF),
           LE_RECT = factor(LE_RECT),
           LE_DIV = factor(LE_DIV),
           METIER_DCF_6_COD = factor(METIER_DCF_6_COD),
           METIER_DCF_5_COD = factor(METIER_DCF_5_COD),
           METIER_COD_SACROIS = factor(METIER_COD_SACROIS),
           ESP_COD_FAO = factor(ESP_COD_FAO),
           FT_LDAT = lubridate::dmy_hms(MAREE_DATE_RET),
           LE_DAT = lubridate::dmy_hms(LE_DAT),
           MAREE_DATE_DEP = dmy_hms(MAREE_DATE_DEP),
           MAREE_DATE_RET = dmy_hms(MAREE_DATE_RET),
           MONTH = lubridate::month(FT_LDAT),
           REF_YEAR = lubridate::year(FT_LDAT),
           LE_ID = factor(paste(VE_REF, SEQ_ID, date(FT_LDAT), LE_RECT, VE_FLT, sep = '-')),
           LE_ID_EFF = factor(paste(LE_ID, LE_EFF, sep = '-')),
           LE_ID_ESP = factor(paste(LE_ID, LE_EFF, ESP_COD_FAO, sep = '-'))) %>%
    droplevels() %>%
    select(-any_of(c("TPS_MER", "TP_NAVIRE_SACROIS")))

  ###---------------------------------------------------------------------------
  ### remove duplicated rows
  df_sacrois_distinct <- df_sacrois %>%
    group_by(LE_ID_ESP) %>%
    arrange(LE_ID_ESP, ESP_COD_FAO, desc(ORIGINE_ESP_COD_FAO), LE_KG, LE_EURO) %>%
    distinct(LE_ID_ESP, .keep_all = TRUE)

  ### Number of rows removes because of NAs
  diag_sacrois_duplicated_rows <- list(
    "seq_id_rows_removed" = anti_join(df_sacrois_distinct, df_sacrois,
                                      by = c("SEQ_ID")) %>% pull(SEQ_ID),
    df_diag = data.frame("year" = y,
                         "n_rows_removed" = dim(df_sacrois_distinct)[1] - dim(df_sacrois)[1],
                         "percentage_removed" = round((1 - dim(df_sacrois_distinct)[1] / dim(df_sacrois)[1]) * 100,
                                                      digits = 2)))

  saveRDS(diag_sacrois_duplicated_rows,
          file = paste0(path_diag_data_tidy,
                        "/diag_sacrois_duplicated_rows_", y, ".rds"))


  df_sacrois <- df_sacrois_distinct %>%
    ungroup()

  ###---------------------------------------------------------------------------
  ### remove rows with a time at sea superior to threshold_maree_duration
  df_sacrois_fishing_time_outlier <- df_sacrois %>%
    filter(LE_TIME_AT_SEA < threshold_maree_duration &
             threshold_min_fishing_time <= LE_FISHING_TIME)

  ### Number of rows removes because of NAs
  diag_sacrois_fishing_time_outlier <- list(
    "seq_id_rows_removed" = anti_join(df_sacrois_fishing_time_outlier, df_sacrois,
                                      by = c("SEQ_ID")) %>% pull(SEQ_ID),
    df_diag = data.frame("year" = y,
                         "n_rows_removed" = dim(df_sacrois_fishing_time_outlier)[1] - dim(df_sacrois)[1],
                         "percentage_removed" = round((1 - dim(df_sacrois_fishing_time_outlier)[1] / dim(df_sacrois)[1]) * 100,
                                                      digits = 1)))

  saveRDS(diag_sacrois_fishing_time_outlier,
          file = paste0(path_diag_data_tidy,
                        "/diag_fishing_time_outlier_", y, ".rds"))


  df_sacrois <- df_sacrois_fishing_time_outlier %>%
    ungroup()

  df_sacrois <- left_join(df_sacrois, df_ref_nav,
                          by = c("REF_YEAR"  = "REF_YEAR",
                                 "VE_REF" = "NAVS_COD"))
  ###---------------------------------------------------------------------------
  if (select_vessel_size == "superior_to_12m") {

    vessel_class_to_keep <- c("[12-15[ m",
                              "[15-18[ m",
                              "[18-24[ m",
                              "[24-40[ m",
                              "[40-80[ m",
                              "[80-Inf[ m")

    df_sacrois <- df_sacrois %>%
      filter((NAVLC4_COD %in% vessel_class_to_keep))
  }

  ###---------------------------------------------------------------------------
  if (select_vessel_size == "less_than_12m") {

    vessel_class_to_keep <- c("[0-7[ m",
                              "[7-10[ m",
                              "[10-12[ m")

    df_sacrois <- df_sacrois %>%
      filter((NAVLC4_COD %in% vessel_class_to_keep))
  }

  ### save df_sacrois in rds format
  saveRDS(df_sacrois,
          file = paste0(path_sacrois_data_tidy, "/df_sacrois_", y, ".rds"))

  ### remove sacrois objects from current year
  rm(list = ls(pattern = y))

  ### remove df_sacrois_raw from current year
  rm(list = c("df_sacrois_raw",
              "df_sacrois_distinct",
              "diag_sacrois_filtered_NA",
              "diag_sacrois_duplicated_rows",
              "diag_sacrois_fishing_time_outlier"))
  gc()
}

df_sacrois_all_year <- bind_rows(df_sacrois_all_year, df_sacrois)
