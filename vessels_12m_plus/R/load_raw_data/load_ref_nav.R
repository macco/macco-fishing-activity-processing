################################################################################
### Load reference navires data
################################################################################

if (file.exists(paste0(path_fishery_data_tidy_vessels,
                       "/df_ref_nav_",
                       min(year_vec), "_",
                       max(year_vec), ".rds")) &
    load_raw_data == FALSE) {

  cat(bgBlue("Load reference navire data \n"))

  df_ref_nav <- read_rds(paste0(path_fishery_data_tidy_vessels,
                                "/df_ref_nav_",
                                min(year_vec), "_",
                                max(year_vec), ".rds"))
} else {

  cat(bgBlue("Generate reference navire data \n"))

  ### read all available table at once
  df_ref_nav <- list.files(path = paste0(path_reftables_vessel_data, '/'),
                           pattern = ".txt") %>%
    .[str_starts(., paste0(year_vec, collapse = "|"))] %>%
    map_df( ~ read_csv2(file.path(paste0(path_reftables_vessel_data), .),
                       # header = TRUE,
                       locale = readr::locale(encoding = 'WINDOWS-1252'),
                       # fileEncoding = "ISO-8859-1",
                       # sep = ";"
                       skip = 0)) %>%
    dplyr::select(NAVS_COD,
                  S_FLOTTILLE_IFREMER_LIB,
                  S_FLOTTILLE_IFREMER_COD,
                  DATE_REF, QAM_COD, QAM_LIB,
                  NAVLC4_COD, NAVLC9_LIB, PORT_EXPL_ACT_LIB) %>%
    mutate(across(where(is.character), ~ str_replace_all(., "é", "e")),
           across(where(is.character), ~ str_replace_all(., "è", "e")),
           QAM_COD = ifelse(QAM_LIB == "Nantes", "NANT", QAM_COD),
           DATE_REF = dmy(DATE_REF),
           REF_YEAR = year(DATE_REF),
           NAVS_COD = factor(NAVS_COD),
           NAVLC4_COD = str_replace(NAVLC4_COD, "< 6 m", "[0-6[ m"),
           NAVLC4_COD = str_replace(NAVLC4_COD, ">= 80 m", "[80-Inf[ m"),
           PortNAVLC = paste(QAM_LIB, NAVLC4_COD))

  if (select_vessel_size == "superior_to_12m") {

    vessel_class_to_keep <- c("[12-15[ m",
                              "[15-18[ m",
                              "[18-24[ m",
                              "[24-40[ m",
                              "[40-80[ m",
                              "[80-Inf[ m")

    df_ref_nav <- df_ref_nav %>%
      filter((NAVLC4_COD %in% vessel_class_to_keep))
  }


  if (file.exists(paste0(path_fishery_data_tidy_vessels, "/df_SUPER_QAM.rds"))) {
    df_SUPER_QAM <- read_rds(paste0( path_fishery_data_tidy_vessels,
                                     "/df_SUPER_QAM.rds"))
  } else {

    df_SUPER_QAM <- data.frame(
      SUPER_QAM_LIB_id = c(3, 3, 1, 2, 3, 2, 2, 2, 1, 1, 3, 2, 2, 3, 2, 3, 3, 3,
                           1, 1, 1, 1, 1, 1, 2, 2, 2, 1, 1, 2, 2, 4, 4, 4, 4),
      QAM_LIB = c("Bayonne", "Marennes", "Paimpol", "Guilvinec", "La Rochelle",
                  "Vannes", "Audierne", "Auray", "Saint-Brieuc", "Cherbourg",
                  "Ile d'Oleron", "Lorient", "Concarneau", "L'Ile-d'Yeu",
                  "Saint-Nazaire", "Bordeaux", "Les Sables-d'Olonne", "Arcachon",
                  "Morlaix", "Dieppe", "Saint-Malo", "Caen", "Boulogne-sur-mer",
                  "Fecamp", "Brest", "Camaret", "Douarnenez", "Dunkerque",
                  "Le Havre", "Nantes", "Noirmoutier",
                  "Sete", "Port-Vendres", "Martigues", "Marseille")) %>%
      mutate(SUPER_QAM_LIB = factor(SUPER_QAM_LIB_id,
                                    labels = c("channel",
                                               "bob_north",
                                               "bob_south",
                                               "medit")))

    saveRDS(df_SUPER_QAM,
            file = paste0(path_fishery_data_tidy_vessels, "/df_SUPER_QAM.rds"))
  }

  ### merge with reference navire table
  df_ref_nav <- left_join(df_ref_nav, df_SUPER_QAM, by = "QAM_LIB") %>%
    mutate(SUPER_QAM_LONG = factor(paste0(SUPER_QAM_LIB, NAVLC4_COD))) %>%
    distinct()

  saveRDS(df_ref_nav, file = paste0(path_fishery_data_tidy_vessels,
                                    "/df_ref_nav_",
                                    min(year_vec), "_",
                                    max(year_vec), ".rds"))
}
