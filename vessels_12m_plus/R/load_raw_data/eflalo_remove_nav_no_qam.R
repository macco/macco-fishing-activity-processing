df_eflalo_nav_no_QAM <- df_eflalo_all_year %>%
  dplyr::select(VE_REF, REF_YEAR, SUPER_QAM_LIB) %>%
  filter(is.na(SUPER_QAM_LIB)) %>%
  dplyr::select(-SUPER_QAM_LIB) %>%
  distinct()

diag_eflalo_vessel_removed <- list("vessel_removed_no_QAM" = df_eflalo_nav_no_QAM)

saveRDS(diag_eflalo_vessel_removed,
        file = paste0(path_diag_data_tidy,
                      "/diag_eflalo_gear_removed.rds"))


df_eflalo_all_year <- anti_join(df_eflalo_all_year,
                                df_eflalo_nav_no_QAM,
                                by = c("VE_REF", "REF_YEAR")) %>%
  droplevels()

rm(df_eflalo_nav_no_QAM, diag_eflalo_vessel_removed)
