################################################################################
### Load ices rectangle and areas
### downloaded from https://gis.ices.dk/shapefiles/ICES_rectangles.zip
### https://gis.ices.dk/shapefiles/ICES_areas.zip
################################################################################

###-----------------------------------------------------------------------------
### load ices rectangle
sf_ices_rec <- st_read(here(path_data_raw, "ref_spatial/ices_rectangles/ICES_Statistical_Rectangles_Eco.shp"))
st_crs(sf_ices_rec) <- 4326
sf_ices_rec <- sf_ices_rec %>%
  filter(#Ecoregion %in% ices_ecoregion,
    WEST >= lon_lim[1],
    EAST  <= lon_lim[2],
    SOUTH >= lat_lim[1],
    NORTH <= lat_lim[2]) %>%
  dplyr::select(ICESNAME, AREA)

###-----------------------------------------------------------------------------
### load ices sous-rectangle #NEW
sf_ices_ss_rec <- st_read(here(path_data_raw, "ref_spatial/114-SOUS_RECTANGLE_STATISTIQUE/IFR_L_SOUS_RECTANGLE_STATISTIQUE_LOC_LEVEL_114.shp"))
st_crs (sf_ices_ss_rec) <-  4326
sf_ices_ss_rec <- sf_ices_ss_rec %>%
  rename(ICESNAME =  LABEL,
         REGION = NAME)

###-----------------------------------------------------------------------------
### load areas
sf_ices_areas <- st_read(here(path_data_raw,"ref_spatial/ices_areas/ICES_Areas_20160601_cut_dense_3857.shp"))
st_crs(sf_ices_areas) <-  4326
