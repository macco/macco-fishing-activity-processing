################################################################################
### Definition metier_iterator for metier analysis
################################################################################

###-----------------------------------------------------------------------------
### create a metier_iterator variable based on gear_isis
df_eflalo <- df_eflalo %>%
  mutate(metier_iterator = fct_infreq(gear_isis))


if (clustering_per_year) {

  df_eflalo <- df_eflalo %>%
    mutate(metier_iterator = factor(paste0(metier_iterator, "_", REF_YEAR)))  %>%
    droplevels()

}

cat("Iterators used for metier analysis",
    levels(df_eflalo$metier_iterator), "\n")
