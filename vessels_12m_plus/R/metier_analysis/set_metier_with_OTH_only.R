################################################################################
### Set metier to other when metier is only catching OTH species
################################################################################

if (exists("key_species")) {
  ###---------------------------------------------------------------------------
  ### create an aggregated columns summing all no key species
  if (super_other_species & !is.null(key_species)) {

    ###-------------------------------------------------------------------------
    ### extract key and other species
    source("R/species_analysis/set_species_key_and_other.R")

    df_eflalo <- df_eflalo %>%
      mutate(metier = factor(ifelse(rowSums(across(all_of(eflalo_col_sp_key_kg))) == 0,
                             "metier_OTH",
                             as.character(metier))))

  }
}
