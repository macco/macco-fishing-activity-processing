################################################################################
### Plot clusters computed for all years and associated cluster at a year level
################################################################################
source("R/fun/f_metier_compute_dist_cluster_year.R")

## reference dataset
df_cluster_metier_ref_all_years <- df_cluster_metier_landing_cut_all_years %>%
  mutate(REF_YEAR = paste(range(year_vec), collapse = "-")) %>%
  left_join(., df_metier_landing_wider_all_years_clust,
            by = "metier")

### Apply function to all years and bind results in one data frame
df_metier_cluster_distance <- df_metier_landing_wider_clust %>%
  group_by(REF_YEAR) %>%
  group_split() %>%
  map(.x = ., .f = f_metier_compute_dist_cluster_year,
      df_data_ref = df_cluster_metier_ref_all_years) %>%
  bind_rows(.) %>%
  bind_rows(., dplyr::select(df_cluster_metier_ref_all_years, metier, cluster_metier,  REF_YEAR)) %>%
  mutate(REF_YEAR = fct_relevel(REF_YEAR, paste(range(year_vec), collapse = "-"), after = 0))

### extract clusters based on mean landings over the whole period
df_metier_clustering_comparison_mean <- df_cluster_metier_ref_all_years %>%
  rename(cluster_metier_mean = cluster_metier) %>%
  select(-REF_YEAR)

### merge for plotting
df_metier_cluster_distance <- left_join(df_metier_cluster_distance,
                                        select(df_metier_clustering_comparison_mean,
                                               metier, cluster_metier_mean),
                                        by = "metier")

### make plot
plot_metier_landing_cluster_consistency <- ggplot(df_metier_cluster_distance,
                                                  aes(x = REF_YEAR,
                                                      y = reorder(metier, as.numeric(cluster_metier_mean)),
                                                      fill = cluster_metier)) +
  geom_tile() +
  ylab(metier_analysis) +
  xlab("Year") +
  theme(axis.text.x = element_text(angle = 70, vjust = 1, hjust = 1, size = 10),
        # axis.text.y = element_blank(),
        axis.title = element_text(size = 12),
        legend.position = "none") +
  xlab("Year") +
  ylab("Cluster metier")

if (show_plot) {
  print(plot_metier_landing_cluster_consistency)
}

f_save_plot(plot_metier_landing_cluster_consistency,
            file_name = paste0("plot_metier_landing_cluster_consistency"),
            path = path_figure,
            save = save_plot)

###-----------------------------------------------------------------------------
### make plot
plot_metier_landing_cluster_consistency_trans <- ggplot(df_metier_cluster_distance,
                                                  aes(x = reorder(metier, as.numeric(cluster_metier_mean)),
                                                      y = REF_YEAR,
                                                      fill = cluster_metier)) +
  geom_tile() +
  xlab(metier_analysis) +
  ylab("Year") +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust = 1, size = 10),
        # axis.text.y = element_blank(),
        axis.title = element_text(size = 12),
        legend.position = "none") +
  ylab("Year") +
  xlab("Cluster metier")


if (show_plot) {
  print(plot_metier_landing_cluster_consistency_trans)
}

f_save_plot(plot_metier_landing_cluster_consistency_trans,
            file_name = paste0("plot_metier_landing_cluster_consistency_trans"),
            path = path_figure,
            save = save_plot)
