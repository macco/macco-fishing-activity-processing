################################################################################
### Sum all non-key species into one super other (OTH) species
################################################################################

if (exists("key_species")) {
  ###---------------------------------------------------------------------------
  ### create an aggregated columns summing all no key species
  if (super_other_species & !is.null(key_species)) {

    ###-------------------------------------------------------------------------
    ### extract key and other species
    source("R/species_analysis/set_species_key_and_other.R")

    saveRDS(df_eflalo, file = here(path_eflalo_data,
                                     "df_eflalo_all_species.rds"))

    df_eflalo <- df_eflalo %>%
      mutate(LE_KG_OTH = rowSums(across(all_of(eflalo_col_sp_other_kg))),
             LE_EURO_OTH = rowSums(across(all_of(eflalo_col_sp_other_euro)))) %>%
      dplyr::select(-one_of(eflalo_col_sp_other))
  }
}
