####
## export Target factors
## using isis semantics
## required a correspondance table between metier_isis and metier_zone_fleet_isis_import
## for all metiers (including metier_oth)
## and all fleets (sup et inf 12m foreign)
####

### estimated target factors with all fleets caughting key_species
df_target <- readRDS(here(pathtidydataintercatchsacrois,"df_target.rds"))


####table metier_isis - esp_capturables- landingsYearMean
TableEspCaptMetierISIS <-readRDS(paste0(pathtidydataintercatchsacrois,"/df_TableEspCaptMetierISIS.rds"))

### Mise à 1 des coef non estimés (contrastes : valeur de ref fixée à 0 (exp(0)=1 )
df_target_comp <- df_target %>% select(estimate,ESP_COD_FAO,metier_isis,term) %>% distinct() %>%
  left_join(TableEspCaptMetierISIS,by=c("ESP_COD_FAO","metier_isis")) %>%
  mutate(estimateWithRef= if_else(is.na(estimate)&!is.na(landingsYearMean),1,estimate)) %>%
  select(term,metier_isis,ESP_COD_FAO,estimateWithRef) %>% distinct()

 # Metier <- df_target %>%
 #   select(metier_isis) %>%  mutate(metier_isis=as.character(metier_isis)) %>%
 #   distinct()
#
# nbMetiers <- Metier %>% summarise(nbMetiers=n()) %>% as.numeric()
#
# Population <- df_target %>%
#   select(ESP_COD_FAO) %>%
#   mutate(ESP_COD_FAO =as.character(ESP_COD_FAO))  %>%
#   distinct() %>% as.vector()
#
# nbPopulations <- Population %>% summarise(nbPopulations=n())

key_species_latin <- c("Solea_solea", "Merluccius_merluccius", "Nephrops_norvegicus",
                       "Lepidorhombus_whiffiagonis", "Lophius_piscatorius",
                      "Raja_clavata","Leucoraja_naevus")

if (file.exists(paste0(pathtidydataisis,
                       "/corresp_metier_isis_zone_sansother.rds")) ) {

  corresp_metier_isis_zone_sansother <- readRDS(paste0(pathtidydataisis,
                                                       "/corresp_metier_isis_zone_sansother.rds"))

} else {


###Extend df_target to all metier_zone_fleet_isis_import (including metier_OTH not included in the model for target factors estimation)

###Correspondance table metier_isis and metier_zone_fleet_isis_import
###plus de 12m

df_vms_metier_zone_isis <- readr::read_rds(here(path_fishery_data_tidy,
                                                "/vms_processed/",
                                                "df_vms_metier_zone_isis.rds")) %>% rename(LE_RECT = ICESNAME)
corresp_metier_plus12m <- df_vms_metier_zone_isis %>%select(metier_isis,fleet_isis,metier_zone_fleet_isis) %>% distinct()
###moins de 12m
df_sacrois_key_species_ag_foreign_final <- readRDS(paste0(pathtidydataintercatchsacrois,"/df_sacrois_key_species_ag_foreign_final.rds"))
temp_allfleet_red_df_sacrois_key_species_ag_foreign_final <- df_sacrois_key_species_ag_foreign_final %>%
  select(metier_isis,fleet_isis,fleetLength) %>%
  distinct() %>%
  filter((fleet_isis != "BE-24-40")&(fleet_isis != "ES-24-40")&(fleet_isis != "ES-18-24")&(fleet_isis != "ES-0-10")&(fleet_isis != "ES-40-80")&(fleet_isis != "UK-18-24")&(fleet_isis != "UK-24-40")&(fleet_isis != "UK-40-80"))

###association metier_isis (du modele) et metier_zone_fleet_isis_import (pour importer dans isis) plus de 12 et moins de 12
corresp_metier_isis_zone_sup12inf12FRA_avecanciensnoms <- left_join(temp_allfleet_red_df_sacrois_key_species_ag_foreign_final,
                                         corresp_metier_plus12m,by = c("metier_isis","fleet_isis")) %>%
  mutate(metier_zone_fleet_isis = fct_explicit_na(metier_zone_fleet_isis),
         metier_zone_fleet_isis_missing = metier_zone_fleet_isis,
         metier_zone_fleet_isis = case_when((metier_zone_fleet_isis_missing == "(Missing)") ~
                                              glue("{metier_isis}_Zunique @ {fleet_isis}"),
                                            TRUE ~ as.character(metier_zone_fleet_isis)),
         metier_zone_fleet_isis = factor(metier_zone_fleet_isis),
         metier_zone_isis = sub(" .*", "", metier_zone_fleet_isis),
         metier_zone_fleet_isis_import = case_when((fleetLength != "12-15")&(fleetLength != "0-10")&(fleetLength != "10-12") ~
                                                     paste(metier_zone_isis, "15-Inf", sep = " @ "),
                                                   TRUE ~  paste(metier_zone_isis, fleet_isis, sep = " @ ")),
         metier_zone_fleet_isis_import = factor(metier_zone_fleet_isis_import)) %>%
  select(metier_isis,metier_zone_fleet_isis_import,metier_zone_fleet_isis) %>% distinct()

saveRDS(corresp_metier_isis_zone_sup12inf12FRA_avecanciensnoms,
        file = paste0(pathtidydataisis,"/corresp_metier_isis_zone_sup12inf12FRA_avecanciensnoms.rds"))

corresp_metier_isis_zone_sup12inf12FRA <-  corresp_metier_isis_zone_sup12inf12FRA_avecanciensnoms %>%
  select(metier_isis,metier_zone_fleet_isis_import) %>% distinct()


###foreign fleets
Mixfish_BoB_landingsEffortForeign8ab_BEESUK_isis <-read_rds(paste0(pathtidydataintercatch,"/Mixfish_BoB_landingsEffortForeign8ab_BEESUK_isis.rds"))

corresp_metier_isis_zone_foreign <- Mixfish_BoB_landingsEffortForeign8ab_BEESUK_isis %>%
  select(fleet_isis,metier_isis,metier_zone_isis)%>%
  distinct()%>%
  mutate(metier_zone_fleet_isis_import=metier_zone_isis,
         metier_zone_fleet_isis_import = factor(metier_zone_fleet_isis_import)) %>%
  select(metier_isis,metier_zone_fleet_isis_import) %>% distinct()
saveRDS(corresp_metier_isis_zone_foreign,
        file = paste0(pathtidydataisis,"/corresp_metier_isis_zone_foreign.rds"))

###regroup all sans other
corresp_metier_isis_zone_sansother <- bind_rows(corresp_metier_isis_zone_foreign,corresp_metier_isis_zone_sup12inf12FRA)

saveRDS(corresp_metier_isis_zone_sansother,
        file = paste0(pathtidydataisis,"/corresp_metier_isis_zone_sansother.rds"))
###metier_oth
df_MetiersOTH <-  read_rds(file = paste0(pathtidydataintercatchsacrois,"/df_MetiersOTH.rds"))
corresp_metier_isis_zone_other <- df_MetiersOTH %>% select(metier_isis,metier_zone_fleet_isis_import)%>% distinct()
saveRDS(corresp_metier_isis_zone_other,
        file = here("data/tidy/bob_macco/isis/corresp_metier_isis_zone_other.rds"))

###regroup all
corresp_metier_isis_zone_all  <- bind_rows(corresp_metier_isis_zone_sansother,corresp_metier_isis_zone_other)
saveRDS(corresp_metier_isis_zone_all,
        file = paste0(pathtidydataisis,"/corresp_metier_isis_zone_all.rds"))

}

##etendre la table des estimations de target facteur des metier_isis pour tous les metiers dans isis sauf other (metier_zone_fleet_isis_import)
corresp_metier_isis_zone_sansother <- readRDS(paste0(pathtidydataisis,"/corresp_metier_isis_zone_sansother.rds"))
#df_target_extended <- left_join(corresp_metier_isis_zone_sansother,df_target,by="metier_isis") %>%
#select(metier_zone_fleet_isis_import,ESP_COD_FAO,estimate) %>%
df_target_extended <- left_join(corresp_metier_isis_zone_sansother,df_target_comp,by="metier_isis") %>%
  select(metier_zone_fleet_isis_import,ESP_COD_FAO,estimateWithRef) %>%
  mutate(ESP_COD_FAO = factor(ESP_COD_FAO),
  ESP_COD_FAO = fct_recode(ESP_COD_FAO,
                           "Solea_solea" ="SOL", "Merluccius_merluccius"="HKE", "Nephrops_norvegicus"="NEP",
                           "Lepidorhombus_whiffiagonis"="MEG", "Lophius_piscatorius"="MNZ",
                           "Raja_clavata"="RJC","Leucoraja_naevus"="RJN")) %>%
  mutate(estimate = replace_na(estimateWithRef,0))

saveRDS(df_target_extended,
        file = paste0(pathtidydataisis,"/df_target_extendedWithRef.rds"))

#### add metier_OTH with target factor = 0
#ajout des metiers metier_oth

corresp_metier_isis_zone_other <- readRDS(here("data/tidy/bob_macco/isis/corresp_metier_isis_zone_other.rds"))

df1 <- corresp_metier_isis_zone_other %>% mutate(jv=1)
df2 <- tibble(key_species_latin) %>% transmute(ESP_COD_FAO=key_species_latin) %>% mutate(jv=1)
df_other <- full_join(df1,df2) %>% select(metier_zone_fleet_isis_import,ESP_COD_FAO) %>% mutate(estimate=0)

df_target_extended <- read_rds(file = paste0(pathtidydataisis,"/df_target_extendedWithRef.rds"))
df_target_extended_full <- bind_rows(df_other,df_target_extended)
df_target_extended_full <- df_target_extended_full %>% transmute(Metier=metier_zone_fleet_isis_import,Species=ESP_COD_FAO,Val=estimate)

saveRDS(df_target_extended_full,
        file = paste0(pathtidydataisis,"/df_target_extended_fullWithRef.rds"))

#Appliquer
source(here("R/export_isis/isis_semantic_matrix.R"))
source(here("R/export_isis/utils_misc.R"))
write_as_semantic_matrix(df_target_extended_full, filename=paste0(pathtidydataisis,"/myTFfileWithRef.txt"), val_col = NULL, var_cols = NULL, complete = TRUE, encoding = "UTF-8")


