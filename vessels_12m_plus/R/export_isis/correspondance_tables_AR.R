################################################################################
#
# This is  a short script to format data to be read by the java Importation script
# in ISIS for French and foreigners fleets
#
# Author : Antoine Ricouard
#
################################################################################


# french fleets >12m
dataFRAsup12 <- readRDS("ImportsMetiers/FlotillesFRA_sup12/dfs_eflalo_metier_isis_zone_SM2010_2018.rds")

metier_zone_fleet_isis_import <- dataFRAsup12$metier_zone_fleet_isis_import
write.table(metier_zone_fleet_isis_import,
            file = 'ImportsMetiers/FlotillesFRA_sup12/metier_zone_fleet_isis_import.csv',
            row.names = F,
            col.names = F,
            quote=F)


metier_zone_fleet_isis <- dataFRAsup12$metier_zone_fleet_isis
write.table(metier_zone_fleet_isis,
            file = 'ImportsMetiers/FlotillesFRA_sup12/metier_zone_fleet_isis.csv',
            row.names = F,
            col.names = F,
            quote=F)

# non-french fleets
dataETRG <- readRDS("ImportsMetiers/FlotillesETRG/Mixfish_BoB_landingsEffortForeign8ab_BEESUK_isis.rds")

metier_zone_isis_etrg<- dataETRG$metier_zone_isis
write.table(metier_zone_isis_etrg,
            file = 'ImportsMetiers/FlotillesETRG/metier_zone_isis.csv',
            row.names = F,
            col.names = F,
            quote=F)

metier_zone_fleet_isis_etrg <- paste(dataETRG$metier_zone_isis,"@",dataETRG$fleet_isis)
write.table(metier_zone_fleet_isis_etrg,
            file = 'ImportsMetiers/corresp_tables/FlotillesETRG/metier_zone_fleet_isis.csv',
            row.names = F,
            col.names = F,
            quote=F)
