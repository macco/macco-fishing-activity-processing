####
## export Target factors
## using isis semantics
## requires a correspondance table between metier_isis and metier_zone_fleet_isis_import
## for all metiers (including metier_oth)
## and all fleets (sup et inf 12m foreign)
####
df_import_efficiency <- readRDS("S:/Projets/MACCO/Dynamique Flottilles 12m Sup/macco-fishing-activity-processing/data/tidy/bob_macco/estim_coefficient/df_import_efficiency.rds")

df_efficiency_export <- df_import_efficiency %>%
  select(term_isis,estimate) %>%
  mutate(SetOfVessels=as.character(term_isis)) %>%
  select(SetOfVessels,estimate) %>%
  mutate(SetOfVessels=as.factor(SetOfVessels),
         SetOfVessels= recode_factor(SetOfVessels,
                                 "Caseyeurs Métiers de l'hameçon exclusifs-10-12" = "Caseyeurs Métiers de l hameçon exclusifs-10-12",
                                 "Fileyeurs Métiers de l'hameçon exclusifs-10-12"="Fileyeurs Métiers de l hameçon exclusifs-10-12"),
         SetOfVessels = as.character(SetOfVessels))


  source(here("R/export_isis/isis_semantic_matrix.R"))
  source(here("R/export_isis/utils_misc.R"))
  write_as_semantic_matrix(df_efficiency_export, filename=paste0(pathtidydataisis,"/myEfficiencyfile.txt"), val_col = NULL, var_cols = NULL, complete = TRUE, encoding = "UTF-8")
