metier6_to_isis_flow <- function(corresp_tab){

  lab_size <- c(rep(0,length(unique(corresp_tab$METIER_DCF_6_COD))),
                rep(3,length(unique(corresp_tab$metier_isis))))
  lab_color <- c(rep("white",length(unique(corresp_tab$METIER_DCF_6_COD))),
                 rep("black",length(unique(corresp_tab$metier_isis))))

  fig <- ggplot(data = corresp_tab,
                aes(axis1 = METIER_DCF_6_COD, axis2 = metier_isis,y=1)) +
    geom_alluvium(aes(fill = METIER_DCF_6_COD)) +
    geom_stratum() +
    geom_text(stat = "stratum",
              aes(label = after_stat(stratum))) +
    scale_x_discrete(limits = c("METIER_DCF_6_COD", "metier_isis"),
                   expand = c(0.15, 0.05)) +
    scale_fill_viridis_d()  +
    theme_void() +
    theme(legend.text = element_text(size = 1),
          axis.text.y = element_blank(),
          axis.text.x = element_text(size=6, vjust = 1, hjust=0.2))


  return(fig)
}
metier5_to_isis_flow <- function(corresp_tab){

  lab_size <- c(rep(0,length(unique(corresp_tab$METIER_DCF_5_COD))),
                rep(3,length(unique(corresp_tab$metier_isis))))
  lab_color <- c(rep("white",length(unique(corresp_tab$METIER_DCF_5_COD))),
                 rep("black",length(unique(corresp_tab$metier_isis))))

  fig <- ggplot(data = corresp_tab,
                aes(axis1 = METIER_DCF_5_COD, axis2 = metier_isis,y=1)) +
    geom_alluvium(aes(fill = METIER_DCF_5_COD)) +
    geom_stratum() +
    geom_text(stat = "stratum",
              aes(label = after_stat(stratum))) +
    scale_x_discrete(limits = c("METIER_DCF_5_COD", "metier_isis"),
                     expand = c(0.15, 0.05)) +
    scale_fill_viridis_d()  +
    theme_void() +
    theme(legend.text = element_text(size = 1),
          axis.text.y = element_blank(),
          axis.text.x = element_text(size=6, vjust = 1, hjust=0.2))


  return(fig)
}
