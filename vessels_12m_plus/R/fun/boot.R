################################################################################
### Script that install and load packages as well as project specific function
################################################################################

###-----------------------------------------------------------------------------
### Load packages

### general package
generalPackages <- c("crayon",
                     "import",
                     "data.table",
                     "dtplyr",
                     "lubridate",
                     "here",
                     "remotes",
                     "sf",
                     "maps",
                     "mapdata",
                     "FactoMineR",
                     "glue",
                     "patchwork",
                     "ggdendro",
                     "viridis",
                     "tidytext",
                     "tweedie",
                     "statmod",
                     "ggalluvial",
                     "tidyverse")
#"ggsn","tidyverse", "rlang", "ggfortify","broom",

installed_packages <- generalPackages %in% rownames(installed.packages())
if (any(installed_packages == FALSE)) {
  install.packages(generalPackages[!installed_packages])
}

invisible(lapply(generalPackages, library, character.only = TRUE))

###-----------------------------------------------------------------------------
# Suppress summarise info
options(dplyr.summarise.inform = FALSE)

###-----------------------------------------------------------------------------
### install vmstools and its dependency
source(here("R", "fun", "install_packages.R"))

### project related package
### vmstools and its dependencies
# library("vmstools")

###-----------------------------------------------------------------------------
### source utilities function specific to this project
###-----------------------------------------------------------------------------

### small function
source(here("R", "fun", "utilities.R"))

### complex function to compute metier from vmstools function
source(here("R","fun", "f_get_table_after_PCA_debug.R"))
source(here("R","fun", "f_get_metier_clusters_generic.R"))
source(here("R","fun", "f_color_palette_for_factor.R"))

### function specific to isis: source them all
invisible(sapply(paste0(here("R", "fun", "isis", "/"),
                        list.files(path = here("R", "fun", "isis"))),
                 source))


###-----------------------------------------------------------------------------
### Create and set directories
###-----------------------------------------------------------------------------
source(here("R/fun/make_path.R"))

###-----------------------------------------------------------------------------
### implemented fisheries
implemented_fisheries <- c("bob_sol_hke_nep", "discardless")

###-----------------------------------------------------------------------------
### do not use S2 if maps package is used
sf::sf_use_s2(FALSE)
