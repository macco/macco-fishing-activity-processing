################################################################################
### plot map with cost funtion
################################################################################

COSTeda::spacePlot(statrects = TRUE,
          landmass = TRUE,
          col.coast = "black",
          col.land = "white",
          colour = TRUE,
          TechStrata = "foCatEu5",
          TimeStrata = "year",
          scaleplace = "topleft",
          xlim = c(-6, 3),
          ylim = c(47, 51),
          depth = c(20, 50, 200),
          xlab = "Longitude",
          ylab = "Latitude")

# divnames()
# ices.division.lines(division=c("IVa","IVb","IVc","VIa","VIb","IIa","IIIa"))
