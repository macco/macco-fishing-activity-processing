f_isis_export_inactivity <- function(df_activity_month){

  df_isis_export_inactivity <- df_activity_month %>%
    ungroup() %>%
    select(MONTH, isis_fleet, inactivity_days) %>%
    pivot_wider(names_from = MONTH,
                values_from = inactivity_days,
                id_cols = isis_fleet)

  average_str_name <- df_activity_month %>%
    mutate(isis_fleet = as.character(isis_fleet),
           isis_fleet = str_replace(isis_fleet, "\\s", "_"),
           isis_fleet = str_replace(isis_fleet, ">", "inf"),
           isis_fleet = str_replace(isis_fleet, "<", "sup")) %>%
    pull(isis_fleet) %>%
    unique()

  write.table(df_isis_export_inactivity,
              sep = ";", row.names = FALSE,
              col.names = FALSE,
              file = paste0(path_isis_data, "/average_strategies_",
                            starting_year, "_", ending_year, "/average_inactivity_",
                            average_str_name, ".csv"))

}


# fn_isis_export_inactivity <- function(df_activity_month, var_str){
#
#   df_isis_export_inactivity <- df_activity_month %>%
#     ungroup() %>%
#     select(MONTH, isis_fleet, inactivity_days) %>%
#     pivot_wider(names_from = MONTH,
#                 values_from = inactivity_days,
#                 id_cols = isis_fleet)
#
#   write.table(df_isis_export,
#               sep = ";", row.names = FALSE,
#               col.names = FALSE,
#               file = paste0(path_isis_data, "/AverageStrategies_",
#                             starting_year, "_", ending_year, "/average_inactivity_",
#                             average_str_name, ".csv"))
#
# }
