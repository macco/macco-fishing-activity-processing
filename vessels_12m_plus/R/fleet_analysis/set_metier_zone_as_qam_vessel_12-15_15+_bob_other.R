###-----------------------------------------------------------------------------
### Set fleet as SUPER_QAM_LIB, user can change manually the fleet definition
df_eflalo <- df_eflalo %>%
  mutate(metier_zone = case_when(vessel_type_isis != "12-15" ~
                                   paste(metier_isis, "15-Inf", sep = "-"),
                                 TRUE ~  paste(metier_isis, fleet_isis, sep = "-")),
         metier_zone = factor(metier_zone),
         fleet_metier_zone = case_when(vessel_type_isis != "12-15" ~
                                  paste(harbour_isis, "15-Inf", sep = "-"),
                                TRUE ~  paste(harbour_isis, fleet_isis, sep = "-")),
         metier_zone = fct_relevel(metier_zone, sort))
