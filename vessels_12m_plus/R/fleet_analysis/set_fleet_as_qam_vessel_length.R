###-----------------------------------------------------------------------------
### Set fleet as SUPER_QAM_LIB, user can change manually the fleet definition
df_eflalo <- df_eflalo %>%
  mutate(vessel_type_isis = str_remove(NAVLC4_COD, pattern = "[[:blank:]]m"),
         vessel_type_isis = str_remove_all(vessel_type_isis, pattern = "\\[|\\]"),
         harbour_isis = SUPER_QAM_LIB,
         fleet = factor(paste0(harbour_isis, "-", vessel_type_isis)),
         fleet_isis = as.character(fleet),
         fleet_isis = str_replace(string = fleet_isis,
                                  pattern = "channel",
                                  replacement = "other"),

         fleet_isis = str_replace(string = fleet_isis,
                                  pattern = "medit",
                                  replacement = "other"),
         fleet_isis = factor(fleet_isis))



