
###-----------------------------------------------------------------------------
### extract unique Vessel, year and strategy
df_metier_zone_effort <- df_eflalo_no_OTH %>%
  ungroup() %>%
  group_by(metier_zone, REF_YEAR, MONTH, metier_isis) %>%
  summarise(LE_FISHING_TIME_sum = sum(LE_FISHING_TIME)) %>%
  group_by(metier_zone, metier_isis) %>%
  summarise(LE_FISHING_TIME_mean = mean(LE_FISHING_TIME_sum)) %>%
  ungroup() %>%
  mutate(LE_FISHING_TIME_prop = LE_FISHING_TIME_mean/sum(LE_FISHING_TIME_mean),
         LE_FISHING_TIME_median = median(LE_FISHING_TIME_mean),
         type = ifelse(LE_FISHING_TIME_mean > LE_FISHING_TIME_median,
                       ">median", "<=median"))

###-----------------------------------------------------------------------------
plot_effort_metier_zone_prop_before_cleaning <- ggplot() +
  geom_col(data = df_metier_zone_effort,
           aes(y = fct_reorder(metier_zone, LE_FISHING_TIME_prop),
               x = LE_FISHING_TIME_mean,
               fill = type)) +
  theme(axis.text.x = element_text(angle = 90,hjust = 1, vjust = 0.5)) +
  geom_hline(yintercept = 0.01)

f_save_plot(plot_effort_metier_zone_prop_before_cleaning,
            file_name = "plot_effort_metier_zone_prop_before_cleaning",
            width = 30,
            height = 40,
            path = path_figure,
            save = save_plot)


###-----------------------------------------------------------------------------
plot_effort_metier_zone_prop_infmed_before_cleaning <- ggplot() +
  geom_col(data = filter(df_metier_zone_effort,
                         type == "<=median"),
           aes(y = fct_reorder(metier_zone, LE_FISHING_TIME_mean),
               x = LE_FISHING_TIME_mean,
               fill = metier_isis)) +
  theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5))

f_save_plot(plot_effort_metier_zone_prop_infmed_before_cleaning,
            file_name = "plot_effort_metier_zone_prop_infmed_before_cleaning",
            width = 30,
            height = 40,
            path = path_figure,
            save = save_plot)

###-----------------------------------------------------------------------------
plot_effort_metier_zone_mean_before_cleaning <- ggplot() +
  geom_col(data = df_metier_zone_effort,
           aes(y = metier_zone,
               x = LE_FISHING_TIME_mean,
               fill = metier_zone)) +
  theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5)) +
  scale_fill_manual(values = scale_color_metier_zone)  +
  theme(legend.position = "none")

f_save_plot(plot_effort_metier_zone_mean_before_cleaning,
            file_name = "plot_effort_metier_zone_mean_before_cleaning",
            width = 30,
            height = 40,
            path = path_figure,
            save = save_plot)
