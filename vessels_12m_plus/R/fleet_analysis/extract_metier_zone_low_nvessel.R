###-----------------------------------------------------------------------------
### extract unique Vessel, year and strategy
df_metier_zone_vessel <- df_eflalo_no_OTH %>%
  dplyr::select(VE_REF, metier_zone, REF_YEAR, metier_isis) %>%
  distinct()

###-----------------------------------------------------------------------------
### count number of vessels per strategy and year
dfs_metier_fleet_nvessel_per_year <- df_metier_zone_vessel %>%
  group_by(REF_YEAR, metier_zone, metier_isis) %>%
  summarise(n_vessels = n())


###-----------------------------------------------------------------------------
### count number of vessels per strategy and year
dfs_metier_zone_nvessel <- dfs_metier_fleet_nvessel_per_year %>%
  group_by(metier_zone, metier_isis) %>%
  summarise(n_vessels_mean = mean(n_vessels)) %>%
  ungroup() %>%
  mutate( metier_zone_median = median(n_vessels_mean),
          type = ifelse(n_vessels_mean > metier_zone_median,
                        ">median", "<=median"))

plot_nvessel_metier_zone_before_cleaning <- ggplot() +
  geom_col(data = dfs_metier_zone_nvessel,
           aes(y = metier_zone,
               x = n_vessels_mean,
               fill = metier_zone)) +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5)) +
  scale_fill_manual(values = scale_color_metier_zone) +
  theme(legend.position = "none")


f_save_plot(plot_nvessel_metier_zone_before_cleaning,
            file_name = "plot_nvessel_metier_zone_before_cleaning",
            width = 30,
            height = 40,
            path = path_figure,
            save = save_plot)


plot_nvessel_metier_zone_inf3_before_cleaning <- ggplot() +
  geom_col(data = filter(dfs_metier_zone_nvessel,
                         type == "<=median"),
           aes(y = fct_reorder(metier_zone, n_vessels_mean),
               x = n_vessels_mean,
               fill = metier_isis)) +
  theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5))



f_save_plot(plot_nvessel_metier_zone_inf3_before_cleaning,
            file_name = "plot_nvessel_metier_zone_inf3_before_cleaning",
            width = 30,
            height = 40,
            path = path_figure,
            save = save_plot)

plot_nvessel_metier_zone_sup3_before_cleaning <- ggplot() +
  geom_col(data = filter(dfs_metier_zone_nvessel,
                         type == ">median"),
           aes(y = fct_reorder(metier_zone, n_vessels_mean),
               x = n_vessels_mean,
               fill = metier_isis)) +
  theme(axis.text.x = element_text(angle = 90, vjust = 0.5))


f_save_plot(plot_nvessel_metier_zone_sup3_before_cleaning,
            file_name = "plot_nvessel_metier_zone_sup3_before_cleaning",
            width = 30,
            height = 40,
            path = path_figure,
            save = save_plot)
