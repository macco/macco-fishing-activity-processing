###-----------------------------------------------------------------------------
### Save eflalo with all isis_variable
if (file.exists(paste0(path_eflalo_data, "/df_eflalo_final_isis",
                       min(year_vec), "_",
                       max(year_vec), ".rds")) & erase_coda == FALSE ) {

  df_eflalo <- read_rds(paste0(path_eflalo_data, "/df_eflalo_final_isis",
                               min(year_vec), "_",
                               max(year_vec), ".rds"))

  list_scale_color <- read_rds(paste0(path_fishery_data_tidy_plots, "/list_scale_color.rds"))

} else {

  saveRDS(df_eflalo, paste0(path_eflalo_data, "/df_eflalo_final_isis",
                            min(year_vec), "_",
                            max(year_vec), ".rds"))

  list_scale_color <- list(scale_color_metier_isis = scale_color_metier_isis,
                           scale_color_gear_isis = scale_color_gear_isis,
                           scale_color_fleet_isis = scale_color_fleet_isis,
                           scale_color_strategy_isis = scale_color_strategy_isis,
                           scale_color_species_isis = scale_color_species_isis)

  saveRDS(list_scale_color,
          paste0(path_fishery_data_tidy_plots, "/list_scale_color.rds"))

}
