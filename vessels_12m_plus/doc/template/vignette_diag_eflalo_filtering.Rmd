### Eflalo data filtering

```{r load_eflalo_diag, echo = FALSE, include = FALSE}
diag_eflalo <- read_rds(file = paste0(path_diag_data_tidy,"/diag_eflalo_gear_removed.rds")) 
```

#### List of gears removed from the clustering analysis

```{r gear-removed, echo = FALSE, include = TRUE}
print(sort(diag_eflalo$gear_removed))
```

The number of rows removed by excluding the above gears is `r diag_eflalo$percentage_removed`.
