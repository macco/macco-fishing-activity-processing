
#### Gear and metier filtering

```{r elfalo-gear-filtering, include = FALSE, echo = FALSE}
df_elfalo_gear_filtering <- data.frame(
  variable = c("Threshold of gear proportion in euro of total landings",
               "Filter given gear threshold",
               "super_gear",
               "Threshold of metier proportion in euro of total landings",
               "Filter given metier threshold"),
  value = c(threshold_euro_prop_gear,
            filter_euro_prop_gear,
            super_gear,
            threshold_euro_prop_metier,
            filter_euro_prop_metier),
  `r_name` = c("threshold_euro_prop_gear",
               "filter_euro_prop_gear",
               "super_gear",
               "threshold_euro_prop_metier",
               "filter_euro_prop_metier")
  
)
```

```{r table-eflalo-gear-filtering, include = TRUE, echo = FALSE}
kable(df_elfalo_gear_filtering)
```

