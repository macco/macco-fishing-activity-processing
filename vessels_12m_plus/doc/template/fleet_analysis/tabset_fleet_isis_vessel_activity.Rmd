# Vessels activity per fleet isis and metier isis {.tabset}

```{r expand-fleet-isis-vessel-activity, include = FALSE}

list_plot_fleet_isis_vessel_activity <- read_rds(here(path_data_tidy_plots, "list_plot_fleet_isis_vessel_activity.rds"))

# create a list of raw Rmarkdown code as a list of character vectors, which are then evaluated
fleet_names <- names(list_plot_fleet_isis_vessel_activity)
n_fleets <- length(fleet_names)
plots_fleets <- vector("list", n_fleets) # the list of raw Rmarkdown code


# 1st level tabset ------------------------------------------------------------
for (i in 1:n_fleets) {
  fleet <- names(fleet_names)[i]
  
  plots_fleet_activity <- vector("list", 1 + i) # the lower level list of raw Rmarkdown code
  plots_fleet_activity[[1]] <- knit_expand(text = c( # first element = heading
    "## Fleet isis: {{fleet_names[i]}} {.tabset}\n",
    "```{r, metier-{{fleet_names[i]}}}\n",
    "list_plot_fleet_isis_vessel_activity[[{{i}}]]$plot_metier_isis +
    ggtitle('')",
    "```\n",
    "\n"
  ))
  plots_fleets[[i]] <- unlist(plots_fleet_activity)
}
```

`r knit(text = unlist(plots_fleets))`
