```{r setup, include = FALSE, echo = FALSE}
library("here")
library("knitr")

knitr::opts_chunk$set(collapse = TRUE,
                      include = TRUE,
                      echo = FALSE,
                      warning = FALSE,
                      message = TRUE,
                      comment = "#>")

###-----------------------------------------------------------------------------
### boot r script: load package and function
source(here("R", "fun", "boot.R"),
       local = knitr::knit_global())

### control file for selecting general options
source(here(path_fishery, "0_control_file.R"),
       local = knitr::knit_global())
```
