---
title: "Macco - data"
date: "`r Sys.Date()`"
output: html_document
---

```{r, include = FALSE}
library(here)
fishery_name <- "bob_macco"
```

```{r, child = c(here("doc", "template", "setup.Rmd"))}
```

```{r, child = c(here("doc", "template", "load_tidy_data.Rmd")), cache = TRUE,cache.lazy = FALSE}
```

# Nombre de séquences de pêche par an

```{r, fig.cap="Number of fishing sequence for each year in the final dataset."}
df_nseq_year <- df_eflalo %>%
  select(REF_YEAR, SEQ_ID) %>%
  distinct()
ggplot() +
  geom_histogram(data = df_nseq_year, aes(x = REF_YEAR),
                 stat = "count") +
  ylab("Number of fishing sequences") +
  xlab("Years")
```


# Nombre de métier dcf-6 : `r nlevels(df_eflalo$METIER_DCF_6_COD)`
