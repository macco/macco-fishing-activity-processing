---
title: "General documentation"
subtitle: "Fishing activity analysis workflow"
date: "`r Sys.Date()`"
author: Jean-Baptiste Lecomte
output: html_document
---

# Introduction

The analysis of the fishing activity requires data of effort and catch (or landings) for each vessel ideally on a daily basis. Spatial information is a plus.
The methods developed in this project use a standardized format (eflalo) and sacrois data as the raw data.
Sacrois data can be replace by another source of data, the user will have to transform its data to the eflalo format to use the methods and scripts developped in this project. 

# Data loading and formating

## Sacrois data

The sacrois format (<https://wwz.ifremer.fr/sih_eng/Debarquements-effort-de-peche/Sacrois>) is structured as a "long-table".
A row corresponds to a fishing sequence and landings of a species with associated effort, spatial information and more.
It means that for a fishing sequence there are a row for each species caught during this fishing sequence.

|VE_REF|VE_FLT|SEQ_ID|LE_DAT|ESP_COD_FAO|QUANT_POIDS_VIF_SACROIS|
|---|---|---|---|---|---|---|---|
|110852|OTB|19165859|2010-06-16|  HKE	| 100 |
|110852|OTB|19165859|2010-06-16|  CET	|   0 |  
|212578|GTR|12484689|2010-07-28|  HKE	| 230	|  	
|212578|GTR|12484689|2010-07-28|  CET	|   0	|  	

R script `R/load_raw_data/sacrois_ices_zone.R` loads sacrois data per year from .Rdata sacrois file and filter sacrois based on user define global variables. Removed rows from sacrois are saved in the tidy data diag folder of the fishery (`path_diag_data_tidy`).


## Transfrom data to EFLALO format

Analyses start with standardized data formats for logbook (EFLALO) used in the VMStools R package 
(<https://github.com/nielshintzen/vmstools/wiki>).
The EFLALO data is structured as a "wide-table".
It consists of a data.frame with fishing sequence as rows and catch and value per species as columns:

|VE_REF|VE_FLT|SEQ_ID|LE_DAT|LE_KG_HKE|LE_KG_CET|LE_EURO_HKE|LE_EURO_CET|
|---|---|---|---|---|---|---|---|
|110852|OTB|19165859|2010-06-16|  100	| 0 |  1200	|0  	|
|212578|GTR|12484689|2010-07-28|  230	| 0	|  1895 |0  	|

Additional columns like the ICES-division where the fishing sequence occurred can be added to the eflalo data frame.
The key structure is that a row represents a unique fishing sequence or a maree with its associated catch per species as a columns.
Two columns per species are stored in the elfalo data: one for the catch in weight (LE_KG) and one for the catch in value (LE_EURO).

- `R/load_raw_data/sacrois_ices_zone_to_eflalo.R`: use the sacrois filtered data and transform it to a eflalo table

# Fishing activity analysis

## Preliminary analysis

Preliminary analysis can be performed to remove or merge variables prior to run metier, strategy and fleet analysis.
The goal here is to reduce the amount of data.
By default, no merge or removal are set, but the user should consider these preliminary analysis in particular if the user wants to work with several year at the same time.

### Gear

Gear can be remove or merge. For example, gears which represent a small proportion of the total revenue or landings (`filter_eflalo_prop_gear_revenue.R`) can be removed from the eflalo dataset.
In order to help the user decide which threshold should be used to filter the data by default `threshold_euro_prop_gear <- 0.95` which means that gears that represent less than 5% of the total landings in euro are removed from eflalo data if and only if `filter_euro_prop_gear <- TRUE`.
By default, `filter_euro_prop_gear` is set to `FALSE`.

One important step for the following analysis is to set the variable that will be used as the gear variable (`set_gear_for_isis.R`).
In the eflalo dataset it is named as `gear_isis`. By default, `gear_isis` is set as the gear of the vessel (`df_eflalo$VE_FLT` : GEN, OTT, OTB ...).

User can choose to work with family of gear taking the first letter of the `df_eflalo$VE_FLT` variable. It is done with 
`set_super_gear_as_gear_family.R` that sets `gear_isis` as G,O,F,D,T...

User can also want to filter which gear or gear_isis to work with. To do so, user can manually filter for example :

```{r, eval = FALSE}
  df_eflalo <- df_eflalo %>%
    filter(str_starts(gear_isis,  "O|G|L")) %>%
    droplevels()
```



### Species

Species can be merge.
For example, species which represent a small proportion of the total revenue or landings (`set_super_other_species.R`) can be merged in one miscellaneous species (`LE_KG_ZZZ` and `LE_EURO_ZZZ` in `df_eflalo`) from the eflalo dataset.
In order to help the user decide which threshold should be used to filter the data by default `threshold_euro_prop_species <- 0.70` which means that species that represent less than 30% of the total landings in euro are merged from eflalo data if and only if `filter_euro_prop_species <- TRUE`.
By default, `filter_euro_prop_species` is set to `FALSE`.

User can also remove species which represent a small fraction of the landings using the same threshold (`threshold_euro_prop_species <- 0.70`) using `filter_eflalo_prop_species_revenue.R`.

User can finally choose to create a super other species with `set_super_other_species.R` which merge all no-key species into `LE_KG_OTH` and `LE_EURO_OTH` in `df_eflalo` if and only if th state variable is set `super_other_species <- TRUE`.
Given the `key_species` state variable, the `set_species_key_and_other.R` extract the columns names in the eflalo dataset for key and no-key species. 


### Metier sacrois or dcf-6

Metier can be remove.
For example, meiter which represent a small proportion of the total revenue or landings (`filter_eflalo_prop_metier_revenue.R`) can be removed from the eflalo dataset.
In order to help the user decide which threshold should be used to filter the data by default `threshold_euro_prop_metier <- 0.95` which means that gears that represent less than 5% of the total landings in euro are removed from eflalo data if and only if `filter_euro_prop_metier <- TRUE`.
By default, `filter_euro_prop_metier` is set to `FALSE`.

## Metier analysis

This analysis consists of assigning a metier to each fishing sequence of the eflalo data frame given for example the species composition of the catch (in quantity or value) and the gear used for each sequence.

### Set metier

#### Set metier as METIER_COD_SACROIS

The simplest and quickest way of setting metier for isis is to use the `METIER_COD_SACROIS` variable computed by the sacrois algorithm (`R/metier_analysis/set_metier_as_sacrois_metier.R`).

#### Set metier with clustering approach

A clustering analysis can be performed to assign a metier to each fishing sequence.
Scripts to perform clustering analysis are saved at `R/metier_analysis/vms_tools_clustering/`.
Given the large dimension of the eflalo table, in particular when the analysis in conducted on several consecutive years, the clustering analysis is performed independently for each gear or super gear (`"R/metier_analysis/vms_tools_clustering/3_metier_clustering.R"`).
First, the iterator used for splitting the clustering analysis is defined with `R/metier_analysis/vms_tools_clustering/1_set_metier_iterator.R`.
Secondly, the user can plot the data before clustering (`R/metier_analysis/vms_tools_clustering/2_plot_eflalo_before_clustering_metier.R`).
The third step is to perform the clustering approach given the control variable set in the control file `R/metier_analysis/vms_tools_clustering/3_metier_clustering.R`.

### Plot and merge metier 

Results of the approach selected to set the metier can be explored with plots (`R/metier_analysis/plot_metier.R`).
These approach will certainly require the user to merge metier in order to reduce their number. This can be done with `source("R/metier_analysis/merge_metier.R")` where the user needs to provide a table associating the metier to merge. For the first execution of the code, an empty table is generated and saved in the tidy data folder of the fishery (for example `data/tidy/bob_macco/user/metier_agg.csv`) .The user can choose to fill it with its merged metier or leave it that way. If the user do not edit this table, no merge will occur.  

### Set metier-zone

When the metier is set properly, user needs to set the metier-zone (`R/metier_analysis/set_metier_zone_season.R`).
By default, for each metier a zone metier is created as the union of the `LE_RECT` spatial variable.
For example, if the `LE_RECT` represents the ICES statistical rectangles, for each metier the union of all the `LE_RECT` associated to the metier will be the zone metier (even if the `LE_RECT` are not direct neighbors).
The user can provide a zone-metier table (feature not tested yet).
For the first execution of the code, an empty table is generated and saved in the tidy data folder of the fishery (for example `data/tidy/bob_macco/user/zone.csv`) .The user can choose to fill it with its zone or leave it that way. If the user do not edit this table, zones are not updated.    

### Set metier-zone-season

Default, no season, season = year (`R/metier_analysis/set_metier_season_as_year.R`).

## Strategy analysis

In process of a generic approach.

- `R/strategy_analysis/set_strategy_as_qam_vessel_length.R` set the strategies as the QAM and vessel length.

- Strategies can be merged by the user if necessary (`R/strategy_analysis/merge_strategie.R`).


## Fleet definition

In process of a generic approach.

- `R/fleet_analysis/set_fleet_as_qam_vessel_length.R` set the strategies as the QAM and vessel length.

- Fleet composition can be plotted using `R/fleet_analysis/process_fleet_data.R` and `R/fleet_analysis/plot_fleet_composition.R`

# Compute effort for ISIS-Fish

Control variable can be set to compute the effort input for ISIS-Fish.
Before computing the input effort, the calibration effort is computed.

## Calibration

Effort and number of vessels per strategy are computed with `R/effort_analysis/compute_isis_effort_calibration.R`.

## Input

User can choose how to compute the number of vessels per strategy as an input with the `strategy_n_navs_input` control variable. 
For now, implemented choice are the number of vessels in the last year (`strategy_n_navs_input <- "last_year"`) or the average number of vessels over the entire time series (`strategy_n_navs_input <- "mean_over_years"`). 
However, user can freely choose the method to set the number of vessels per strategy. It is recommended to keep track of the method with the `strategy_n_navs_input`.

The same approach is used to compute the effort per strategy. The `strategy_effort_input` control variable is implemented with `strategy_effort_input <- "last_year"` and `strategy_effort_input <- "mean_over_years"`.

# Estimate coefficient for ISIS-Fish fishing activity

Finally the augmented data, with the computed ISIS-variables (gear_isis, metier_zone, fleet, strategy, ...), is used to estimate fishing coefficient used in ISIS-Fish parametrization.

For this step, user needs to go back to sacrois data. The first step is to merge the newly created ISIS-fish variable to the sacrois dataset (`R/estim_fishing_activity_parameters/1_load_data_for_estim.R`).
The sacrois data is plotted before modelling to check of any discrepancies (`R/estim_fishing_activity_parameters/2_plot_data.R`).
Models are fitted with `R/estim_fishing_activity_parameters/3_fit_model.R`.

The formula of the model is set dynmaicly allowing to test the formula in different model :

```{r, eval = FALSE}

var_to_explain <- "lpue_log"
standardisation <- "gear_isis"
efficacity <- "fleet"
targeting  <- "metier_zone"

formula_factor_isis <- as.formula(paste0(
  var_to_explain, " ~ ",
  standardisation, " + ",
  efficacity, " + ",
  "ESP_COD_FAO:",
  targeting))

```

Once the user considers a best model, it is stored as 

```{r, eval = FALSE}
###-----------------------------------------------------------------------------
### set best models for later use
lpue_log_best_model <- lpue_log_lm_metier_isis
```

Using `lpue_log_best_model`, user can easily extracts model coefficients with `R/estim_fishing_activity_parameters/4_extract_fitted_coefficient.R` and plotted with `R/estim_fishing_activity_parameters/5_plot_coefficient.R`. If the user wants to control the name of its best model, `R/estim_fishing_activity_parameters/4_extract_fitted_coefficient.R` will need to be edited to take into account the user model name.
